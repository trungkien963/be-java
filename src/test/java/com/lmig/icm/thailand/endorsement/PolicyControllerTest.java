package com.lmig.icm.thailand.endorsement;

import com.google.gson.Gson;
import com.lmig.icm.thailand.endorsement.payload.request.AddEndorsementRequest;
import com.lmig.icm.thailand.endorsement.payload.request.CheckEndorsementStatusRequest;
import com.lmig.icm.thailand.endorsement.payload.response.ApiResponse;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PolicyControllerTest extends AbstractTest {

    @Override
    @Before
    public void setUp() {

        super.setUp();
    }

    @Test
    public void searchPolicies() throws Exception {

        String policyNoPar = "0041877";

        String uri = "/policies";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .param("policyNoPar", policyNoPar)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        if(status == HttpStatus.GATEWAY_TIMEOUT.value()) {
            assertEquals(504, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void getPolicyInfos() throws Exception {

        String policyNo = "19-AV1-0016980-00000-2020-10";

        String uri = "/policies/details";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .param("policyNo", policyNo)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        if(status == HttpStatus.GATEWAY_TIMEOUT.value()) {
            assertEquals(504, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void getPolicyDetails() throws Exception {

        String policyNo = "19-AV1-0018042-00000-2021-12";

        String uri = "/policies/{policyNo}";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri, policyNo)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        if(status == HttpStatus.GATEWAY_TIMEOUT.value()) {
            assertEquals(504, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void addPolicyEndorsement() throws Exception {

        String uri = "/policies/endorsement";

        String json =
            "{\n"
                + "    \"policyNo\": \"19-AV1-0016980-00000-2020-10\",\n"
                + "    \"endorsementType\": \"CHI\"\n"
                + "}";

        Gson gson = new Gson();
        AddEndorsementRequest request = gson.fromJson(json, AddEndorsementRequest.class);

        String inputJson = super.mapToJson(request);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
    }

    @Test
    public void checkEndorsementStatus() throws Exception {

        String uri = "/policies/endorsement/status";

        String json =
            "{\n"
                + "    \"referenceNo\": \"TestRJ0002\",\n"
                + "    \"caller\": \"NEO\",\n"
                + "    \"policyNo\": \"00-AV1-0018381-00000-2016-05\",\n"
                + "    \"rorjorNo\": \"17\"\n"
                + "}";

        Gson gson = new Gson();
        CheckEndorsementStatusRequest request = gson.fromJson(json, CheckEndorsementStatusRequest.class);

        String inputJson = super.mapToJson(request);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        if(status == HttpStatus.NOT_FOUND.value()) {
            assertEquals(404, status);
        }
    }

    @Test
    public void getEndorsementsByPolicyNo() throws Exception {

        String policyNo = "19-AV1-0018042-00000-2021-12";

        String uri = "/{policyNo}/endorsements";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri, policyNo)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        if(status == HttpStatus.GATEWAY_TIMEOUT.value()) {
            assertEquals(504, status);
        }
        if(status == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
            assertEquals(500, status);
        }
    }
}
