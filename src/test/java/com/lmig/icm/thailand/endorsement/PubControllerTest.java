package com.lmig.icm.thailand.endorsement;

import com.google.gson.Gson;
import com.lmig.icm.thailand.endorsement.payload.request.PubLoginRequest;
import com.lmig.icm.thailand.endorsement.payload.response.WsPubFormDetails;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;

public class PubControllerTest extends AbstractTest {

    @Override
    @Before
    public void setUp() {

        super.setUp();
    }

    @Test
    public void login() throws Exception {

        String uri = "/pub/login";

        String json =
            "{\n"
                + "    \"client_id\": \"HJ4FwzG2cCmUmqLjF9AnaO8EFV4REfoL\",\n"
                + "    \"client_secret\": \"k2zn4aarQDKZ520D\"\n"
                + "}";

        Gson gson = new Gson();
        PubLoginRequest request = gson.fromJson(json, PubLoginRequest.class);

        String inputJson = super.mapToJson(request);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        if(status == HttpStatus.UNAUTHORIZED.value()) {
            assertEquals(401, status);
        }
    }

    @Test
    public void searchPolicies() throws Exception {

        String uri = "/pub/forms";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
    }

    @Test
    public void getFormById() throws Exception {

        String formId = "ThailandNeoEndorsementSampleForm";

        String uri = "/pub/forms/{formId}";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri, formId)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
    }

    @Test
    public void upload() throws Exception {

        String uri = "/pub/forms";

        String json =
            "{\n"
                + "    \"callBackUrl\": null,\n"
                + "    \"validateOnly\": null,\n"
                + "    \"client\": {\n"
                + "        \"id\": \"amcmregional\",\n"
                + "        \"requestId\": null,\n"
                + "        \"clientRequestId\": \"amcmregional\",\n"
                + "        \"apiKey\": \"sampleAPIKey\"\n"
                + "    },\n"
                + "    \"formPackages\": [\n"
                + "        {\n"
                + "            \"formIds\": [\n"
                + "                \"ThailandNeoEndorsementSampleForm\"\n"
                + "            ],\n"
                + "            \"packageId\": \"1\",\n"
                + "            \"formFields\": {\n"
                + "                \"Thailand_Endorsement_Sample_CompanyName\": \"FPT Software\",\n"
                + "                \"Thailand_Endorsement_Sample_RecipientName\": \"Mac\",\n"
                + "                \"Thailand_Endorsement_Sample_Surname\": \"NamTe\",\n"
                + "                \"Thailand_Endorsement_Sample_CompanyAddress\": \"HaNoi, Vietnam\",\n"
                + "                \"Thailand_Endorsement_Sample_RecipientAddress\": \"Thailand\",\n"
                + "                \"Thailand_Endorsement_Sample_RecipientCity\": \"Bangkok\"\n"
                + "            },\n"
                + "            \"distributions\": {\n"
                + "                \"email\": null,\n"
                + "                \"archive\": null,\n"
                + "                \"esign\": null,\n"
                + "                \"print\": null,\n"
                + "                \"nebulaArchive\": null\n"
                + "            }\n"
                + "        }\n"
                + "    ]\n"
                + "}";

        Gson gson = new Gson();
        WsPubFormDetails request = gson.fromJson(json, WsPubFormDetails.class);

        String inputJson = super.mapToJson(request);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        if(status == HttpStatus.BAD_REQUEST.value()) {
            assertEquals(400, status);
        }
    }

    @Test
    public void download() throws Exception {

        String requestId = "31607";

        String uri = "/pub/forms/requests/{requestId}";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri, requestId)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
    }
}
