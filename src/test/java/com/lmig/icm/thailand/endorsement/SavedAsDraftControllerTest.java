package com.lmig.icm.thailand.endorsement;

import com.google.gson.Gson;
import com.lmig.icm.thailand.endorsement.payload.request.SavedAsDraftRequest;
import com.lmig.icm.thailand.endorsement.payload.response.ApiResponse;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SavedAsDraftControllerTest extends AbstractTest {

    @Override
    @Before
    public void setUp() {

        super.setUp();
    }

    @Test
    public void saveEndorsementAsDraft() throws Exception {

        String uri = "/drafts/endorsements";

        String json =
            "{\n"
                + "    \"policyNo\": \"00-AV1-0041877-00000-2013-11\",\n"
                + "    \"content\": \"Test\"\n"
                + "}";

        Gson gson = new Gson();
        SavedAsDraftRequest request = gson.fromJson(json, SavedAsDraftRequest.class);

        String inputJson = super.mapToJson(request);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void updateEndorsementAsDraftById() throws Exception {

        String savedAsDraftId = "1";

        String uri = "/drafts/endorsements/{savedAsDraftId}";

        String json =
            "{\n"
                    + "    \"policyNo\": \"00-AV1-0041877-00000-2013-11\",\n"
                    + "    \"content\": \"Test\"\n"
                    + "}";

        Gson gson = new Gson();
        SavedAsDraftRequest request = gson.fromJson(json, SavedAsDraftRequest.class);

        String inputJson = super.mapToJson(request);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri, savedAsDraftId)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void getDraftsById() throws Exception {

        String savedAsDraftId = "1";

        String uri = "/drafts/endorsements/{savedAsDraftId}";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri, savedAsDraftId)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void getDraftsByPolicyNo() throws Exception {

        String policyNo = "00-AV1-0041877-00000-2013-11";

        String uri = "/drafts/endorsements";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .param("policyNo", policyNo)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void getLastDraftsByPolicyNo() throws Exception {

        String policyNo = "00-AV1-0041877-00000-2013-11";

        String uri = "/drafts/endorsements/last";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .param("policyNo", policyNo)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void delete() throws Exception {

        String savedAsDraftId = "1";

        String uri = "/drafts/endorsements/{savedAsDraftId}";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri, savedAsDraftId)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
    }
}
