package com.lmig.icm.thailand.endorsement;

import com.google.gson.Gson;
import com.lmig.icm.thailand.endorsement.payload.request.WsEarnixCalculationRequest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;

public class CalculationControllerTest extends AbstractTest {

    @Override
    @Before
    public void setUp() {

        super.setUp();
    }

    @Test
    public void calculate() throws Exception {

        String uri = "/calculation";

        String json =
            "{\n"
                + "    \"clientService\": \"endorsement\",\n"
                + "    \"earnixEndpoint\": \"libertyasiamcm-staging.earnix.com:9443/earnix/online/v2/product/30005/profilesResult?encounter=1\",\n"
                + "    \"callbackEndpoint\": \"libertyasiamcm-staging.earnix.com:9443/earnix/online/v2/product/30005/profilesResult?encounter=1\",\n"
                + "    \"policyRequest\": [\n"
                + "        {\n"
                + "            \"policyVehicle\": {\n"
                + "                \"requestId\": \"d1389eb9-0cd2-405b-924c-6e9e88be90a0\",\n"
                + "                \"agentTypeKey\": \"0010\",\n"
                + "                \"basePremiumUser\": \"9000\",\n"
                + "                \"branchCode\": \"12\",\n"
                + "                \"branchName\": \"Rama 3 - Auto\",\n"
                + "                \"collBasePremiumUser\": \"3000\",\n"
                + "                \"crossBorderPeriodUser\": null,\n"
                + "                \"distribution\": \"NON-FINANCE\",\n"
                + "                \"distributionType\": \"BROKER\",\n"
                + "                \"fleetDiscountPercentUser\": \"0.0\",\n"
                + "                \"garageTypeUser\": \"Insurer\",\n"
                + "                \"lobUser\": \"AV2+\",\n"
                + "                \"motorCode\": \"210\",\n"
                + "                \"ncbPercentUser\": \"0.5\",\n"
                + "                \"otherDiscountPercentUser\": \"0.0\",\n"
                + "                \"policyEffectiveDate\": \"30/08/2021\",\n"
                + "                \"policyEffectiveDateExpiring\": \"30/08/2020\",\n"
                + "                \"policyExpirationDate\": \"30/04/2022\",\n"
                + "                \"policyExpirationDateExpiring\": \"30/08/2021\",\n"
                + "                \"shortRateUser\": \"Pro Rate\",\n"
                + "                \"surchargePercentUser\": \"0.0\",\n"
                + "                \"sumInsuredUser\": \"100000\",\n"
                + "                \"unannualizedPremiumExpiring\": 16127,\n"
                + "                \"vehicleAdditionalEquipment\": \"N\",\n"
                + "                \"vehicleAge\": 10,\n"
                + "                \"vehicleCapacityKey\": \"01\",\n"
                + "                \"vehicleGroup\": \"0\",\n"
                + "                \"vehicleNoOfSeat\": 8,\n"
                + "                \"vehicleRegistrationYear\": 2012,\n"
                + "\t\t\t\t\"polInsuredType\": \"P\",\n"
                + "\t\t\t\t\"postalCode\": \"10510\",\n"
                + "\t\t\t\t\"vehicleManufactureYear\": 2010,\n"
                + "                \"driver\": [\n"
                + "                    {\n"
                + "                        \"driverAge\": \"\"\n"
                + "                    },\n"
                + "                    {\n"
                + "                        \"driverAge\": \"\"\n"
                + "                    }\n"
                + "                ],\n"
                + "                \"claims\": [],\n"
                + "                \"garageLOB\": [\n"
                + "                    {\n"
                + "                        \"coverage\": [\n"
                + "                            {\n"
                + "                                \"coverageName\": \"Own Damage\",\n"
                + "                                \"covPremiumUser\": \"\",\n"
                + "                                \"covPremiumDriverUser\": \"\",\n"
                + "                                \"covPremiumPassengerUser\": \"\",\n"
                + "                                \"covSumInsuredUser\": \"0\",\n"
                + "                                \"deductibleAmountUser\": \"0\"\n"
                + "                            },\n"
                + "                            {\n"
                + "                                \"coverageName\": \"Temporary Disability\",\n"
                + "                                \"covPremiumUser\": \"\",\n"
                + "                                \"covPremiumDriverUser\": \"0\",\n"
                + "                                \"covPremiumPassengerUser\": \"0\",\n"
                + "                                \"covSumInsuredUser\": \"\",\n"
                + "                                \"deductibleAmountUser\": \"0\"\n"
                + "                            },\n"
                + "                            {\n"
                + "                                \"coverageName\": \"TPBI Person\",\n"
                + "                                \"covPremiumUser\": \"\",\n"
                + "                                \"covPremiumDriverUser\": \"\",\n"
                + "                                \"covPremiumPassengerUser\": \"\",\n"
                + "                                \"covSumInsuredUser\": \"500000\",\n"
                + "                                \"deductibleAmountUser\": \"0\"\n"
                + "                            },\n"
                + "                            {\n"
                + "                                \"coverageName\": \"TPBI Accident\",\n"
                + "                                \"covPremiumUser\": \"\",\n"
                + "                                \"covPremiumDriverUser\": \"\",\n"
                + "                                \"covPremiumPassengerUser\": \"\",\n"
                + "                                \"covSumInsuredUser\": \"10000000\",\n"
                + "                                \"deductibleAmountUser\": \"0\"\n"
                + "                            },\n"
                + "                            {\n"
                + "                                \"coverageName\": \"TPPD\",\n"
                + "                                \"covPremiumUser\": \"\",\n"
                + "                                \"covPremiumDriverUser\": \"\",\n"
                + "                                \"covPremiumPassengerUser\": \"\",\n"
                + "                                \"covSumInsuredUser\": \"1000000\",\n"
                + "                                \"deductibleAmountUser\": \"0\"\n"
                + "                            },\n"
                + "                            {\n"
                + "                                \"coverageName\": \"Fire Theft\",\n"
                + "                                \"covPremiumUser\": \"\",\n"
                + "                                \"covPremiumDriverUser\": \"\",\n"
                + "                                \"covPremiumPassengerUser\": \"\",\n"
                + "                                \"covSumInsuredUser\": \"100000\",\n"
                + "                                \"deductibleAmountUser\": \"0\"\n"
                + "                            },\n"
                + "                            {\n"
                + "                                \"coverageName\": \"Personal Accident\",\n"
                + "                                \"covPremiumUser\": \"\",\n"
                + "                                \"covPremiumDriverUser\": \"1\",\n"
                + "                                \"covPremiumPassengerUser\": \"1\",\n"
                + "                                \"covSumInsuredUser\": \"100000\",\n"
                + "                                \"deductibleAmountUser\": \"0\"\n"
                + "                            },\n"
                + "                            {\n"
                + "                                \"coverageName\": \"Medical Expense\",\n"
                + "                                \"covPremiumUser\": \"\",\n"
                + "                                \"covPremiumDriverUser\": \"1\",\n"
                + "                                \"covPremiumPassengerUser\": \"1\",\n"
                + "                                \"covSumInsuredUser\": \"100000\",\n"
                + "                                \"deductibleAmountUser\": \"0\"\n"
                + "                            },\n"
                + "                            {\n"
                + "                                \"coverageName\": \"Bail Bond\",\n"
                + "                                \"covPremiumUser\": \"1\",\n"
                + "                                \"covPremiumDriverUser\": \"\",\n"
                + "                                \"covPremiumPassengerUser\": \"\",\n"
                + "                                \"covSumInsuredUser\": \"300000\",\n"
                + "                                \"deductibleAmountUser\": \"0\"\n"
                + "                            },\n"
                + "                            {\n"
                + "                                \"coverageName\": \"Collision\",\n"
                + "                                \"covPremiumUser\": \"\",\n"
                + "                                \"covPremiumDriverUser\": \"\",\n"
                + "                                \"covPremiumPassengerUser\": \"\",\n"
                + "                                \"covSumInsuredUser\": \"100000\",\n"
                + "                                \"deductibleAmountUser\": \"0\"\n"
                + "                            }\n"
                + "                        ]\n"
                + "                    }\n"
                + "                ]\n"
                + "            }\n"
                + "        }\n"
                + "    ]\n"
                + "}";

        Gson gson = new Gson();
        WsEarnixCalculationRequest request = gson.fromJson(json, WsEarnixCalculationRequest.class);

        String inputJson = super.mapToJson(request);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
    }
}
