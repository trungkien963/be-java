package com.lmig.icm.thailand.endorsement;

import com.lmig.icm.thailand.endorsement.payload.response.ApiResponse;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ResourceControllerTest extends AbstractTest {

    @Override
    @Before
    public void setUp() {

        super.setUp();
    }

    @Test
    public void getProvinces() throws Exception {

        String uri = "/resources/provinces";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void uploadProvinces() throws Exception {

        String uri = "/resources/provinces/upload";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void getProvincesFromExcel() throws Exception {

        String uri = "/resources/provinces/excel";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void getEndorsements() throws Exception {

        String uri = "/resources/endorsements";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void uploadEndorsements() throws Exception {

        String uri = "/resources/endorsements/upload";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void getEndorsementsFromExcel() throws Exception {

        String uri = "/resources/endorsements/excel";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void getBeneficiaries() throws Exception {

        String uri = "/resources/beneficiaries";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void uploadBeneficiaries() throws Exception {

        String uri = "/resources/beneficiaries/upload";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void getBeneficiariesFromExcel() throws Exception {

        String uri = "/resources/beneficiaries/excel";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void getRoles() throws Exception {

        String uri = "/resources/roles";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void uploadRoles() throws Exception {

        String uri = "/resources/roles/upload";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void getRolesFromExcel() throws Exception {

        String uri = "/resources/roles/excel";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                        .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }

    @Test
    public void getRoleModulesByRoleName() throws Exception {

        String roleName = "ROLE_BRANCH";

        String uri = "/resources/roles/{roleName}";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri,roleName)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
    }
}
