package com.lmig.icm.thailand.endorsement;

import com.lmig.icm.thailand.endorsement.payload.response.ApiResponse;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FileControllerTest extends AbstractTest {

    @Override
    @Before
    public void setUp() {

        super.setUp();
    }

    @Test
    public void getFiles() throws Exception {

        String policyNo = "19-AV1-0016980-00000-2020-10";
        String endorsementType = "CHI";

        String uri = "/files";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .param("policyNo", policyNo)
                .param("endorsementType", endorsementType)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
        String content = mvcResult.getResponse().getContentAsString();
        ApiResponse apiResponse = super.mapFromJson(content, ApiResponse.class);
        assertTrue(apiResponse.getCount() >= 0);
    }
}
