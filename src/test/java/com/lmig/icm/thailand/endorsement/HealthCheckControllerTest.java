package com.lmig.icm.thailand.endorsement;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;

public class HealthCheckControllerTest extends AbstractTest {

    @Override
    @Before
    public void setUp() {

        super.setUp();
    }

    @Test
    public void healthCheck() throws Exception {

        String uri = "/health";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        if(status == HttpStatus.OK.value()) {
            assertEquals(200, status);
        }
    }
}
