package com.lmig.icm.thailand.endorsement.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsNebulaFileUpload {

    @JsonProperty("docUploadReq")
    private WsNebulaFileUploadDetails docUploadReq;
}
