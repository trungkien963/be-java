package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsMemoInfoNew {

    @JsonProperty("EFFECTIVE_DATE")
    private String EFFECTIVE_DATE;

    @JsonProperty("EXPIRATION_DATE")
    private String EXPIRATION_DATE;

    @JsonProperty("MEMO_CODE")
    private String MEMO_CODE;

    @JsonProperty("MEMO_DETAIL")
    private String MEMO_DETAIL;

    @JsonProperty("MEMO_HEADER")
    private String MEMO_HEADER;

    @JsonProperty("MEMO_STATUS")
    private String MEMO_STATUS;

    @JsonProperty("POLICY_ENDORSEMENT_NO")
    private String POLICY_ENDORSEMENT_NO;

    @JsonProperty("POLICY_MEMO_KEY")
    private String POLICY_MEMO_KEY;

    @JsonProperty("POLICY_NO")
    private String POLICY_NO;
}
