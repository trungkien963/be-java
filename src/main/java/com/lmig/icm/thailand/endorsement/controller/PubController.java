package com.lmig.icm.thailand.endorsement.controller;

import com.lmig.icm.thailand.endorsement.payload.request.PubLoginRequest;
import com.lmig.icm.thailand.endorsement.payload.response.*;
import com.lmig.icm.thailand.endorsement.service.PubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/pub")
public class PubController {

    @Autowired
    private PubService pubService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody PubLoginRequest pubLoginRequest) {

        PubLoginResponse response = pubService.login(pubLoginRequest);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/forms")
    public ResponseEntity<?> getForms() {

        PubForms response = pubService.getForms();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/forms/{formId}")
    public ResponseEntity<?> getFormById(@Valid @PathVariable String formId) {

        PubFormDetails response = pubService.getFormById(formId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/forms")
    public ResponseEntity<?> upload(@Valid @RequestBody WsPubFormDetails wsPubFormDetails) {

        WsPubFormStatus response = pubService.upload(wsPubFormDetails);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/forms/requests/{requestId}")
    public ResponseEntity<?> download(@Valid @PathVariable String requestId) {

        WsPubFormStatus response = pubService.download(requestId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
