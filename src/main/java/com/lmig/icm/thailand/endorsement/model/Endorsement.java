package com.lmig.icm.thailand.endorsement.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Endorsement {

    private long id;

    private String type;

    private String endorsement;

    private Integer priority;

    private Integer finalizedPriorityAfter20210907;

    private Integer phase;

    private Integer rorjorNo;

    private String thaiLanguage;
}
