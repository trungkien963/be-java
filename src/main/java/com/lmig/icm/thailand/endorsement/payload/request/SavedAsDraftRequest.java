package com.lmig.icm.thailand.endorsement.payload.request;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SavedAsDraftRequest {

    private String content;

    private String policyNo;

    public String getContent() {
        return (this.content == null ? "" : this.content);
    }

    public String getPolicyNo() {
        return (this.policyNo == null ? "" : this.policyNo);
    }
}
