package com.lmig.icm.thailand.endorsement.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtility {

    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    public static String now() {

        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_NOW);
        String strDate = dateFormat.format(date);
        return strDate;
    }
}
