package com.lmig.icm.thailand.endorsement.service;

import com.lmig.icm.thailand.endorsement.payload.request.NebulaLoginRequest;
import com.lmig.icm.thailand.endorsement.payload.request.WsNebulaFileUpload;
import com.lmig.icm.thailand.endorsement.payload.response.NebulaLoginResponse;
import com.lmig.icm.thailand.endorsement.payload.response.WsNebulaFileDetails;
import com.lmig.icm.thailand.endorsement.payload.response.WsNebulaFileDownload;

public interface NebulaService {

    NebulaLoginResponse login(NebulaLoginRequest nebulaLoginRequest);

    WsNebulaFileDownload upload(WsNebulaFileUpload wsNebulaFileUpload);

    WsNebulaFileDownload download(String documentId);

    WsNebulaFileDetails search(String documentId);
}
