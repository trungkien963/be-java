package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReturnStatus {

    private String batchNumber;

    private String policyNoPar;

    private String vehiclePrefixLicenseNoPar;

    private String vehicleLicenseNoPar;

    private String provinceLicenseNoPar;

    private String insuredNamePar;

    private String dateFromPar;

    private String dateToPar;

    private String corpIdPar;

    private String policyLineOfBusinessPar;

    private String policySerialNumberPar;

    private String policySerialSuffixPar;

    private String policyEffectiveYearPar;

    private String policyEffectiveMonthPar;

    private String agentCodePar;

    private String vehicleChassisNoPar;

    private String wsReturnCode;

    private String wsReturnDescription;
}