package com.lmig.icm.thailand.endorsement.config;

import com.lmig.icm.thailand.endorsement.security.validator.OauthAudienceValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.oauth2.jwt.JwtValidators;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.web.client.RestTemplate;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Value("${auth0.audience}")
  private String audience;

  @Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}")
  private String issuer;

  @Autowired private Environment env;

  @Autowired private RestTemplate restTemplate;

  @Bean
  JwtDecoder jwtDecoder() {
    NimbusJwtDecoder jwtDecoder;
    if (env.getActiveProfiles().length == 0) {
      jwtDecoder =
          NimbusJwtDecoder.withJwkSetUri(issuer + ".well-known/jwks.json")
              .restOperations(restTemplate)
              .build();
    } else {
      jwtDecoder = JwtDecoders.fromOidcIssuerLocation(issuer);
    }

    OAuth2TokenValidator<Jwt> audienceValidator = new OauthAudienceValidator(audience);
    OAuth2TokenValidator<Jwt> withIssuer = JwtValidators.createDefaultWithIssuer(issuer);
    OAuth2TokenValidator<Jwt> withAudience =
        new DelegatingOAuth2TokenValidator<>(withIssuer, audienceValidator);

    jwtDecoder.setJwtValidator(withAudience);

    return jwtDecoder;
  }

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.cors().and().csrf().disable()
        .authorizeRequests()
        .mvcMatchers("/**/calculation/**").authenticated()
        .mvcMatchers("/**/drafts/**").authenticated()
        .mvcMatchers("/**/files/**").authenticated()
        .mvcMatchers("/**/health/**").permitAll()
        .mvcMatchers("/**/helloworld/**").authenticated()
        .mvcMatchers("/**/nebula/**").authenticated()
        .mvcMatchers("/**/policies/**").authenticated()
        .mvcMatchers("/**/pub/**").authenticated()
        .mvcMatchers("/**/resources/**").authenticated()
        .and()
        .oauth2ResourceServer()
        .jwt();
  }
}
