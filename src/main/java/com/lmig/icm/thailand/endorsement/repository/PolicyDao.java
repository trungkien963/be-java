package com.lmig.icm.thailand.endorsement.repository;

import com.lmig.icm.thailand.endorsement.model.Policy;
import com.lmig.icm.thailand.endorsement.model.PolicyEndorsement;

import java.util.List;

public interface PolicyDao {

    List<Policy> getPolicies(String policyNo);

    boolean insertPolicy(Policy policy);

    List<PolicyEndorsement> getEndorsementById(String endorsementId);

    List<PolicyEndorsement> getEndorsements(String policyNo, String endorsementType);

    List<PolicyEndorsement> getUnapprovedEndorsementsByPolicyNo(String policyNo);

    List<PolicyEndorsement> getSubmittedEndorsementsByPolicyNo(String policyNo);

    List<PolicyEndorsement> getApprovedEndorsementsByPolicyNo(String policyNo);

    List<PolicyEndorsement> getRejectedEndorsementsByPolicyNo(String policyNo);

    boolean insertEndorsement(PolicyEndorsement policyEndorsement);

    boolean submitEndorsementForApproval(PolicyEndorsement policyEndorsement);

    boolean approveEndorsement(PolicyEndorsement policyEndorsement);

    boolean rejectEndorsement(PolicyEndorsement policyEndorsement);

    boolean delete(String policyNo, Integer endorsementId);

    List<PolicyEndorsement> getAllEndorsements(String creator);

    List<PolicyEndorsement> getAllEndorsements(String creator, Integer gettable);

    List<PolicyEndorsement> getAllEndorsementsByStatus(String status);

    List<PolicyEndorsement> getEndorsementsByPolicyNo(String policyNo, String creator, Integer gettable);

    List<PolicyEndorsement> getEndorsementsByStatus(String policyNo, String status);
}
