package com.lmig.icm.thailand.endorsement.controller;

import com.lmig.icm.thailand.endorsement.payload.request.NebulaLoginRequest;
import com.lmig.icm.thailand.endorsement.payload.request.WsNebulaFileUpload;
import com.lmig.icm.thailand.endorsement.payload.response.NebulaLoginResponse;
import com.lmig.icm.thailand.endorsement.payload.response.WsNebulaFileDetails;
import com.lmig.icm.thailand.endorsement.payload.response.WsNebulaFileDownload;
import com.lmig.icm.thailand.endorsement.service.NebulaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/nebula")
public class NebulaController {

    @Autowired
    private NebulaService nebulaService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody NebulaLoginRequest nebulaLoginRequest) {

        NebulaLoginResponse response = nebulaService.login(nebulaLoginRequest);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/upload")
    public ResponseEntity<?> upload(@Valid @RequestBody WsNebulaFileUpload wsNebulaFileUpload) {

        WsNebulaFileDownload response = nebulaService.upload(wsNebulaFileUpload);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/documents/{documentId}/download")
    public ResponseEntity<?> download(@Valid @PathVariable String documentId) {

        WsNebulaFileDownload response = nebulaService.download(documentId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/documents/{documentId}")
    public ResponseEntity<?> search(@Valid @PathVariable String documentId) {

        WsNebulaFileDetails response = nebulaService.search(documentId);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}