package com.lmig.icm.thailand.endorsement.service;

import com.lmig.icm.thailand.endorsement.model.SavedAsDraft;
import com.lmig.icm.thailand.endorsement.payload.request.SavedAsDraftRequest;

import java.util.List;

public interface SavedAsDraftService {

    List<SavedAsDraft> saveEndorsementAsDraft(SavedAsDraftRequest savedAsDraftRequest);

    List<SavedAsDraft> updateEndorsementAsDraftById(String content, Integer savedAsDraftId);

    List<SavedAsDraft> getDraftsById(Integer savedAsDraftId);

    List<SavedAsDraft> getDraftsByCategory(String category);

    List<SavedAsDraft> getDraftsByPolicyNo(String category, String policyNo);

    List<SavedAsDraft> getLastDraftsByPolicyNo(String category, String policyNo);

    boolean delete(Integer savedAsDraftId);

    boolean deleteByPolicyNo(String policyNo);

    List<SavedAsDraft> getDraftsByStatus(String category, String savedAsDraftStatus);

    List<SavedAsDraft> getDraftsByHistory(String category);
}
