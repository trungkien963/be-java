package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsPolicyRenewalHistory {

    @JsonProperty("COMPLAINANT_OS")
    private Float COMPLAINANT_OS;

    @JsonProperty("DEFENDANT_OS")
    private Float DEFENDANT_OS;

    @JsonProperty("GPRM")
    private Float GPRM;

    @JsonProperty("GPRM_EXCLUDE_ADJUST")
    private Float GPRM_EXCLUDE_ADJUST;

    @JsonProperty("LOSS_RATIO")
    private Float LOSS_RATIO;

    @JsonProperty("NCB_PREMIUM")
    private Float NCB_PREMIUM;

    @JsonProperty("POLICY_EFFECTIVE_DATE")
    private String POLICY_EFFECTIVE_DATE;

    @JsonProperty("POLICY_EXPIRATION_DATE")
    private String POLICY_EXPIRATION_DATE;

    @JsonProperty("POLICY_NO")
    private String POLICY_NO;

    @JsonProperty("RENEWAL_YEAR")
    private Integer RENEWAL_YEAR;

    @JsonProperty("TOTAL_PREMIUM")
    private Float TOTAL_PREMIUM;
}
