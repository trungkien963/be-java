package com.lmig.icm.thailand.endorsement.controller;

import com.lmig.icm.thailand.endorsement.enumType.SavedAsDraftType;
import com.lmig.icm.thailand.endorsement.model.SavedAsDraft;
import com.lmig.icm.thailand.endorsement.payload.request.SavedAsDraftRequest;
import com.lmig.icm.thailand.endorsement.payload.response.ApiResponse;
import com.lmig.icm.thailand.endorsement.service.SavedAsDraftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/drafts")
public class SavedAsDraftController {

    @Autowired
    private SavedAsDraftService savedAsDraftService;

    @PostMapping("/endorsements")
    public ResponseEntity<?> saveEndorsementAsDraft(
            @Valid @RequestBody SavedAsDraftRequest savedAsDraftRequest) {

        List<SavedAsDraft> results = savedAsDraftService.saveEndorsementAsDraft(savedAsDraftRequest);

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null));
    }

    @PutMapping("/endorsements/{savedAsDraftId}")
    public ResponseEntity<?> updateEndorsementAsDraftById(
            @Valid @RequestBody SavedAsDraftRequest savedAsDraftRequest,
            @Valid @PathVariable Integer savedAsDraftId) {

        List<SavedAsDraft> results = savedAsDraftService.updateEndorsementAsDraftById(savedAsDraftRequest.getContent(), savedAsDraftId);

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null));
    }

    @GetMapping("/endorsements/{savedAsDraftId}")
    public ResponseEntity<?> getDraftsById(
            @Valid @PathVariable Integer savedAsDraftId) {

        List<SavedAsDraft> results = savedAsDraftService.getDraftsById(savedAsDraftId);

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null));
    }

    @GetMapping("/endorsements")
    public ResponseEntity<?> getDraftsByPolicyNo(
            @Valid @RequestParam(required = true) String policyNo) {

        List<SavedAsDraft> results = savedAsDraftService.getDraftsByPolicyNo(SavedAsDraftType.ENDORSEMENT.toString(), policyNo);

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null));
    }

    @GetMapping("/endorsements/last")
    public ResponseEntity<?> getLastDraftsByPolicyNo(
            @Valid @RequestParam(required = false) String policyNo) {

        List<SavedAsDraft> results = savedAsDraftService.getLastDraftsByPolicyNo(SavedAsDraftType.ENDORSEMENT.toString(), policyNo);

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null));
    }

    @DeleteMapping("/endorsements/{savedAsDraftId}")
    public ResponseEntity<?> delete(
            @Valid @PathVariable Integer savedAsDraftId) {

        List<SavedAsDraft> results = new ArrayList<>();

        savedAsDraftService.delete(savedAsDraftId);

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null));
    }

    @GetMapping("/endorsements/status/{savedAsDraftStatus}")
    public ResponseEntity<?> getDraftsByStatus(
            @Valid @PathVariable String savedAsDraftStatus) {

        List<SavedAsDraft> results = savedAsDraftService.getDraftsByStatus(SavedAsDraftType.ENDORSEMENT.toString(), savedAsDraftStatus);

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null));
    }

    @GetMapping("/endorsements/history")
    public ResponseEntity<?> getDraftsByHistory() {

        List<SavedAsDraft> results = savedAsDraftService.getDraftsByHistory(SavedAsDraftType.ENDORSEMENT.toString());

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null));
    }
}
