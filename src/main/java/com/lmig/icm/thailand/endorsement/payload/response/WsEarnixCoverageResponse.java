package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsEarnixCoverageResponse {

    private Integer annualizedCoveragePremiumDriver;

    private Integer annualizedCoveragePremiumPassenger;

    private Integer annualizedCoveragePremium;

    private String coverageName;

    private Integer coveragePremiumDriver;

    private Integer coveragePremiumPassenger;

    private Integer coveragePremium;

    private Integer sumInsuredCov;
}
