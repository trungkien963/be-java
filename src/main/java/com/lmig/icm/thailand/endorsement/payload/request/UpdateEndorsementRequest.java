package com.lmig.icm.thailand.endorsement.payload.request;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateEndorsementRequest {

    private String status;

    private String content;

    private String reason;

    public String getStatus() {
        return (this.status == null ? "" : this.status);
    }

    public String getContent() {
        return (this.content == null ? "" : this.content);
    }

    public String getReason() {
        return (this.reason == null ? "" : this.reason);
    }
}
