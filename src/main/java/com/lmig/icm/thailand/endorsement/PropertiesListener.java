package com.lmig.icm.thailand.endorsement;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.DecryptRequest;
import software.amazon.awssdk.services.kms.model.DecryptResponse;
import software.amazon.awssdk.utils.BinaryUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class PropertiesListener implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {

  @Override
  public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
    ConfigurableEnvironment environment = event.getEnvironment();

    if (environment.getActiveProfiles().length > 0) {
      Map<String, Object> propertiesMap = new HashMap<>();
      propertiesMap.put(
              "spring.datasource.password",
              decryptPassword(
                      environment.getProperty("aws.kms.keyId"),
                      environment.getProperty("spring.datasource.password")));

      MutablePropertySources mps = environment.getPropertySources();
      mps.addFirst(new MapPropertySource("MyProperties", propertiesMap));
    }
  }

  private String decryptPassword(String awsKmsArn, String encryptedPassword) {
    log.info("awsKmsArn={}", awsKmsArn);
    log.info("pass={}", encryptedPassword);
    KmsClient kmsClient = KmsClient.builder().region(Region.AP_SOUTHEAST_1).build();

    byte[] encryptedPasswordBytes = BinaryUtils.fromBase64(encryptedPassword);
    DecryptRequest decryptRequest =
            DecryptRequest.builder()
                    .keyId(awsKmsArn)
                    .ciphertextBlob(SdkBytes.fromByteArray(encryptedPasswordBytes))
                    .encryptionContext(Collections.singletonMap("Password", "CheckIntegrity"))
                    .build();
    DecryptResponse decryptResponse = kmsClient.decrypt(decryptRequest);

    return decryptResponse.plaintext().asUtf8String();
  }
}
