package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsAegisCreateEndorsementResponse {

    @JsonProperty("ws_return_code")
    private WsAegisCreateEndorsementReturnCode wsReturnCode;

    @JsonProperty("endorsement_details")
    private List<WsAegisCreateEndorsementDetails> endorsementDetails;
}
