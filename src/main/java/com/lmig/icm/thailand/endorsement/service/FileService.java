package com.lmig.icm.thailand.endorsement.service;

import com.lmig.icm.thailand.endorsement.model.PolicyEndorsementFile;
import com.lmig.icm.thailand.endorsement.payload.request.FileUploadRequest;
import com.lmig.icm.thailand.endorsement.payload.response.WsNebulaFileDownload;
import com.lmig.icm.thailand.endorsement.payload.response.WsPubFormDetails;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface FileService {

    void init();

    List<PolicyEndorsementFile> getFiles(String endorsementId);

    List<PolicyEndorsementFile> getFiles(String policyNo, String endorsementType);

    List<?> store(MultipartFile[] files, FileUploadRequest fileUploadRequest);

    Resource loadAsResource(String fullName);

    boolean delete(String fullName);

    WsNebulaFileDownload uploadFileToNebula(String policyNo, WsPubFormDetails wsPubFormDetails) throws IOException;
}
