package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsPolicyInfo {

    private String batch_number;

    private String batch_key;

    private String reference_no;

    private String policy_no;

    private String office_code;

    private String policy_effective_date;

    private String policy_expiration_date;

    private String total_day;

    private String agent_code;

    private String agent_name;

    private String finance_agent_type_flag;

    private String direct_agent_type_flag;

    private String agent_type_key;

    private String broker_code;

    private String broker_name;

    private String parent_agent_code;

    private String parent_agent_name;

    private String sub_agent_code;

    private String sub_agent_name;

    private String gprm;

    private String tax;

    private String stamp;

    private String total_premium;

    private String sign_date;

    private String entry_date;

    private String insured_code;

    private String insured_type;

    private String title;

    private String insured_name;

    private String occupation_code;

    private String occupation;

    private String address_line1;

    private String tumbon;

    private String district;

    private String province;

    private String postal_code;

    private String motor_code;

    private String motor_sub_code;

    private String motor_code_info;

    private String vehicle_prefix_license_no;

    private String vehicle_license_no;

    private String vehicle_province;

    private String key_field;

    private String vehicle_chassis_no;

    private String vehicle_engine_no;

    private String vehicle_capacity_key;

    private String group_of_vehicle;

    private String vehicle_reg_year;

    private String additional_equipment_flag;

    private String vehicle_no_of_seat;

    private String count_claim;

    private String link_policy_no;

    private String link_policy_effective_date;

    private String link_policy_expiration_date;

    private String link_policy_status;

    private String basic_premium;

    private String sum_insured;

    private String av5_basic_premium;

    private String av5_sum_insured;

    private String odav_amount;

    private String ftav_amount;

    private String tpbiav_person_amount;

    private String tpbiav_accident_amount;

    private String tppdav_amount;

    private String sipr_premium;

    private String acovpd_amount;

    private String acovpd_premium;

    private String acovpp_amount;

    private String acovpp_premium;

    private String acovtd_amount;

    private String acovtd_premium;

    private String acovtp_amount;

    private String acovtp_premium;

    private String acovme_amount;

    private String acovme_premium;

    private String acovmp_amount;

    private String acovmp_premium;

    private String acovbb_amount;

    private String acovbb_premium;

    private String acovod_amount;

    private String acovth_amount;

    private String acovfd_percent;

    private String acovfd_amount;

    private String acoved_percent;

    private String acoved_amount;

    private String acovot_percent;

    private String acovot_amount;

    private String acovea_percent;

    private String acovea_amount;

    private String vehicle_make;

    private String vehicle_model;

    private String vehicle_body_type;

    private String vehicle_engine_capacity;

    private String vehicle_garage_location;

    private String vehicle_model_type;

    private String english_name_flag;

    private String ws_call_from;

    private String motor_body_type;

    private String epolicy_flag;

    private String policy_status;
}
