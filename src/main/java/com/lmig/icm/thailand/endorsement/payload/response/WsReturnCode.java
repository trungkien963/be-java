package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsReturnCode {

    private String batch_number;

    private String batch_key;

    private String reference_no;

    private String ws_return_code;

    private String ws_return_description;

    private String policy_no_par;
}
