package com.lmig.icm.thailand.endorsement.payload.request;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsCheckOKToEndorseAutoRequest {

    public String REFERENCE_NO;

    public String CALLER;

    public String POLICY_NO;

    public String RORJOR_NO;

    public String getREFERENCE_NO() {
        return (this.REFERENCE_NO == null ? "" : this.REFERENCE_NO);
    }

    public String getCALLER() {
        return (this.CALLER == null ? "" : this.CALLER);
    }

    public String getPOLICY_NO() {
        return (this.POLICY_NO == null ? "" : this.POLICY_NO);
    }

    public String getRORJOR_NO() {
        return (this.RORJOR_NO == null ? "" : this.RORJOR_NO);
    }
}
