package com.lmig.icm.thailand.endorsement;

import com.lmig.icm.thailand.endorsement.config.StorageConfig;
import com.lmig.icm.thailand.endorsement.service.FileService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties(StorageConfig.class)
public class ThailandNeoEndorsementApiApplication {

  public static void main(String[] args) {
    SpringApplication app = new SpringApplication(ThailandNeoEndorsementApiApplication.class);
    app.addListeners(new PropertiesListener());
    app.run(args);
  }

  @Bean
  CommandLineRunner init(FileService fileUploadDao) {
    return (args) -> {
      fileUploadDao.init();
    };
  }
}
