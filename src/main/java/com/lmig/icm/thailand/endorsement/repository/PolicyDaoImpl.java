package com.lmig.icm.thailand.endorsement.repository;

import com.lmig.icm.thailand.endorsement.enumType.PolicyEndorsementStatus;
import com.lmig.icm.thailand.endorsement.model.Policy;
import com.lmig.icm.thailand.endorsement.model.PolicyEndorsement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class PolicyDaoImpl implements PolicyDao {

    @Value("${api.host.baseurl}")
    public String RESTUri;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final class PolicyRowMapper implements RowMapper<Policy> {

        @Override
        public Policy mapRow(final ResultSet rs, final int rowNum) throws SQLException {

            return new Policy(
                    rs.getLong("ID"),
                    rs.getString("BATCH_NUMBER"),
                    rs.getString("POLICY_NO"),
                    rs.getString("ENTRY_DATE"),
                    rs.getString("APPLICATION_DATE"),
                    rs.getString("SIGN_DATE"),
                    rs.getString("POST_DATE"),
                    rs.getString("POLICY_STATUS"),
                    rs.getString("RENEWAL_FLAG"),
                    rs.getString("LAST_ENDORSEMENT_NO"),
                    rs.getString("AGENT_CODE"),
                    rs.getString("VEHICLE_CHASSIS_NO"),
                    rs.getString("WS_CALL_FROM"));
        }
    }

    private static final class PolicyEndorsementRowMapper implements RowMapper<PolicyEndorsement> {

        @Override
        public PolicyEndorsement mapRow(final ResultSet rs, final int rowNum) throws SQLException {

            return new PolicyEndorsement(
                    rs.getLong("ID"),
                    rs.getString("POLICY_NO"),
                    rs.getString("ENDORSEMENT_TYPE"),
                    rs.getString("CONTENT"),
                    rs.getString("ENDORSEMENT_DETAILS"),
                    rs.getString("STATUS"),
                    rs.getString("CREATOR"),
                    rs.getString("CREATOR_EMAIL"),
                    rs.getString("CREATED_DATE"),
                    rs.getString("SUBMITTER"),
                    rs.getString("SUBMITTER_EMAIL"),
                    rs.getString("SUBMITTED_DATE"),
                    rs.getString("APPROVER"),
                    rs.getString("APPROVER_EMAIL"),
                    rs.getString("APPROVED_DATE"),
                    rs.getString("REJECTER"),
                    rs.getString("REJECTER_EMAIL"),
                    rs.getString("REJECTED_REASON"),
                    rs.getString("REJECTED_DATE"));
        }
    }

    @Override
    public List<Policy> getPolicies(String policyNo) {

        return jdbcTemplate.query("SELECT * FROM POLICY WHERE POLICY_NO = ? ",
                new Object[] { policyNo }, new PolicyRowMapper());
    }

    @Override
    public boolean insertPolicy(Policy policy) {

        jdbcTemplate.update("INSERT INTO POLICY (" +
                                "BATCH_NUMBER, " +
                                "POLICY_NO, " +
                                "ENTRY_DATE, " +
                                "APPLICATION_DATE, " +
                                "SIGN_DATE, " +
                                "POST_DATE," +
                                "POLICY_STATUS," +
                                "RENEWAL_FLAG," +
                                "LAST_ENDORSEMENT_NO," +
                                "AGENT_CODE," +
                                "VEHICLE_CHASSIS_NO," +
                                "WS_CALL_FROM) " +
                                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                        policy.getBatchNumber(),
                        policy.getPolicyNo(),
                        policy.getEntryDate(),
                        policy.getApplicationDate(),
                        policy.getSignDate(),
                        policy.getPostDate(),
                        policy.getPolicyStatus(),
                        policy.getRenewalFlag(),
                        policy.getLastEndorsementNo(),
                        policy.getAgentCode(),
                        policy.getVehicleChassisNo(),
                        policy.getWsCallFrom());

        return true;
    }

    @Override
    public List<PolicyEndorsement> getEndorsementById(String endorsementId) {
        return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT " +
                        "WHERE ID = ? ",
                new Object[] { endorsementId },
                new PolicyEndorsementRowMapper());
    }

    @Override
    public List<PolicyEndorsement> getEndorsements(String policyNo, String endorsementType) {

        return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT " +
                        "WHERE POLICY_NO = ? " +
                        "AND ENDORSEMENT_TYPE = ? ",
                new Object[] { policyNo, endorsementType },
                new PolicyEndorsementRowMapper());
    }

    @Override
    public List<PolicyEndorsement> getUnapprovedEndorsementsByPolicyNo(String policyNo) {

        return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT " +
                        "WHERE POLICY_NO = ? " +
                        "AND (STATUS = ? OR STATUS = 'STATUS_UNAPPROVED') ",
                new Object[] { policyNo, PolicyEndorsementStatus.UNAPPROVED.toString() },
                new PolicyEndorsementRowMapper());
    }

    @Override
    public List<PolicyEndorsement> getSubmittedEndorsementsByPolicyNo(String policyNo) {

        return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT " +
                        "WHERE POLICY_NO = ? " +
                        "AND STATUS = ? ",
                new Object[] { policyNo, PolicyEndorsementStatus.WAITING_FOR_APPROVAL.toString() },
                new PolicyEndorsementRowMapper());
    }

    @Override
    public List<PolicyEndorsement> getApprovedEndorsementsByPolicyNo(String policyNo) {

        return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT " +
                        "WHERE POLICY_NO = ? " +
                        "AND STATUS = ? ",
                new Object[] { policyNo, PolicyEndorsementStatus.APPROVED.toString() },
                new PolicyEndorsementRowMapper());
    }

    @Override
    public List<PolicyEndorsement> getRejectedEndorsementsByPolicyNo(String policyNo) {

        return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT " +
                        "WHERE POLICY_NO = ? " +
                        "AND STATUS = ? ",
                new Object[] { policyNo, PolicyEndorsementStatus.REJECTED.toString() },
                new PolicyEndorsementRowMapper());
    }

    @Override
    public boolean insertEndorsement(PolicyEndorsement policyEndorsement) {

        jdbcTemplate.update("INSERT INTO POLICY_ENDORSEMENT (" +
                            "POLICY_NO, " +
                            "ENDORSEMENT_TYPE, " +
                            "CONTENT, " +
                            "ENDORSEMENT_DETAILS, " +
                            "STATUS, " +
                            "CREATOR, " +
                            "CREATOR_EMAIL, " +
                            "CREATED_DATE, " +
                            "SUBMITTER, " +
                            "SUBMITTER_EMAIL, " +
                            "SUBMITTED_DATE, " +
                            "APPROVER, " +
                            "APPROVER_EMAIL, " +
                            "APPROVED_DATE, " +
                            "REJECTER, " +
                            "REJECTER_EMAIL, " +
                            "REJECTED_REASON, " +
                            "REJECTED_DATE) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                    policyEndorsement.getPolicyNo(),
                    policyEndorsement.getEndorsementType(),
                    policyEndorsement.getContent(),
                    policyEndorsement.getEndorsementDetails(),
                    policyEndorsement.getStatus(),
                    policyEndorsement.getCreator(),
                    policyEndorsement.getCreatorEmail(),
                    policyEndorsement.getCreatedDate(),
                    policyEndorsement.getSubmitter(),
                    policyEndorsement.getSubmitterEmail(),
                    policyEndorsement.getSubmittedDate(),
                    policyEndorsement.getApprover(),
                    policyEndorsement.getApproverEmail(),
                    policyEndorsement.getApprovedDate(),
                    policyEndorsement.getRejecter(),
                    policyEndorsement.getRejecterEmail(),
                    policyEndorsement.getRejectedReason(),
                    policyEndorsement.getRejectedDate());

        return true;
    }

    @Override
    public boolean submitEndorsementForApproval(PolicyEndorsement policyEndorsement) {

        jdbcTemplate.update("UPDATE POLICY_ENDORSEMENT " +
                        "SET STATUS = ?, " +
                        "CONTENT = ?, " +
                        "SUBMITTER = ?, " +
                        "SUBMITTER_EMAIL = ?, " +
                        "SUBMITTED_DATE = ? " +
                        "WHERE POLICY_NO = ? AND (STATUS = 'UNAPPROVED' OR STATUS = 'STATUS_UNAPPROVED') ",
                policyEndorsement.getStatus(),
                policyEndorsement.getContent(),
                policyEndorsement.getSubmitter(),
                policyEndorsement.getSubmitterEmail(),
                policyEndorsement.getSubmittedDate(),
                policyEndorsement.getPolicyNo());

        return true;
    }

    @Override
    public boolean approveEndorsement(PolicyEndorsement policyEndorsement) {

        jdbcTemplate.update("UPDATE POLICY_ENDORSEMENT " +
                        "SET STATUS = ?, " +
                        "CONTENT = ?, " +
                        "ENDORSEMENT_DETAILS = ?, " +
                        "APPROVER = ?, " +
                        "APPROVER_EMAIL = ?, " +
                        "APPROVED_DATE = ?, " +
                        "REJECTER = ?, " +
                        "REJECTER_EMAIL = ?, " +
                        "REJECTED_REASON = ?, " +
                        "REJECTED_DATE = ? " +
                        "WHERE POLICY_NO = ? AND (STATUS = 'WAITING_FOR_APPROVAL' OR STATUS = 'UNAPPROVED') ",
                policyEndorsement.getStatus(),
                policyEndorsement.getContent(),
                policyEndorsement.getEndorsementDetails(),
                policyEndorsement.getApprover(),
                policyEndorsement.getApproverEmail(),
                policyEndorsement.getApprovedDate(),
                policyEndorsement.getRejecter(),
                policyEndorsement.getRejecterEmail(),
                policyEndorsement.getRejectedReason(),
                policyEndorsement.getRejectedDate(),
                policyEndorsement.getPolicyNo());

        return true;
    }

    @Override
    public boolean rejectEndorsement(PolicyEndorsement policyEndorsement) {

        jdbcTemplate.update("UPDATE POLICY_ENDORSEMENT " +
                        "SET STATUS = ?, " +
                        "CONTENT = ?, " +
                        "ENDORSEMENT_DETAILS = ?, " +
                        "APPROVER = ?, " +
                        "APPROVER_EMAIL = ?, " +
                        "APPROVED_DATE = ?, " +
                        "REJECTER = ?, " +
                        "REJECTER_EMAIL = ?, " +
                        "REJECTED_REASON = ?, " +
                        "REJECTED_DATE = ? " +
                        "WHERE POLICY_NO = ? AND STATUS = 'WAITING_FOR_APPROVAL' ",
                policyEndorsement.getStatus(),
                policyEndorsement.getContent(),
                policyEndorsement.getEndorsementDetails(),
                policyEndorsement.getApprover(),
                policyEndorsement.getApproverEmail(),
                policyEndorsement.getApprovedDate(),
                policyEndorsement.getRejecter(),
                policyEndorsement.getRejecterEmail(),
                policyEndorsement.getRejectedReason(),
                policyEndorsement.getRejectedDate(),
                policyEndorsement.getPolicyNo());

        return true;
    }

    @Override
    public boolean delete(String policyNo, Integer endorsementId) {

        String sql = "DELETE FROM POLICY_ENDORSEMENT " +
                "WHERE POLICY_NO = ? AND ID = ? ";
        Object[] args = new Object[] { policyNo, endorsementId };

        return jdbcTemplate.update(sql, args) == 1;
    }

    @Override
    public List<PolicyEndorsement> getAllEndorsements(String creator) {

        return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT " +
                        "WHERE CREATOR = ? ",
                new Object[] { creator }, new PolicyEndorsementRowMapper());
    }

    @Override
    public List<PolicyEndorsement> getAllEndorsements(String creator, Integer gettable) {

        if(gettable.equals(1)) {

            return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT " +
                            "WHERE (STATUS = 'APPROVED' OR STATUS = 'REJECTED') AND (CREATOR = ? OR APPROVER = ? OR REJECTER = ?) ",
                    new Object[] { creator, creator, creator }, new PolicyEndorsementRowMapper());
        } else {

            return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT " +
                            "WHERE CREATOR = ? ",
                    new Object[] { creator }, new PolicyEndorsementRowMapper());
        }
    }

    @Override
    public List<PolicyEndorsement> getAllEndorsementsByStatus(String status) {

        return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT " +
                        "WHERE STATUS = ? ",
                new Object[] { status }, new PolicyEndorsementRowMapper());
    }

    @Override
    public List<PolicyEndorsement> getEndorsementsByPolicyNo(String policyNo, String creator, Integer gettable) {

        if(gettable.equals(1)) {

            return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT " +
                            "WHERE POLICY_NO = ? ",
                    new Object[] { policyNo }, new PolicyEndorsementRowMapper());
        } else {

            return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT " +
                            "WHERE POLICY_NO = ? AND CREATOR = ? ",
                    new Object[] { policyNo, creator }, new PolicyEndorsementRowMapper());
        }
    }

    @Override
    public List<PolicyEndorsement> getEndorsementsByStatus(String policyNo, String status) {

        return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT " +
                        "WHERE POLICY_NO = ? AND STATUS = ? ",
                new Object[] { policyNo, status }, new PolicyEndorsementRowMapper());
    }
}
