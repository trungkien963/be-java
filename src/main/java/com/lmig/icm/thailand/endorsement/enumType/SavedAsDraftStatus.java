package com.lmig.icm.thailand.endorsement.enumType;

public enum SavedAsDraftStatus {
    STATUS_SUBMITTED_FOR_APPROVAL,
    STATUS_PROCESSED,
    STATUS_REJECTED;
}
