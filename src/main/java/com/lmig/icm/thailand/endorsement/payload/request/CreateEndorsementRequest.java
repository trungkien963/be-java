package com.lmig.icm.thailand.endorsement.payload.request;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateEndorsementRequest {

    private String informToChange;

    private String informer;

    private String receiveEndorsement;

    private String completeEndorsement;

    private String effectiveDate;

    private String remarks;

    private String licensePrefix;

    private String licenseNo;

    private String licenseProvince;

    private String chassisNo;

    private String engineNo;

    private String make;

    private String model;

    private String subModel;

    private String year;

    private String cc;

    private String weight;

    private String numberOfSeat;

    private String color;

    public String getInformToChange() {
        return (this.informToChange == null ? "" : this.informToChange);
    }

    public String getInformer() {
        return (this.informer == null ? "" : this.informer);
    }

    public String getReceiveEndorsement() {
        return (this.receiveEndorsement == null ? "" : this.receiveEndorsement);
    }

    public String getCompleteEndorsement() {
        return (this.completeEndorsement == null ? "" : this.completeEndorsement);
    }

    public String getEffectiveDate() {
        return (this.effectiveDate == null ? "" : this.effectiveDate);
    }

    public String getRemarks() {
        return (this.remarks == null ? "" : this.remarks);
    }

    public String getLicensePrefix() {
        return (this.licensePrefix == null ? "" : this.licensePrefix);
    }

    public String getLicenseNo() {
        return (this.licenseNo == null ? "" : this.licenseNo);
    }

    public String getLicenseProvince() {
        return (this.licenseProvince == null ? "" : this.licenseProvince);
    }

    public String getChassisNo() {
        return (this.chassisNo == null ? "" : this.chassisNo);
    }

    public String getEngineNo() {
        return (this.engineNo == null ? "" : this.engineNo);
    }

    public String getMake() {
        return (this.make == null ? "" : this.make);
    }

    public String getModel() {
        return (this.model == null ? "" : this.model);
    }

    public String getSubModel() {
        return (this.subModel == null ? "" : this.subModel);
    }

    public String getYear() {
        return (this.year == null ? "" : this.year);
    }

    public String getCc() {
        return (this.cc == null ? "" : this.cc);
    }

    public String getWeight() {
        return (this.weight == null ? "" : this.weight);
    }

    public String getNumberOfSeat() {
        return (this.numberOfSeat == null ? "" : this.numberOfSeat);
    }

    public String getColor() {
        return (this.color == null ? "" : this.color);
    }
}
