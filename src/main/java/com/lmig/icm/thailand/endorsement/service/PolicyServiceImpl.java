package com.lmig.icm.thailand.endorsement.service;

import com.google.gson.Gson;
import com.lmig.icm.thailand.endorsement.config.GeneralConfig;
import com.lmig.icm.thailand.endorsement.enumType.EndorsementType;
import com.lmig.icm.thailand.endorsement.enumType.PolicyEndorsementStatus;
import com.lmig.icm.thailand.endorsement.model.*;
import com.lmig.icm.thailand.endorsement.payload.request.*;
import com.lmig.icm.thailand.endorsement.payload.response.*;
import com.lmig.icm.thailand.endorsement.repository.PolicyDao;
import com.lmig.icm.thailand.endorsement.util.DateUtility;
import com.lmig.icm.thailand.endorsement.util.UserUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class PolicyServiceImpl implements PolicyService {

    @Value("${api.host.baseurl}")
    public String RESTUri;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private PolicyDao policyDao;

    @Autowired
    private SavedAsDraftService savedAsDraftService;

    @Override
    public List<Policy> getPolicies(String policyNo) {

        return policyDao.getPolicies(policyNo);
    }

    @Override
    public ReturnResult searchPolicies(SearchPolicyRequest searchPolicyRequest) {

        ReturnResult returnResult = new ReturnResult();
        List<Policy> policies = new ArrayList<>();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<SearchPolicyRequest> entity = new HttpEntity<>(searchPolicyRequest, headers);

        WsSearchPolicyInfoResult wsSearchPolicyInfoResult = restTemplate.postForObject(RESTUri + GeneralConfig.searchPolicyInfo, entity, WsSearchPolicyInfoResult.class);

        WsReturnStatus wsReturnStatus = wsSearchPolicyInfoResult.getWs_return_status();

        ReturnStatus returnStatus = new ReturnStatus();
        returnStatus.setBatchNumber(wsReturnStatus.getBatch_number());
        returnStatus.setPolicyNoPar(wsReturnStatus.getPolicy_no_par());
        returnStatus.setVehiclePrefixLicenseNoPar(wsReturnStatus.getVehicle_prefix_license_no_par());
        returnStatus.setVehicleLicenseNoPar(wsReturnStatus.getVehicle_license_no_par());
        returnStatus.setProvinceLicenseNoPar(wsReturnStatus.getProvince_license_no_par());
        returnStatus.setInsuredNamePar(wsReturnStatus.getInsured_name_par());
        returnStatus.setDateFromPar(wsReturnStatus.getDate_from_par());
        returnStatus.setDateToPar(wsReturnStatus.getDate_to_par());
        returnStatus.setCorpIdPar(wsReturnStatus.getCorp_id_par());
        returnStatus.setPolicyLineOfBusinessPar(wsReturnStatus.getPolicy_line_of_business_par());
        returnStatus.setPolicySerialNumberPar(wsReturnStatus.getPolicy_serial_number_par());
        returnStatus.setPolicySerialSuffixPar(wsReturnStatus.getPolicy_serial_suffix_par());
        returnStatus.setPolicyEffectiveYearPar(wsReturnStatus.getPolicy_effective_year_par());
        returnStatus.setPolicyEffectiveMonthPar(wsReturnStatus.getPolicy_effective_month_par());
        returnStatus.setAgentCodePar(wsReturnStatus.getAgent_code_par());
        returnStatus.setVehicleChassisNoPar(wsReturnStatus.getVehicle_chassis_no_par());
        returnStatus.setWsReturnCode(wsReturnStatus.getWs_return_code());
        returnStatus.setWsReturnDescription(wsReturnStatus.getWs_return_description());

        returnResult.setWsStatus(returnStatus);

        if(wsSearchPolicyInfoResult.getWs_return_data() != null) {

            List<WsReturnData> wsReturnDatas = wsSearchPolicyInfoResult.getWs_return_data();

            for(WsReturnData wsReturnData : wsReturnDatas) {

                Policy policy = new Policy();

                policy.setBatchNumber(wsReturnData.getBatch_number());
                policy.setPolicyNo(wsReturnData.getPolicy_no());
                policy.setEntryDate(wsReturnData.getEntry_date());
                policy.setApplicationDate(wsReturnData.getApplication_date());
                policy.setSignDate(wsReturnData.getSign_date());
                policy.setPostDate(wsReturnData.getPost_date());
                policy.setPolicyStatus(wsReturnData.getPolicy_status());
                policy.setRenewalFlag(wsReturnData.getRenewal_flag());
                policy.setLastEndorsementNo(wsReturnData.getLast_endorsement_no());
                policy.setAgentCode(wsReturnData.getAgent_code());
                policy.setVehicleChassisNo(wsReturnData.getVehicle_chassis_no());
                policy.setWsCallFrom(wsReturnData.getWs_call_from());

                policies.add(policy);

                if(getPolicies(policy.getPolicyNo()).isEmpty()) {

                    policyDao.insertPolicy(policy);
                }
            }
        }

        returnResult.setResults(policies);

        return returnResult;
    }

    @Override
    public List<PolicyInfo> getPolicyInfos(GetPolicyInfoRequest getPolicyInfoRequest) {

        List<PolicyInfo> policyInfos = new ArrayList<>();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<GetPolicyInfoRequest> entityPolicyInfo = new HttpEntity<>(getPolicyInfoRequest, headers);
        WsSearchPolicyInfoAutoResult wsSearchPolicyInfoAutoResult = restTemplate.postForObject(RESTUri + GeneralConfig.searchPolicyInfoAuto, entityPolicyInfo, WsSearchPolicyInfoAutoResult.class);

        WsGetCustomerDetailsRequest wsGetCustomerDetailsRequest = new WsGetCustomerDetailsRequest();
        wsGetCustomerDetailsRequest.setReferenceNo(getPolicyInfoRequest.getPolicyNo());
        wsGetCustomerDetailsRequest.setCaller("Neo");
        wsGetCustomerDetailsRequest.setInsuredCode(wsSearchPolicyInfoAutoResult.getPolicy_info().getInsured_code());
        HttpEntity<WsGetCustomerDetailsRequest> entityCustomerDetails = new HttpEntity<>(wsGetCustomerDetailsRequest, headers);
        WsGetCustomerDetailsResponse wsGetCustomerDetailsResponse = restTemplate.postForObject(GeneralConfig.getCustomerDetails, entityCustomerDetails, WsGetCustomerDetailsResponse.class);

        WsPolicyInfo wsPolicyInfo = wsSearchPolicyInfoAutoResult.getPolicy_info();

        if(wsPolicyInfo != null) {

            PolicyInfo policyInfo = new PolicyInfo();
            policyInfo.setBatchNumber(wsPolicyInfo.getBatch_number());
            policyInfo.setBatchKey(wsPolicyInfo.getBatch_key());
            policyInfo.setReferenceNo(wsPolicyInfo.getReference_no());
            policyInfo.setPolicyNo(wsPolicyInfo.getPolicy_no());
            policyInfo.setOfficeCode(wsPolicyInfo.getOffice_code());
            policyInfo.setPolicyEffectiveDate(wsPolicyInfo.getPolicy_effective_date());
            policyInfo.setPolicyExpirationDate(wsPolicyInfo.getPolicy_expiration_date());
            policyInfo.setTotalDay(wsPolicyInfo.getTotal_day());
            policyInfo.setAgentCode(wsPolicyInfo.getAgent_code());
            policyInfo.setAgentName(wsPolicyInfo.getAgent_name());
            policyInfo.setFinanceAgentTypeFlag(wsPolicyInfo.getFinance_agent_type_flag());
            policyInfo.setDirectAgentTypeFlag(wsPolicyInfo.getDirect_agent_type_flag());
            policyInfo.setAgentTypeKey(wsPolicyInfo.getAgent_type_key());
            policyInfo.setBrokerCode(wsPolicyInfo.getBroker_code());
            policyInfo.setBrokerName(wsPolicyInfo.getBroker_name());
            policyInfo.setParentAgentCode(wsPolicyInfo.getParent_agent_code());
            policyInfo.setParentAgentName(wsPolicyInfo.getParent_agent_name());
            policyInfo.setSubAgentCode(wsPolicyInfo.getSub_agent_code());
            policyInfo.setSubAgentName(wsPolicyInfo.getSub_agent_name());
            policyInfo.setGprm(wsPolicyInfo.getGprm());
            policyInfo.setTax(wsPolicyInfo.getTax());
            policyInfo.setStamp(wsPolicyInfo.getStamp());
            policyInfo.setTotalPremium(wsPolicyInfo.getTotal_premium());
            policyInfo.setSignDate(wsPolicyInfo.getSign_date());
            policyInfo.setEntryDate(wsPolicyInfo.getEntry_date());
            policyInfo.setInsuredCode(wsPolicyInfo.getInsured_code());
            policyInfo.setInsuredType(wsPolicyInfo.getInsured_type());
            policyInfo.setTitle(wsPolicyInfo.getTitle());
            policyInfo.setInsuredName(wsPolicyInfo.getInsured_name());
            policyInfo.setOccupationCode(wsPolicyInfo.getOccupation_code());
            policyInfo.setOccupation(wsPolicyInfo.getOccupation());
            policyInfo.setAddressLine1(wsPolicyInfo.getAddress_line1());
            policyInfo.setTumbon(wsPolicyInfo.getTumbon());
            policyInfo.setDistrict(wsPolicyInfo.getDistrict());
            policyInfo.setProvince(wsPolicyInfo.getProvince());
            policyInfo.setPostalCode(wsPolicyInfo.getPostal_code());
            policyInfo.setMotorCode(wsPolicyInfo.getMotor_code());
            policyInfo.setMotorSubCode(wsPolicyInfo.getMotor_sub_code());
            policyInfo.setMotorCodeInfo(wsPolicyInfo.getMotor_code_info());
            policyInfo.setVehiclePrefixLicenseNo(wsPolicyInfo.getVehicle_prefix_license_no());
            policyInfo.setVehicleLicenseNo(wsPolicyInfo.getVehicle_license_no());
            policyInfo.setVehicleProvince(wsPolicyInfo.getVehicle_province());
            policyInfo.setKeyField(wsPolicyInfo.getKey_field());
            policyInfo.setVehicleChassisNo(wsPolicyInfo.getVehicle_chassis_no());
            policyInfo.setVehicleEngineNo(wsPolicyInfo.getVehicle_engine_no());
            policyInfo.setVehicleCapacityKey(wsPolicyInfo.getVehicle_capacity_key());
            policyInfo.setGroupOfVehicle(wsPolicyInfo.getGroup_of_vehicle());
            policyInfo.setVehicleRegYear(wsPolicyInfo.getVehicle_reg_year());
            policyInfo.setAdditionalEquipmentFlag(wsPolicyInfo.getAdditional_equipment_flag());
            policyInfo.setVehicleNoOfSeat(wsPolicyInfo.getVehicle_no_of_seat());
            policyInfo.setCountClaim(wsPolicyInfo.getCount_claim());
            policyInfo.setLinkPolicyNo(wsPolicyInfo.getLink_policy_no());
            policyInfo.setLinkPolicyEffectiveDate(wsPolicyInfo.getLink_policy_effective_date());
            policyInfo.setLinkPolicyExpirationDate(wsPolicyInfo.getLink_policy_expiration_date());
            policyInfo.setLinkPolicyStatus(wsPolicyInfo.getLink_policy_status());
            policyInfo.setBasicPremium(wsPolicyInfo.getBasic_premium());
            policyInfo.setSumInsured(wsPolicyInfo.getSum_insured());
            policyInfo.setAv5BasicPremium(wsPolicyInfo.getAv5_basic_premium());
            policyInfo.setAv5SumInsured(wsPolicyInfo.getAv5_sum_insured());
            policyInfo.setOdavAmount(wsPolicyInfo.getOdav_amount());
            policyInfo.setFtavAmount(wsPolicyInfo.getFtav_amount());
            policyInfo.setTpbiavPersonAmount(wsPolicyInfo.getTpbiav_person_amount());
            policyInfo.setTpbiavAccidentAmount(wsPolicyInfo.getTpbiav_accident_amount());
            policyInfo.setTppdavAmount(wsPolicyInfo.getTppdav_amount());
            policyInfo.setSiprPremium(wsPolicyInfo.getSipr_premium());
            policyInfo.setAcovpdAmount(wsPolicyInfo.getAcovpd_amount());
            policyInfo.setAcovpdPremium(wsPolicyInfo.getAcovpd_premium());
            policyInfo.setAcovppAmount(wsPolicyInfo.getAcovpp_amount());
            policyInfo.setAcovppPremium(wsPolicyInfo.getAcovpp_premium());
            policyInfo.setAcovtdAmount(wsPolicyInfo.getAcovtd_amount());
            policyInfo.setAcovtdPremium(wsPolicyInfo.getAcovtd_premium());
            policyInfo.setAcovtpAmount(wsPolicyInfo.getAcovtp_amount());
            policyInfo.setAcovtpPremium(wsPolicyInfo.getAcovtp_premium());
            policyInfo.setGroupOfVehicle(wsPolicyInfo.getGroup_of_vehicle());
            policyInfo.setAcovmeAmount(wsPolicyInfo.getAcovme_amount());
            policyInfo.setAcovmePremium(wsPolicyInfo.getAcovme_premium());
            policyInfo.setAcovmpAmount(wsPolicyInfo.getAcovmp_amount());
            policyInfo.setAcovmpPremium(wsPolicyInfo.getAcovmp_premium());
            policyInfo.setAcovbbAmount(wsPolicyInfo.getAcovbb_amount());
            policyInfo.setAcovbbPremium(wsPolicyInfo.getAcovbb_premium());
            policyInfo.setAcovodAmount(wsPolicyInfo.getAcovod_amount());
            policyInfo.setAcovthAmount(wsPolicyInfo.getAcovth_amount());
            policyInfo.setAcovfdPercent(wsPolicyInfo.getAcovfd_percent());
            policyInfo.setAcovfdAmount(wsPolicyInfo.getAcovfd_amount());
            policyInfo.setAcovedPercent(wsPolicyInfo.getAcoved_percent());
            policyInfo.setAcovedAmount(wsPolicyInfo.getAcoved_amount());
            policyInfo.setAcovotPercent(wsPolicyInfo.getAcovot_percent());
            policyInfo.setAcovotAmount(wsPolicyInfo.getAcovot_amount());
            policyInfo.setAcoveaPercent(wsPolicyInfo.getAcovea_percent());
            policyInfo.setAcoveaAmount(wsPolicyInfo.getAcovea_amount());
            policyInfo.setVehicleMake(wsPolicyInfo.getVehicle_make());
            policyInfo.setVehicleModel(wsPolicyInfo.getVehicle_model());
            policyInfo.setVehicleBodyType(wsPolicyInfo.getVehicle_body_type());
            policyInfo.setVehicleEngineCapacity(wsPolicyInfo.getVehicle_engine_capacity());
            policyInfo.setVehicleGarageLocation(wsPolicyInfo.getVehicle_garage_location());
            policyInfo.setVehicleModelType(wsPolicyInfo.getVehicle_model_type());
            policyInfo.setEnglishNameFlag(wsPolicyInfo.getEnglish_name_flag());
            policyInfo.setWsCallFrom(wsPolicyInfo.getWs_call_from());
            policyInfo.setMotorBodyType(wsPolicyInfo.getMotor_body_type());
            policyInfo.setEpolicyFlag(wsPolicyInfo.getEpolicy_flag());
            policyInfo.setPolicyStatus(wsPolicyInfo.getPolicy_status());

            List<MemoInfo> memoInfos = new ArrayList<>();

            if(wsSearchPolicyInfoAutoResult.getMemo_info() != null) {

                WsMemoInfo wsMemoInfo = wsSearchPolicyInfoAutoResult.getMemo_info();

                MemoInfo memoInfo = new MemoInfo();
                memoInfo.setBatchNumber(wsMemoInfo.getBatch_number());
                memoInfo.setBatchKey(wsMemoInfo.getBatch_key());
                memoInfo.setReferenceNo(wsMemoInfo.getReference_no());
                memoInfo.setPolicyNo(wsMemoInfo.getPolicy_no());
                memoInfo.setMemoCode(wsMemoInfo.getMemo_code());
                memoInfo.setMemoHeader(wsMemoInfo.getMemo_header());
                memoInfo.setMemoDetail(wsMemoInfo.getMemo_detail());

                memoInfos.add(memoInfo);
            }

            policyInfo.setMemoInfos(memoInfos);

            List<EndorsementInfo> endorsementInfos = new ArrayList<>();

            if(wsSearchPolicyInfoAutoResult.getEndorsement_info() != null) {

                List<WsEndorsementInfo> wsEndorsementInfos = wsSearchPolicyInfoAutoResult.getEndorsement_info();

                for(WsEndorsementInfo wsEndorsementInfo : wsEndorsementInfos) {

                    EndorsementInfo endorsementInfo = new EndorsementInfo();
                    endorsementInfo.setBatchNumber(wsEndorsementInfo.getBatch_number());
                    endorsementInfo.setBatchKey(wsEndorsementInfo.getBatch_key());
                    endorsementInfo.setReferenceNo(wsEndorsementInfo.getReference_no());
                    endorsementInfo.setPolicyNo(wsEndorsementInfo.getPolicy_no());
                    endorsementInfo.setPolicyEndorsementNo(wsEndorsementInfo.getPolicy_endorsement_no());
                    endorsementInfo.setEndorsementType(wsEndorsementInfo.getEndorsement_type());

                    endorsementInfos.add(endorsementInfo);
                }
            }

            policyInfo.setEndorsementInfos(endorsementInfos);

            policyInfo.setWsCustomerInfo(wsGetCustomerDetailsResponse.getWsCustomerInfo());

            policyInfos.add(policyInfo);
        }

        return policyInfos;
    }

    @Override
    public PolicyDetailsResponse getPolicyDetails(PolicyDetailsRequest policyDetailsRequest) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<PolicyDetailsRequest> entityPolicyDetails = new HttpEntity<>(policyDetailsRequest, headers);
        WsPolicyDetailsResponse wsPolicyDetailsResponse = restTemplate.postForObject(GeneralConfig.getPolicyDetails, entityPolicyDetails, WsPolicyDetailsResponse.class);

        WsGetCustomerDetailsRequest wsGetCustomerDetailsRequest = new WsGetCustomerDetailsRequest();
        wsGetCustomerDetailsRequest.setReferenceNo(policyDetailsRequest.getPOLICY_NO());
        wsGetCustomerDetailsRequest.setCaller("Neo");
        wsGetCustomerDetailsRequest.setInsuredCode(wsPolicyDetailsResponse.getPolicy_Detail().get(0).getINSURED_CODE());
        HttpEntity<WsGetCustomerDetailsRequest> entityCustomerDetails = new HttpEntity<>(wsGetCustomerDetailsRequest, headers);
        WsGetCustomerDetailsResponse wsGetCustomerDetailsResponse = restTemplate.postForObject(GeneralConfig.getCustomerDetails, entityCustomerDetails, WsGetCustomerDetailsResponse.class);

        PolicyDetailsResponse policyDetailsResponse = new PolicyDetailsResponse();
        policyDetailsResponse.setEndorsement_Info(wsPolicyDetailsResponse.getEndorsement_Info());
        policyDetailsResponse.setMemo_Info(wsPolicyDetailsResponse.getMemo_Info());
        policyDetailsResponse.setPolicy_Detail(wsPolicyDetailsResponse.getPolicy_Detail());
        policyDetailsResponse.setPolicy_History(wsPolicyDetailsResponse.getPolicy_History());
        policyDetailsResponse.setPolicy_Renewal_History(wsPolicyDetailsResponse.getPolicy_Renewal_History());
        policyDetailsResponse.setWsCustomerInfo(wsGetCustomerDetailsResponse.getWsCustomerInfo());

        return policyDetailsResponse;
    }

    @Override
    public ApiResponse addEndorsement(String policyNo, AddEndorsementRequest addEndorsementRequest) {

        ApiResponse response = null;

        if(!checkExistedUnapprovedEndorsementByPolicyNo(policyNo)) {

            PolicyEndorsement policyEndorsement = new PolicyEndorsement();
            policyEndorsement.setPolicyNo(policyNo);
            policyEndorsement.setEndorsementType(addEndorsementRequest.getEndorsementType());
            policyEndorsement.setContent(addEndorsementRequest.getContent());
            policyEndorsement.setEndorsementDetails(null);
            policyEndorsement.setStatus(PolicyEndorsementStatus.UNAPPROVED.toString());
            policyEndorsement.setCreator(UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null);
            policyEndorsement.setCreatorEmail(UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getEmail() : null);
            policyEndorsement.setCreatedDate(DateUtility.now());
            policyEndorsement.setSubmitter(null);
            policyEndorsement.setSubmitterEmail(null);
            policyEndorsement.setSubmittedDate(null);
            policyEndorsement.setApprover(null);
            policyEndorsement.setApproverEmail(null);
            policyEndorsement.setApprovedDate(null);
            policyEndorsement.setRejecter(null);
            policyEndorsement.setRejecterEmail(null);
            policyEndorsement.setRejectedReason(null);
            policyEndorsement.setRejectedDate(null);

            policyDao.insertEndorsement(policyEndorsement);

            List<PolicyEndorsement> results = policyDao.getEndorsements(policyNo, addEndorsementRequest.getEndorsementType());

            response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null);
        } else {

            List<PolicyEndorsement> results = policyDao.getUnapprovedEndorsementsByPolicyNo(policyNo);

            response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "There is an unapproved endorsement. Please process it before creating a new endorsement. Thank you!", results.size(), results, null);
        }

        return response;
    }

    @Override
    public List<PolicyEndorsement> updateEndorsement(String policyNo, UpdateEndorsementRequest updateEndorsementRequest) {

        PolicyEndorsement policyEndorsement = new PolicyEndorsement();
        policyEndorsement.setPolicyNo(policyNo);
        policyEndorsement.setContent(updateEndorsementRequest.getContent());

        if(checkExistedUnapprovedEndorsementByPolicyNo(policyNo)) {

            if(updateEndorsementRequest.getStatus().equals(PolicyEndorsementStatus.WAITING_FOR_APPROVAL.toString())) {

                policyEndorsement.setStatus(PolicyEndorsementStatus.WAITING_FOR_APPROVAL.toString());
                policyEndorsement.setSubmitter(UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null);
                policyEndorsement.setSubmitterEmail(UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getEmail() : null);
                policyEndorsement.setSubmittedDate(DateUtility.now());
            }

            if(policyDao.submitEndorsementForApproval(policyEndorsement)) {

                savedAsDraftService.deleteByPolicyNo(policyNo);
            }

            return policyDao.getSubmittedEndorsementsByPolicyNo(policyNo);
        }

        if(checkExistedSubmittedEndorsementByPolicyNo(policyNo)) {

            if(updateEndorsementRequest.getStatus().equals(PolicyEndorsementStatus.REJECTED.toString())) {

                policyEndorsement.setContent(updateEndorsementRequest.getContent());
                policyEndorsement.setEndorsementDetails(null);
                policyEndorsement.setStatus(PolicyEndorsementStatus.REJECTED.toString());
                policyEndorsement.setRejecter(UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null);
                policyEndorsement.setRejecterEmail(UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getEmail() : null);
                policyEndorsement.setRejectedReason(updateEndorsementRequest.getReason());
                policyEndorsement.setRejectedDate(DateUtility.now());
                policyEndorsement.setApprover(null);
                policyEndorsement.setApproverEmail(null);
                policyEndorsement.setApprovedDate(null);
            }

            policyDao.rejectEndorsement(policyEndorsement);

            return policyDao.getRejectedEndorsementsByPolicyNo(policyNo);
        }

        return getEndorsementsByPolicyNo(policyNo, 0);
    }

    @Override
    public CreateEndorsementResponse createEndorsement(String policyNo, Integer endorsable, CreateEndorsementRequest createEndorsementRequest) {

        CreateEndorsementResponse createEndorsementResponse = new CreateEndorsementResponse();

        List<PolicyEndorsement> policyEndorsements = null;

        if(endorsable == null) {

            policyEndorsements = policyDao.getSubmittedEndorsementsByPolicyNo(policyNo);
        } else {

            policyEndorsements = policyDao.getUnapprovedEndorsementsByPolicyNo(policyNo);
        }

        if(!policyEndorsements.isEmpty()) {

            PolicyEndorsement policyEndorsement = policyEndorsements.get(0);

            PolicyDetailsRequest policyDetailsRequest = new PolicyDetailsRequest();
            policyDetailsRequest.setPOLICY_NO(policyNo);
            policyDetailsRequest.setCALLER("NEO");
            policyDetailsRequest.setREFERENCE_NO(policyNo);
            policyDetailsRequest.setMK_INFO_FLAG("Y");
            PolicyDetailsResponse policyDetailsResponse = getPolicyDetails(policyDetailsRequest);

            WsAegisCreateEndorsementRequest wsAegisCreateEndorsementRequest = new WsAegisCreateEndorsementRequest();
            WsAegisEndorsementInfo wsAegisEndorsementInfo = new WsAegisEndorsementInfo();
            wsAegisEndorsementInfo.setReferenceNo(policyNo);
            wsAegisEndorsementInfo.setCaller("NEO");
            wsAegisEndorsementInfo.setWsCallFrom("NEOEN");
            wsAegisEndorsementInfo.setUserId(UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : "");
            wsAegisEndorsementInfo.setPolicyLineOfBusiness(policyNo.substring(3, 5));
            wsAegisEndorsementInfo.setPolicyNo(policyNo);
            wsAegisEndorsementInfo.setRorjorNo(EndorsementType.CHI.rorjorNo);
            wsAegisEndorsementInfo.setEntryDate(policyDetailsResponse.getPolicy_Detail().get(0).getENTRY_DATE());
            wsAegisEndorsementInfo.setSignDate(policyDetailsResponse.getPolicy_Detail().get(0).getSIGN_DATE());
            wsAegisEndorsementInfo.setPolicyEffectiveDate(createEndorsementRequest.getEffectiveDate());
            wsAegisEndorsementInfo.setCountryCode("");
            wsAegisEndorsementInfo.setRemarks(createEndorsementRequest.getRemarks());
            wsAegisEndorsementInfo.setEndorsementReason(createEndorsementRequest.getRemarks());
            //Fix for License Province  to pass the KEY_FIELD VALUE
            //wsAegisEndorsementInfo.setKeyField(policyDetailsResponse.getPolicy_Detail().get(0).getKEY_FIELD());
            wsAegisEndorsementInfo.setKeyField(createEndorsementRequest.getLicenseProvince());
            wsAegisEndorsementInfo.setInformToChange(createEndorsementRequest.getInformToChange());
            wsAegisEndorsementInfo.setVehicleBodyType(policyDetailsResponse.getPolicy_Detail().get(0).getVEHICLE_BODY_TYPE());
            wsAegisEndorsementInfo.setVehicleCapacityKey(policyDetailsResponse.getPolicy_Detail().get(0).getVEHICLE_CAPACITY_KEY());
            wsAegisEndorsementInfo.setVehicleChassisNo(createEndorsementRequest.getChassisNo());
            wsAegisEndorsementInfo.setVehicleCodeDesc(policyDetailsResponse.getPolicy_Detail().get(0).getVEHICLE_CODE_DESC());
            wsAegisEndorsementInfo.setVehicleColor(createEndorsementRequest.getColor());
            wsAegisEndorsementInfo.setVehicleEngineCapacity(createEndorsementRequest.getCc());
            wsAegisEndorsementInfo.setVehicleEngineNo(createEndorsementRequest.getEngineNo());
            wsAegisEndorsementInfo.setVehicleGarageLocation(policyDetailsResponse.getPolicy_Detail().get(0).getVEHICLE_GARAGE_LOCATION());
            wsAegisEndorsementInfo.setVehicleLicenseNo(createEndorsementRequest.getLicenseNo());
            wsAegisEndorsementInfo.setVehicleMake(createEndorsementRequest.getMake());
            wsAegisEndorsementInfo.setVehicleManufactureYear(policyDetailsResponse.getPolicy_Detail().get(0).getVEHICLE_MANUFACTURE_YEAR());
            wsAegisEndorsementInfo.setVehicleModel(createEndorsementRequest.getModel());
            wsAegisEndorsementInfo.setVehicleModelType(createEndorsementRequest.getSubModel());
            wsAegisEndorsementInfo.setVehicleNoOfSeat(createEndorsementRequest.getNumberOfSeat());
            wsAegisEndorsementInfo.setVehiclePrefixLicenseNo(createEndorsementRequest.getLicensePrefix());
            //wsAegisEndorsementInfo.setVehicleProvince(createEndorsementRequest.getLicenseProvince());
            wsAegisEndorsementInfo.setVehicleRegYear(createEndorsementRequest.getYear());
            wsAegisEndorsementInfo.setVehicleStickerNo(policyDetailsResponse.getPolicy_Detail().get(0).getVEHICLE_STICKER_NO());
            wsAegisEndorsementInfo.setVehicleWeight(createEndorsementRequest.getWeight());
            wsAegisCreateEndorsementRequest.setEndorsementInfo(wsAegisEndorsementInfo);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

            HttpEntity<WsAegisCreateEndorsementRequest> entity = new HttpEntity<>(wsAegisCreateEndorsementRequest, headers);

            WsAegisCreateEndorsementResponse wsAegisCreateEndorsementResponse = restTemplate.postForObject(RESTUri + GeneralConfig.createEndorsement, entity, WsAegisCreateEndorsementResponse.class);
            createEndorsementResponse.setWsStatus(wsAegisCreateEndorsementResponse.getWsReturnCode());

            if(wsAegisCreateEndorsementResponse.getWsReturnCode().getWsReturnCode().equals("1")) {

                Gson gson = new Gson();
                String content = gson.toJson(createEndorsementRequest);
                String endorsementDetails = gson.toJson(wsAegisCreateEndorsementResponse);

                policyEndorsement.setContent(content);
                policyEndorsement.setEndorsementDetails(endorsementDetails);
                policyEndorsement.setStatus(PolicyEndorsementStatus.APPROVED.toString());
                policyEndorsement.setApprover(UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null);
                policyEndorsement.setApproverEmail(UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getEmail() : null);
                policyEndorsement.setApprovedDate(DateUtility.now());
                policyEndorsement.setRejecter(null);
                policyEndorsement.setRejecterEmail(null);
                policyEndorsement.setRejectedDate(null);

                savedAsDraftService.deleteByPolicyNo(policyNo);

                policyDao.approveEndorsement(policyEndorsement);

                createEndorsementResponse.setResults(policyDao.getApprovedEndorsementsByPolicyNo(policyNo));
            } else {

                createEndorsementResponse.setResults(new ArrayList<>());
            }
        } else {

            createEndorsementResponse.setResults(new ArrayList<>());
        }

        return createEndorsementResponse;
    }

    @Override
    public boolean delete(String policyNo, Integer endorsementId) {

        savedAsDraftService.deleteByPolicyNo(policyNo);

        return policyDao.delete(policyNo, endorsementId);
    }

    @Override
    public List<PolicyEndorsement> getAllEndorsements() {

        String creator = UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null;

        return policyDao.getAllEndorsements(creator);
    }

    @Override
    public List<PolicyEndorsement> getAllEndorsements(Integer gettable) {

        String creator = UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null;

        return policyDao.getAllEndorsements(creator, gettable);
    }

    @Override
    public List<PolicyEndorsement> getAllEndorsementsByStatus(String status) {

        return policyDao.getAllEndorsementsByStatus(status);
    }

    @Override
    public List<PolicyEndorsement> getEndorsementsByPolicyNo(String policyNo, Integer gettable) {

        String creator = UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null;

        return policyDao.getEndorsementsByPolicyNo(policyNo, creator, gettable);
    }

    @Override
    public List<PolicyEndorsement> getEndorsementsByStatus(String policyNo, String status) {

        return policyDao.getEndorsementsByStatus(policyNo, status);
    }

    @Override
    public List<CheckEndorsementStatusResponse> checkEndorsementStatus(CheckEndorsementStatusRequest checkEndorsementStatusRequest) {

        WsCheckOKToEndorseAutoRequest wsCheckOKToEndorseAutoRequest = new WsCheckOKToEndorseAutoRequest();
        wsCheckOKToEndorseAutoRequest.setREFERENCE_NO(checkEndorsementStatusRequest.getReferenceNo());
        wsCheckOKToEndorseAutoRequest.setCALLER(checkEndorsementStatusRequest.getCaller());
        wsCheckOKToEndorseAutoRequest.setPOLICY_NO(checkEndorsementStatusRequest.getPolicyNo());
        wsCheckOKToEndorseAutoRequest.setRORJOR_NO(checkEndorsementStatusRequest.getRorjorNo());

        List<CheckEndorsementStatusResponse> checkEndorsementStatuses = new ArrayList<>();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<WsCheckOKToEndorseAutoRequest> entity = new HttpEntity<>(wsCheckOKToEndorseAutoRequest, headers);

        WsCheckOKToEndorseAutoResponse wsCheckOKToEndorseAutoResponse = restTemplate.postForObject(RESTUri + GeneralConfig.checkEndorsementStatus, entity, WsCheckOKToEndorseAutoResponse.class);

        CheckEndorsementStatusResponse checkEndorsementStatusResponse = new CheckEndorsementStatusResponse();
        checkEndorsementStatusResponse.setBatchNumber(wsCheckOKToEndorseAutoResponse.getBATCH_NUMBER());
        checkEndorsementStatusResponse.setBatchKey(wsCheckOKToEndorseAutoResponse.getBATCH_KEY());
        checkEndorsementStatusResponse.setReferenceNo(wsCheckOKToEndorseAutoResponse.getREFERENCE_NO());
        checkEndorsementStatusResponse.setWsReturnCode(wsCheckOKToEndorseAutoResponse.getWS_RETURN_CODE());
        checkEndorsementStatusResponse.setWsReturnDescription(wsCheckOKToEndorseAutoResponse.getWS_RETURN_DESCRIPTION());
        checkEndorsementStatusResponse.setResultCode(wsCheckOKToEndorseAutoResponse.getRESULT_CODE());
        checkEndorsementStatusResponse.setPolicyNoPar(wsCheckOKToEndorseAutoResponse.getPOLICY_NO_PAR());
        checkEndorsementStatusResponse.setRorjorNoPar(wsCheckOKToEndorseAutoResponse.getRORJOR_NO_PAR());
        checkEndorsementStatusResponse.setResultDescription(wsCheckOKToEndorseAutoResponse.getRESULT_DESCRIPTION());

        checkEndorsementStatuses.add(checkEndorsementStatusResponse);

        return checkEndorsementStatuses;
    }

    public boolean checkExistedUnapprovedEndorsementByPolicyNo(String policyNo) {

        List<PolicyEndorsement> results = policyDao.getUnapprovedEndorsementsByPolicyNo(policyNo);

        return !results.isEmpty();
    }

    public boolean checkExistedSubmittedEndorsementByPolicyNo(String policyNo) {

        List<PolicyEndorsement> results = policyDao.getSubmittedEndorsementsByPolicyNo(policyNo);

        return !results.isEmpty();
    }
}
