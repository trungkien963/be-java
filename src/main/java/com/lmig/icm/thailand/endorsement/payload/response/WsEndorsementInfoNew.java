package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsEndorsementInfoNew {

    @JsonProperty("ENDORSEMENT_REASON")
    private String ENDORSEMENT_REASON;

    @JsonProperty("ENDORSEMENT_TYPE")
    private String ENDORSEMENT_TYPE;

    @JsonProperty("ENTRY_DATE")
    private String ENTRY_DATE;

    @JsonProperty("GPRM")
    private Float GPRM;

    @JsonProperty("POLICY_ENDORSEMENT_NO")
    private String POLICY_ENDORSEMENT_NO;

    @JsonProperty("POLICY_NO")
    private String POLICY_NO;

    @JsonProperty("RORJOR_NO")
    private String RORJOR_NO;
}
