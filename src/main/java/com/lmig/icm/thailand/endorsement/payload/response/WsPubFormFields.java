package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsPubFormFields {

    @JsonProperty("Thailand.Endorsement.Sample.CompanyName")
    private String Thailand_Endorsement_Sample_CompanyName;

    @JsonProperty("Thailand.Endorsement.Sample.RecipientName")
    private String Thailand_Endorsement_Sample_RecipientName;

    @JsonProperty("Thailand.Endorsement.Sample.Surname")
    private String Thailand_Endorsement_Sample_Surname;

    @JsonProperty("Thailand.Endorsement.Sample.CompanyAddress")
    private String Thailand_Endorsement_Sample_CompanyAddress;

    @JsonProperty("Thailand.Endorsement.Sample.RecipientAddress")
    private String Thailand_Endorsement_Sample_RecipientAddress;

    @JsonProperty("Thailand.Endorsement.Sample.RecipientCity")
    private String Thailand_Endorsement_Sample_RecipientCity;
}
