package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsEndorsementInfo {

    private String batch_number;

    private String batch_key;

    private String reference_no;

    private String policy_no;

    private String policy_endorsement_no;

    private String endorsement_type;

    private String rorjor_no;
}
