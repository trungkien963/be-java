package com.lmig.icm.thailand.endorsement.enumType;

public enum EndorsementType {
    CHI("17"),
    CHU("23"),
    BNF("24"),
    CNP("11"),
    CNC("12"),
    OTH("99"),
    CHP("19"),
    XPR("05"),
    XTR("04"),
    PLC("18"),
    CHS("20"),
    CHV("25"),
    NPE("");

    public final String rorjorNo;

    private EndorsementType(String rorjorNo) {
        this.rorjorNo = rorjorNo;
    }
}
