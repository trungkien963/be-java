package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PubFormFields {

    private String companyName;

    private String recipientName;

    private String surname;

    private String companyAddress;

    private String recipientAddress;

    private String recipientCity;
}
