package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NebulaLoginResponse {

    private String refreshTokenExpiresIn;

    private String apiProductList;

    private List<String> apiProductListJson;

    private String organizationName;

    private String developerEmail;

    private String tokenType;

    private String issuedAt;

    private String clientId;

    private String accessToken;

    private String applicationName;

    private String scope;

    private String expiresIn;

    private String refreshCount;

    private String status;
}
