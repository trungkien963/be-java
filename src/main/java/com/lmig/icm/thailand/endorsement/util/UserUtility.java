package com.lmig.icm.thailand.endorsement.util;

import com.lmig.icm.thailand.endorsement.config.GeneralConfig;
import com.lmig.icm.thailand.endorsement.payload.response.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Arrays;

@Component
public class UserUtility {

    private static String auth0_static;

    @Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}")
    public void setAuth0Static(String auth0){

        UserUtility.auth0_static = auth0;
    }

    private static RestTemplate restTemplate_static;

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        UserUtility.restTemplate_static = restTemplate;
    }

    public static UserInfo getUserInfo() {

        String token = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getHeader("Authorization");

        if(token != null) {

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.set("Authorization", token);

            HttpEntity entity = new HttpEntity(headers);

            ResponseEntity<UserInfo> response = restTemplate_static.exchange(auth0_static + GeneralConfig.auth0_userinfo, HttpMethod.GET, entity, UserInfo.class);

            return response.getBody();
        }

        return null;
    }
}
