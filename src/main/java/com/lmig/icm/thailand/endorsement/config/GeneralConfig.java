package com.lmig.icm.thailand.endorsement.config;

public class GeneralConfig {

    public final static String auth0_userinfo = "userinfo";

    public final static String searchPolicyInfo = "/searchPolicyInfo";

    public final static String searchPolicyInfoAuto = "/searchPolicyInfoAuto";

    public final static String checkEndorsementStatus = "/endorsements/status";

    public final static String createEndorsement = "/endorsements/endorse";

    public final static String getPolicyDetails = "https://ebizuatws2.lmig.com:8443/Webservice/iNeo/wcfSearchInfoMK/MKService.svc/GetPolicyDetail";

    public final static String getCustomerDetails = "https://ebizws2.lmig.com:8443/Webservice/iNeo/wcfSearchInfoMK/MKService.svc/GetCustomerDetail";
}
