package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ApiResponse<T> {

    private Integer code;
    private String type;
    private String message;
    private Integer count;
    private List<T> results;
    private T wsStatus;

    public ApiResponse(Integer code, String type, String message, Integer count, List<T> results, T wsStatus) {

        this.setCode(code);
        this.setType(type);
        this.setMessage(message);
        this.setCount(count);
        this.setResults(results);
        this.setWsStatus(wsStatus);
    }
}
