package com.lmig.icm.thailand.endorsement.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsEarnixCoverageRequest {

    private String coverageName;

    private String annualizedCovPremiumExpiring;

    private String annualizedCovPremiumDriverExpiring;

    private String annualizedCovPremiumPassengerExpiring;

    private String covPremiumExpiring;

    private String covPremiumDriverExpiring;

    private String covPremiumPassengerExpiring;

    private String covPremiumUser;

    private String covPremiumDriverUser;

    private String covPremiumPassengerUser;

    private String covSumInsuredUser;

    private String covSumInsuredExpiring;

    private String deductibleAmountUser;

    private String deductibleAmountExpiring;
}
