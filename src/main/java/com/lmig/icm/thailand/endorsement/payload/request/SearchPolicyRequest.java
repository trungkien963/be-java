package com.lmig.icm.thailand.endorsement.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SearchPolicyRequest {

    @JsonProperty("policy_no_par")
    private String policyNoPar;

    @JsonProperty("vehicle_prefix_license_no_par")
    private String vehiclePrefixLicenseNoPar;

    @JsonProperty("province_license_no_par")
    private String provinceLicenseNoPar;

    @JsonProperty("insured_name_par")
    private String insuredNamePar;

    @JsonProperty("date_from_par")
    private String dateFromPar;

    @JsonProperty("date_to_par")
    private String dateToPar;

    @JsonProperty("caller")
    private String caller;

    @JsonProperty("corp_id_par")
    private String corpIdPar;

    @JsonProperty("policy_line_of_business_par")
    private String policyLineOfBusinessPar;

    @JsonProperty("policy_serial_number_par")
    private String policySerialNumberPar;

    @JsonProperty("policy_serial_suffix_par")
    private String policySerialSuffixPar;

    @JsonProperty("policy_effective_year_par")
    private String policyEffectiveYearPar;

    @JsonProperty("policy_effective_month_par")
    private String policyEffectiveMonthPar;

    @JsonProperty("agent_code")
    private String agentCode;

    @JsonProperty("vehicle_chassis_no")
    private String vehicleChassisNo;

    public String getPolicyNoPar() {
        return (this.policyNoPar == null ? "" : this.policyNoPar);
    }

    public String getVehiclePrefixLicenseNoPar() {
        return (this.vehiclePrefixLicenseNoPar == null ? "" : this.vehiclePrefixLicenseNoPar);
    }

    public String getProvinceLicenseNoPar() {
        return (this.provinceLicenseNoPar == null ? "" : this.provinceLicenseNoPar);
    }

    public String getInsuredNamePar() {
        return (this.insuredNamePar == null ? "" : this.insuredNamePar);
    }

    public String getDateFromPar() {
        return (this.dateFromPar == null ? "" : this.dateFromPar);
    }

    public String getDateToPar() {
        return (this.dateToPar == null ? "" : this.dateToPar);
    }

    public String getCaller() {
        return (this.caller == null ? "" : this.caller);
    }

    public String getCorpIdPar() {
        return (this.corpIdPar == null ? "" : this.corpIdPar);
    }

    public String getPolicyLineOfBusinessPar() {
        return (this.policyLineOfBusinessPar == null ? "" : this.policyLineOfBusinessPar);
    }

    public String getPolicySerialNumberPar() {
        return (this.policySerialNumberPar == null ? "" : this.policySerialNumberPar);
    }

    public String getPolicySerialSuffixPar() {
        return (this.policySerialSuffixPar == null ? "" : this.policySerialSuffixPar);
    }

    public String getPolicyEffectiveYearPar() {
        return (this.policyEffectiveYearPar == null ? "" : this.policyEffectiveYearPar);
    }

    public String getPolicyEffectiveMonthPar() {
        return (this.policyEffectiveMonthPar == null ? "" : this.policyEffectiveMonthPar);
    }

    public String getAgentCode() {
        return (this.agentCode == null ? "" : this.agentCode);
    }

    public String getVehicleChassisNo() {
        return (this.vehicleChassisNo == null ? "" : this.vehicleChassisNo);
    }
}
