package com.lmig.icm.thailand.endorsement.service;

import com.lmig.icm.thailand.endorsement.payload.request.NebulaLoginRequest;
import com.lmig.icm.thailand.endorsement.payload.request.WsNebulaFileUpload;
import com.lmig.icm.thailand.endorsement.payload.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class NebulaServiceImpl implements NebulaService {

    private static final String clientId = "o934ab4nsKfqsosw8A2I8NAU6RK0CEjP";
    private static final String clientSecret = "VGIneqXSh9ewRSo8";
    private static final String grantType = "client_credentials";

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public NebulaLoginResponse login(NebulaLoginRequest nebulaLoginRequest) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", nebulaLoginRequest.getClient_id());
        map.add("client_secret", nebulaLoginRequest.getClient_secret());
        map.add("grant_type", nebulaLoginRequest.getGrant_type());

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

        WsNebulaLoginResponse wsNebulaLoginResponse = restTemplate.postForObject("https://test-apis.ap-southeast-1.libertymutual.com/oauth/access-token", entity, WsNebulaLoginResponse.class);

        NebulaLoginResponse nebulaLoginResponse = new NebulaLoginResponse();
        nebulaLoginResponse.setRefreshTokenExpiresIn(wsNebulaLoginResponse.getRefresh_token_expires_in());
        nebulaLoginResponse.setApiProductList(wsNebulaLoginResponse.getApi_product_list());
        nebulaLoginResponse.setApiProductListJson(wsNebulaLoginResponse.getApi_product_list_json());
        nebulaLoginResponse.setOrganizationName(wsNebulaLoginResponse.getOrganization_name());
        nebulaLoginResponse.setDeveloperEmail(wsNebulaLoginResponse.getDeveloper_email());
        nebulaLoginResponse.setTokenType(wsNebulaLoginResponse.getToken_type());
        nebulaLoginResponse.setIssuedAt(wsNebulaLoginResponse.getIssued_at());
        nebulaLoginResponse.setClientId(wsNebulaLoginResponse.getClient_id());
        nebulaLoginResponse.setAccessToken(wsNebulaLoginResponse.getAccess_token());
        nebulaLoginResponse.setApplicationName(wsNebulaLoginResponse.getApplication_name());
        nebulaLoginResponse.setScope(wsNebulaLoginResponse.getScope());
        nebulaLoginResponse.setExpiresIn(wsNebulaLoginResponse.getExpires_in());
        nebulaLoginResponse.setRefreshCount(wsNebulaLoginResponse.getRefresh_count());
        nebulaLoginResponse.setStatus(wsNebulaLoginResponse.getStatus());

        return nebulaLoginResponse;
    }

    @Override
    public WsNebulaFileDownload upload(WsNebulaFileUpload wsNebulaFileUpload) {

        NebulaLoginRequest nebulaLoginRequest = new NebulaLoginRequest();
        nebulaLoginRequest.setClient_id(clientId);
        nebulaLoginRequest.setClient_secret(clientSecret);
        nebulaLoginRequest.setGrant_type(grantType);

        NebulaLoginResponse nebulaLoginResponse = login(nebulaLoginRequest);

        String token = "Bearer " + nebulaLoginResponse.getAccessToken();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", token);

        HttpEntity<WsNebulaFileUpload> entity = new HttpEntity<>(wsNebulaFileUpload, headers);

        WsNebulaFileUploadStatus wsNebulaFileUploadStatus = restTemplate.postForObject("https://test-apis.ap-southeast-1.libertymutual.com/api/apac/documents/v1/file", entity, WsNebulaFileUploadStatus.class);

        HttpHeaders headers2 = new HttpHeaders();
        headers2.setContentType(MediaType.APPLICATION_JSON);
        headers2.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers2.set("Authorization", token);

        HttpEntity entity2 = new HttpEntity(headers2);

        ResponseEntity<WsNebulaFileDownload> response = restTemplate.exchange("https://test-apis.ap-southeast-1.libertymutual.com/api/apac/documents/v1/file/" + wsNebulaFileUploadStatus.getDocReferenceNo(), HttpMethod.GET, entity2, WsNebulaFileDownload.class);
        WsNebulaFileDownload wsNebulaFileDownload = response.getBody();

        return wsNebulaFileDownload;
    }

    @Override
    public WsNebulaFileDownload download(String documentId) {

        NebulaLoginRequest nebulaLoginRequest = new NebulaLoginRequest();
        nebulaLoginRequest.setClient_id(clientId);
        nebulaLoginRequest.setClient_secret(clientSecret);
        nebulaLoginRequest.setGrant_type(grantType);

        NebulaLoginResponse nebulaLoginResponse = login(nebulaLoginRequest);

        String token = "Bearer " + nebulaLoginResponse.getAccessToken();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", token);

        HttpEntity entity2 = new HttpEntity(headers);

        ResponseEntity<WsNebulaFileDownload> response = restTemplate.exchange("https://test-apis.ap-southeast-1.libertymutual.com/api/apac/documents/v1/file/" + documentId, HttpMethod.GET, entity2, WsNebulaFileDownload.class);
        WsNebulaFileDownload wsNebulaFileDownload = response.getBody();

        return wsNebulaFileDownload;
    }

    @Override
    public WsNebulaFileDetails search(String documentId) {

        NebulaLoginRequest nebulaLoginRequest = new NebulaLoginRequest();
        nebulaLoginRequest.setClient_id(clientId);
        nebulaLoginRequest.setClient_secret(clientSecret);
        nebulaLoginRequest.setGrant_type(grantType);

        NebulaLoginResponse nebulaLoginResponse = login(nebulaLoginRequest);

        String token = "Bearer " + nebulaLoginResponse.getAccessToken();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", token);

        HttpEntity entity = new HttpEntity(headers);

        ResponseEntity<WsNebulaFileDetails> response = restTemplate.exchange("https://test-apis.ap-southeast-1.libertymutual.com/api/apac/documents/v1/file/docIndex/" + documentId, HttpMethod.GET, entity, WsNebulaFileDetails.class);
        WsNebulaFileDetails wsNebulaFileDetails = response.getBody();

        return wsNebulaFileDetails;
    }
}
