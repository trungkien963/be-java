package com.lmig.icm.thailand.endorsement.repository;

import com.lmig.icm.thailand.endorsement.model.Beneficiary;
import com.lmig.icm.thailand.endorsement.model.Endorsement;
import com.lmig.icm.thailand.endorsement.model.Province;
import com.lmig.icm.thailand.endorsement.model.Role;

import java.io.FileNotFoundException;
import java.util.List;

public interface ResourceDao {

    List<Province> getProvinces();

    List<Province> uploadProvinces() throws FileNotFoundException;

    List<Province> getProvincesFromExcel() throws FileNotFoundException;

    List<Endorsement> getEndorsements();

    List<Endorsement> uploadEndorsements() throws FileNotFoundException;

    List<Endorsement> getEndorsementsFromExcel() throws FileNotFoundException;

    List<Beneficiary> getBeneficiaries();

    List<Beneficiary> uploadBeneficiaries() throws FileNotFoundException;

    List<Beneficiary> getBeneficiariesFromExcel() throws FileNotFoundException;

    List<Role> getRoles();

    List<Role> uploadRoles() throws FileNotFoundException;

    List<Role> getRolesFromExcel() throws FileNotFoundException;

    List<Role> getRoleModulesByRoleName(String roleName);
}
