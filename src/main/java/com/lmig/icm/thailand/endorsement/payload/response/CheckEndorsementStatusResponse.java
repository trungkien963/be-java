package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CheckEndorsementStatusResponse {

    private String batchNumber;

    private String batchKey;

    private String referenceNo;

    private String wsReturnCode;

    private String wsReturnDescription;

    private String resultCode;

    private String policyNoPar;

    private String rorjorNoPar;

    private String resultDescription;
}
