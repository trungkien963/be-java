package com.lmig.icm.thailand.endorsement.service;

import com.lmig.icm.thailand.endorsement.payload.request.WsEarnixCalculationRequest;
import com.lmig.icm.thailand.endorsement.payload.response.WsEarnixCalculationResponse;

public interface CalculationService {

    WsEarnixCalculationResponse calculation(WsEarnixCalculationRequest wsEarnixCalculationRequest);
}
