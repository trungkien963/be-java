package com.lmig.icm.thailand.endorsement.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PolicyEndorsement {

    private long id;

    private String policyNo;

    private String endorsementType;

    private String content;

    private String endorsementDetails;

    private String status;

    private String creator;

    private String creatorEmail;

    private String createdDate;

    private String submitter;

    private String submitterEmail;

    private String submittedDate;

    private String approver;

    private String approverEmail;

    private String approvedDate;

    private String rejecter;

    private String rejecterEmail;

    private String rejectedReason;

    private String rejectedDate;
}
