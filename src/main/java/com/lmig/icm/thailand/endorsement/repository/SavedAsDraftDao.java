package com.lmig.icm.thailand.endorsement.repository;

import com.lmig.icm.thailand.endorsement.model.SavedAsDraft;

import java.util.List;

public interface SavedAsDraftDao {

    boolean insert(SavedAsDraft savedAsDraft);

    boolean update(String content, Integer savedAsDraftId);

    List<SavedAsDraft> getDraftsById(Integer savedAsDraftId);

    List<SavedAsDraft> getDraftsByCategory(String category, String creator);

    List<SavedAsDraft> getDraftsByPolicyNo(String category, String policyNo, String creator);

    List<SavedAsDraft> getLastDraftsByPolicyNo(String category, String policyNo, String creator);

    boolean delete(Integer savedAsDraftId);

    boolean deleteByPolicyNo(String category, String policyNo);

    List<SavedAsDraft> getDraftsByStatus(String category, String savedAsDraftStatus);

    List<SavedAsDraft> getDraftsByHistory(String category);
}
