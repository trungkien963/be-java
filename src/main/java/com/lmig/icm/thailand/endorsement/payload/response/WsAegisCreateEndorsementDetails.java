package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsAegisCreateEndorsementDetails {

    @JsonProperty("BATCH_NUMBER")
    private String batchNumber;

    @JsonProperty("BATCH_KEY")
    private String batchKey;

    @JsonProperty("REFERENCE_NO")
    private String referenceNo;

    @JsonProperty("POLICY_LINE_OF_BUSINESS")
    private String policyLineOfBusiness;

    @JsonProperty("POLICY_NO")
    private String policyNo;

    @JsonProperty("POLICY_ENDORSEMENT_NO")
    private String policyEndorsementNo;

    @JsonProperty("ENDORSEMENT_TYPE")
    private String endorsementType;

    @JsonProperty("ENDORSEMENT_REASON_CODE")
    private String endorsementReasonCode;

    @JsonProperty("POLICY_STATUS")
    private String policyStatus;

    @JsonProperty("GPRM")
    private String gprm;

    @JsonProperty("TAX")
    private String tax;

    @JsonProperty("STAMP")
    private String stamp;

    @JsonProperty("TAX_NOTE_NO")
    private String taxNoteNo;

    @JsonProperty("TAX_NOTE_DATE")
    private String taxNoteDate;

    @JsonProperty("TAX_ISSUE_BRANCH")
    private String taxIssueBranch;

    @JsonProperty("INSURED_CODE")
    private String insuredCode;

    @JsonProperty("NOTE_NO")
    private String noteNo;

    @JsonProperty("EPOLICY_FLAG")
    private String epolicyFlag;

    @JsonProperty("PAPER_POLICY_FLAG")
    private String paperPolicyFlag;

    @JsonProperty("EPOLICY_CHANNEL")
    private String epolicyChannel;

    @JsonProperty("INSURED_EMAIL")
    private String insuredEmail;

    @JsonProperty("INSURED_MOBILE_NO")
    private String insuredMobileNo;

    @JsonProperty("NATIONALITY1")
    private String nationality1;

    @JsonProperty("NATIONALITY2")
    private String nationality2;

    public String getBatchNumber() {
        return (this.batchNumber == null ? "" : this.batchNumber);
    }

    public String getBatchKey() {
        return (this.batchKey == null ? "" : this.batchKey);
    }

    public String getReferenceNo() {
        return (this.referenceNo == null ? "" : this.referenceNo);
    }

    public String getPolicyLineOfBusiness() {
        return (this.policyLineOfBusiness == null ? "" : this.policyLineOfBusiness);
    }

    public String getPolicyNo() {
        return (this.policyNo == null ? "" : this.policyNo);
    }

    public String getPolicyEndorsementNo() {
        return (this.policyEndorsementNo == null ? "" : this.policyEndorsementNo);
    }

    public String getEndorsementType() {
        return (this.endorsementType == null ? "" : this.endorsementType);
    }

    public String getEndorsementReasonCode() {
        return (this.endorsementReasonCode == null ? "" : this.endorsementReasonCode);
    }

    public String getPolicyStatus() {
        return (this.policyStatus == null ? "" : this.policyStatus);
    }

    public String getGprm() {
        return (this.gprm == null ? "" : this.gprm);
    }

    public String getTax() {
        return (this.tax == null ? "" : this.tax);
    }

    public String getStamp() {
        return (this.stamp == null ? "" : this.stamp);
    }

    public String getTaxNoteNo() {
        return (this.taxNoteNo == null ? "" : this.taxNoteNo);
    }

    public String getTaxNoteDate() {
        return (this.taxNoteDate == null ? "" : this.taxNoteDate);
    }

    public String getTaxIssueBranch() {
        return (this.taxIssueBranch == null ? "" : this.taxIssueBranch);
    }

    public String getInsuredCode() {
        return (this.insuredCode == null ? "" : this.insuredCode);
    }

    public String getNoteNo() {
        return (this.noteNo == null ? "" : this.noteNo);
    }

    public String getEpolicyFlag() {
        return (this.epolicyFlag == null ? "" : this.epolicyFlag);
    }

    public String getPaperPolicyFlag() {
        return (this.paperPolicyFlag == null ? "" : this.paperPolicyFlag);
    }

    public String getEpolicyChannel() {
        return (this.epolicyChannel == null ? "" : this.epolicyChannel);
    }

    public String getInsuredEmail() {
        return (this.insuredEmail == null ? "" : this.insuredEmail);
    }

    public String getInsuredMobileNo() {
        return (this.insuredMobileNo == null ? "" : this.insuredMobileNo);
    }

    public String getNationality1() {
        return (this.nationality1 == null ? "" : this.nationality1);
    }

    public String getNationality2() {
        return (this.nationality2 == null ? "" : this.nationality2);
    }
}
