package com.lmig.icm.thailand.endorsement.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Province {

    private long id;

    private String keyField;

    private String provinceCode;

    private String provinceName;

    private Double modDate;

    private String provinceNameEng;

    private String provinceFlag;
}
