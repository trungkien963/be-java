package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsEarnixResult {

    private Integer versionId;

    private String quoteId;

    @JsonProperty("PolicyVehicle")
    private WsEarnixPolicyVehicleResponse policyVehicle;
}
