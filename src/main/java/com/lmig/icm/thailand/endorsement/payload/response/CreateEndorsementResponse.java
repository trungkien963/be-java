package com.lmig.icm.thailand.endorsement.payload.response;

import com.lmig.icm.thailand.endorsement.model.PolicyEndorsement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateEndorsementResponse {

    private WsAegisCreateEndorsementReturnCode wsStatus;

    private List<PolicyEndorsement> results;
}
