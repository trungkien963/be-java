package com.lmig.icm.thailand.endorsement.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsEarnixPolicyVehicleRequest {

    private String requestId;

    private String agentTypeKey;

    private String basePremiumUser;

    private String branchCode;

    private String branchName;

    private String collBasePremiumUser;

    private String crossBorderPeriodUser;

    private String distribution;

    private String distributionType;

    private String fleetDiscountPercentUser;

    private String garageTypeUser;

    private String lobUser;

    private String motorCode;

    private String ncbPercentUser;

    private String otherDiscountPercentUser;

    private String policyEffectiveDate;

    private String policyEffectiveDateExpiring;

    private String policyExpirationDate;

    private String policyExpirationDateExpiring;

    private String polInsuredType;

    private String postalCode;

    private String shortRateUser;

    private String sumInsuredUser;

    private String surchargePercentUser;

    private Float unannualizedPremiumExpiring;

    private String vehicleAdditionalEquipment;

    private Float vehicleAge;

    private String vehicleCapacityKey;

    private String vehicleGroup;

    private Float vehicleManufactureYear;

    private Float vehicleNoOfSeat;

    private Float vehicleRegistrationYear;

    @JsonProperty("Driver")
    private List<WsEarnixDriverRequest> driver;

    @JsonProperty("Claims")
    private List<WsEarnixClaimsRequest> claims;

    @JsonProperty("GarageLOB")
    private List<WsEarnixGarageLOBRequest> garageLOB;
}
