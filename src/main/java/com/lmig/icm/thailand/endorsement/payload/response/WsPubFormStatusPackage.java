package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsPubFormStatusPackage {

    private String packageId;

    private String transactionId;

    private String packagestatus;

    private List<WsPubFormStatusPackagePhases> phases;

    private List<WsPubFormStatusPackageDocuments> documents;

    private List<WsPubFormStatusPackageDistributions> distributions;
}
