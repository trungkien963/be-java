package com.lmig.icm.thailand.endorsement.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsAegisEndorsementInfo {

    @JsonProperty("REFERENCE_NO")
    private String referenceNo;

    @JsonProperty("CALLER")
    private String caller;

    @JsonProperty("WS_CALL_FROM")
    private String wsCallFrom;

    @JsonProperty("USER_ID")
    private String userId;

    @JsonProperty("POLICY_LINE_OF_BUSINESS")
    private String policyLineOfBusiness;

    @JsonProperty("POLICY_NO")
    private String policyNo;

    @JsonProperty("RORJOR_NO")
    private String rorjorNo;

    @JsonProperty("ENTRY_DATE")
    private String entryDate;

    @JsonProperty("SIGN_DATE")
    private String signDate;

    @JsonProperty("POLICY_EFFECTIVE_DATE")
    private String policyEffectiveDate;

    @JsonProperty("COUNTRY_CODE")
    private String countryCode;

    @JsonProperty("REMARKS")
    private String remarks;

    @JsonProperty("ENDORSEMENT_REASON")
    private String endorsementReason;

    @JsonProperty("VEHICLE_BODY_TYPE")
    private String vehicleBodyType;

    @JsonProperty("VEHICLE_CAPACITY_KEY")
    private String vehicleCapacityKey;

    @JsonProperty("VEHICLE_CHASSIS_NO")
    private String vehicleChassisNo;

    @JsonProperty("VEHICLE_CODE_DESC")
    private String vehicleCodeDesc;

    @JsonProperty("VEHICLE_COLOR")
    private String vehicleColor;

    @JsonProperty("VEHICLE_ENGINE_CAPACITY")
    private String vehicleEngineCapacity;

    @JsonProperty("VEHICLE_ENGINE_NO")
    private String vehicleEngineNo;

    @JsonProperty("VEHICLE_GARAGE_LOCATION")
    private String vehicleGarageLocation;

    @JsonProperty("VEHICLE_LICENSE_NO")
    private String vehicleLicenseNo;

    @JsonProperty("VEHICLE_MAKE")
    private String vehicleMake;

    @JsonProperty("KEY_FIELD")
    private String keyField;

    @JsonProperty("VEHICLE_MANUFACTURE_YEAR")
    private String vehicleManufactureYear;

    @JsonProperty("VEHICLE_MODEL")
    private String vehicleModel;

    @JsonProperty("VEHICLE_MODEL_TYPE")
    private String vehicleModelType;

    @JsonProperty("VEHICLE_NO_OF_SEAT")
    private String vehicleNoOfSeat;

    @JsonProperty("VEHICLE_PREFIX_LICENSE_NO")
    private String vehiclePrefixLicenseNo;

    @JsonProperty("VEHICLE_PROVINCE")
    private String vehicleProvince;

    @JsonProperty("VEHICLE_REG_YEAR")
    private String vehicleRegYear;

    @JsonProperty("VEHICLE_STICKER_NO")
    private String vehicleStickerNo;

    @JsonProperty("VEHICLE_WEIGHT")
    private String vehicleWeight;

    @JsonProperty("INFORM_TO_CHANGE")
    private String informToChange;

    public String getReferenceNo() {
        return (this.referenceNo == null ? "" : this.referenceNo);
    }

    public String getCaller() {
        return (this.caller == null ? "" : this.caller);
    }

    public String getWsCallFrom() {
        return (this.wsCallFrom == null ? "" : this.wsCallFrom);
    }

    public String getUserId() {
        return (this.userId == null ? "" : this.userId);
    }

    public String getPolicyLineOfBusiness() {
        return (this.policyLineOfBusiness == null ? "" : this.policyLineOfBusiness);
    }

    public String getPolicyNo() {
        return (this.policyNo == null ? "" : this.policyNo);
    }

    public String getRorjorNo() {
        return (this.rorjorNo == null ? "" : this.rorjorNo);
    }

    public String getEntryDate() {
        return (this.entryDate == null ? "" : this.entryDate);
    }

    public String getSignDate() {
        return (this.signDate == null ? "" : this.signDate);
    }

    public String getPolicyEffectiveDate() {
        return (this.policyEffectiveDate == null ? "" : this.policyEffectiveDate);
    }

    public String getCountryCode() {
        return (this.countryCode == null ? "" : this.countryCode);
    }

    public String getRemarks() {
        return (this.remarks == null ? "" : this.remarks);
    }

    public String getEndorsementReason() {
        return (this.endorsementReason == null ? "" : this.endorsementReason);
    }

    public String getVehicleBodyType() {
        return (this.vehicleBodyType == null ? "" : this.vehicleBodyType);
    }

    public String getVehicleCapacityKey() {
        return (this.vehicleCapacityKey == null ? "" : this.vehicleCapacityKey);
    }

    public String getVehicleEngineCapacity() {
        return (this.vehicleEngineCapacity == null ? "" : this.vehicleEngineCapacity);
    }

    public String getVehicleChassisNo() {
        return (this.vehicleChassisNo == null ? "" : this.vehicleChassisNo);
    }

    public String getVehicleCodeDesc() {
        return (this.vehicleCodeDesc == null ? "" : this.vehicleCodeDesc);
    }

    public String getVehicleColor() {
        return (this.vehicleColor == null ? "" : this.vehicleColor);
    }

    public String getVehicleEngineNo() {
        return (this.vehicleEngineNo == null ? "" : this.vehicleEngineNo);
    }

    public String getVehicleGarageLocation() {
        return (this.vehicleGarageLocation == null ? "" : this.vehicleGarageLocation);
    }

    public String getVehicleLicenseNo() {
        return (this.vehicleLicenseNo == null ? "" : this.vehicleLicenseNo);
    }

    public String getVehicleMake() {
        return (this.vehicleMake == null ? "" : this.vehicleMake);
    }

    public String geKeyField() {
        return (this.keyField == null ? "" : this.keyField);
    }

    public String getVehicleManufactureYear() {
        return (this.vehicleManufactureYear == null ? "" : this.vehicleManufactureYear);
    }

    public String getVehicleModel() {
        return (this.vehicleModel == null ? "" : this.vehicleModel);
    }

    public String getVehicleModelType() {
        return (this.vehicleModelType == null ? "" : this.vehicleModelType);
    }

    public String getVehicleNoOfSeat() {
        return (this.vehicleNoOfSeat == null ? "" : this.vehicleNoOfSeat);
    }

    public String getVehiclePrefixLicenseNo() {
        return (this.vehiclePrefixLicenseNo == null ? "" : this.vehiclePrefixLicenseNo);
    }

    public String getVehicleProvince() {
        return (this.vehicleProvince == null ? "" : this.vehicleProvince);
    }

    public String getVehicleRegYear() {
        return (this.vehicleRegYear == null ? "" : this.vehicleRegYear);
    }

    public String getVehicleStickerNo() {
        return (this.vehicleStickerNo == null ? "" : this.vehicleStickerNo);
    }

    public String getVehicleWeight() {
        return (this.vehicleWeight == null ? "" : this.vehicleWeight);
    }

    public String getInformToChange() {
        return (this.informToChange == null ? "" : this.informToChange);
    }
}
