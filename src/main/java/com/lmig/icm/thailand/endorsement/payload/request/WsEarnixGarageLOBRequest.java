package com.lmig.icm.thailand.endorsement.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsEarnixGarageLOBRequest {

    @JsonProperty("Coverage")
    private List<WsEarnixCoverageRequest> coverage;
}
