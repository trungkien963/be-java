package com.lmig.icm.thailand.endorsement.service;

import com.lmig.icm.thailand.endorsement.config.StorageConfig;
import com.lmig.icm.thailand.endorsement.enumType.FileUploadType;
import com.lmig.icm.thailand.endorsement.exception.StorageException;
import com.lmig.icm.thailand.endorsement.exception.StorageFileNotFoundException;
import com.lmig.icm.thailand.endorsement.model.PolicyEndorsementFile;
import com.lmig.icm.thailand.endorsement.payload.request.*;
import com.lmig.icm.thailand.endorsement.payload.response.WsNebulaFileDownload;
import com.lmig.icm.thailand.endorsement.payload.response.WsPubFormDetails;
import com.lmig.icm.thailand.endorsement.payload.response.WsPubFormStatus;
import com.lmig.icm.thailand.endorsement.repository.FileDao;
import com.lmig.icm.thailand.endorsement.util.DateUtility;
import com.lmig.icm.thailand.endorsement.util.UserUtility;
import liquibase.util.file.FilenameUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

@Service
public class FileServiceImpl implements FileService {

    private final Path rootLocation;

    @Autowired
    private PubService pubService;

    @Autowired
    private NebulaService nebulaService;

    @Autowired
    public FileServiceImpl(StorageConfig storageConfig) {

        this.rootLocation = Paths.get(storageConfig.getLocation());
    }

    @Value("${api.path}")
    public String serverPath;

    @Autowired
    private FileDao fileDao;

    private static final class PolicyEndorsementFileRowMapper implements RowMapper<PolicyEndorsementFile> {

        @Override
        public PolicyEndorsementFile mapRow(final ResultSet rs, final int rowNum) throws SQLException {

            return new PolicyEndorsementFile(
                    rs.getLong("ID"),
                    rs.getString("ENDORSEMENT_ID"),
                    rs.getString("POLICY_NO"),
                    rs.getString("ENDORSEMENT_TYPE"),
                    rs.getString("FILE_NAME"),
                    rs.getString("FULL_NAME"),
                    rs.getString("EXTENSION"),
                    rs.getString("FILE_PATH"),
                    rs.getString("UPLOADER"),
                    rs.getString("UPLOADED_DATE"));
        }
    }

    @Override
    public void init() {

        try {

            Files.createDirectories(rootLocation);
        } catch (IOException e) {

            throw new StorageException("Could not initialize storage", e);
        }
    }

    @Override
    public List<PolicyEndorsementFile> getFiles(String endorsementId) {

        return fileDao.getFiles(endorsementId);
    }

    @Override
    public List<PolicyEndorsementFile> getFiles(String policyNo, String endorsementType) {

        return fileDao.getFiles(policyNo, endorsementType);
    }

    @Override
    public List<?> store(
            MultipartFile[] files,
            FileUploadRequest fileUploadRequest) {

        if (files == null || files.length == 0) {

            throw new StorageException("You must select at least one file for uploading.");
        }

        List<?> fileList = new ArrayList<>();

        for (int i = 0; i < files.length; i++) {

            String fileName = files[i].getOriginalFilename();
            String basename = FilenameUtils.getBaseName(fileName);
            String hashedName = UUID.randomUUID().toString();
            String fileType = FilenameUtils.getExtension(fileName);
            String fullName = hashedName + "." + fileType;

            try {

                if (files[i].isEmpty()) {

                    throw new StorageException("Failed to store empty file.");
                }

                Path destinationFile = this.rootLocation.resolve(Paths.get(fullName)).normalize().toAbsolutePath();

                if (!destinationFile.getParent().equals(this.rootLocation.toAbsolutePath())) {

                    // This is a security check
                    throw new StorageException("Cannot store file outside current directory.");
                }

                try (InputStream inputStream = files[i].getInputStream()) {

                    Files.copy(inputStream, destinationFile, StandardCopyOption.REPLACE_EXISTING);

                    if(fileUploadRequest.getFileType().equals(FileUploadType.ENDORSEMENT.toString())) {

                        PolicyEndorsementFile policyEndorsementFile = new PolicyEndorsementFile();

                        policyEndorsementFile.setEndorsementId(fileUploadRequest.getEndorsementId());
                        policyEndorsementFile.setPolicyNo(fileUploadRequest.getPolicyNo());
                        policyEndorsementFile.setEndorsementType(fileUploadRequest.getEndorsementType());
                        policyEndorsementFile.setFileName(basename);
                        policyEndorsementFile.setFullName(fullName);
                        policyEndorsementFile.setExtension(fileType);
                        policyEndorsementFile.setFilePath(serverPath + "/api/v1/files/" + fullName);
                        policyEndorsementFile.setUploader(UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null);
                        policyEndorsementFile.setUploadedDate(DateUtility.now());

                        fileDao.insert(policyEndorsementFile);

                        fileList = fileDao.getFiles(policyEndorsementFile.getEndorsementId());
                    }
                }
            } catch (IOException e) {

                throw new StorageException("Failed to store file.", e);
            }
        }

        return fileList;
    }

    @Override
    public Resource loadAsResource(String fullName) {

        try {

            Path file = fileDao.load(fullName);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {

                return resource;
            } else {

                throw new StorageFileNotFoundException("Could not read file: " + fullName);
            }
        } catch (MalformedURLException e) {

            throw new StorageFileNotFoundException("Could not read file: " + fullName, e);
        }
    }

    @Override
    public boolean delete(String fullName) {

        try {

            Path file = fileDao.load(fullName);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {

                Files.delete(file);
            }

            return fileDao.delete(fullName);
        } catch(IOException e) {

            throw new StorageException("Failed to delete file.", e);
        }
    }

    @Override
    public WsNebulaFileDownload uploadFileToNebula(String policyNo, WsPubFormDetails wsPubFormDetails)
            throws IOException {

        WsPubFormStatus wsPubFormStatus = pubService.upload(wsPubFormDetails);

        String url = wsPubFormStatus.getPackages().get(0).getDocuments().get(0).getUrl();
        String docName = wsPubFormStatus.getRequestId() + ".pdf";

        Path destinationFile = this.rootLocation.resolve(Paths.get(docName)).normalize().toAbsolutePath();

        try (InputStream in = new URL(url).openStream()) {

            Files.copy(in, destinationFile, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {

            e.printStackTrace();
        }

        Resource resource = loadAsResource(docName);

        byte[] fileContent = FileUtils.readFileToByteArray(resource.getFile());
        String encodedString = Base64.getEncoder().encodeToString(fileContent);

        WsNebulaFileUpload wsNebulaFileUpload = new WsNebulaFileUpload();

        WsNebulaFileUploadDetails docUploadReq = new WsNebulaFileUploadDetails();
        docUploadReq.setCountry("TH");
        docUploadReq.setLob("P");
        docUploadReq.setCreatedBy("Phuong.D@lmginsurance.co.th");
        docUploadReq.setSourceSystem("Neo-Endorsement");

        WsNebulaFileUploadDetailsTransaction transactionDetails = new WsNebulaFileUploadDetailsTransaction();
        transactionDetails.setPrimaryKey("Primary key");
        transactionDetails.setSecondaryKey("Secondary key");
        docUploadReq.setTransactionDetails(transactionDetails);

        WsNebulaFileUploadDetailsDocument documentDetails = new WsNebulaFileUploadDetailsDocument();

        List<WsNebulaFileUploadDetailsDocumentIndex> docIndex = new ArrayList<>();

        WsNebulaFileUploadDetailsDocumentIndex wsNebulaFileUploadDetailsDocumentIndex = new WsNebulaFileUploadDetailsDocumentIndex();
        wsNebulaFileUploadDetailsDocumentIndex.setName(policyNo);
        wsNebulaFileUploadDetailsDocumentIndex.setValue("1");
        docIndex.add(wsNebulaFileUploadDetailsDocumentIndex);

        documentDetails.setDocIndex(docIndex);
        documentDetails.setDocObject(encodedString);
        documentDetails.setDocName(docName);
        documentDetails.setDocType("PCPolicy");
        documentDetails.setMimeType("application/pdf");

        docUploadReq.setDocumentDetails(documentDetails);

        wsNebulaFileUpload.setDocUploadReq(docUploadReq);

        WsNebulaFileDownload WsNebulaFileDownload = nebulaService.upload(wsNebulaFileUpload);

        return WsNebulaFileDownload;
    }
}
