package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsPolicyDetails {

    @JsonProperty("ACOVBB_AMOUNT")
    private Integer ACOVBB_AMOUNT;

    @JsonProperty("ACOVBB_PREMIUM")
    private Integer ACOVBB_PREMIUM;

    @JsonProperty("ACOVBB_PREMIUM_EXCLUDE_ADJUST")
    private Integer ACOVBB_PREMIUM_EXCLUDE_ADJUST;

    @JsonProperty("ACOVEA_AMOUNT")
    private Integer ACOVEA_AMOUNT;

    @JsonProperty("ACOVEA_PERCENT")
    private Integer ACOVEA_PERCENT;

    @JsonProperty("ACOVED_AMOUNT")
    private Integer ACOVED_AMOUNT;

    @JsonProperty("ACOVED_PERCENT")
    private Integer ACOVED_PERCENT;

    @JsonProperty("ACOVFD_AMOUNT")
    private Integer ACOVFD_AMOUNT;

    @JsonProperty("ACOVFD_PERCENT")
    private Integer ACOVFD_PERCENT;

    @JsonProperty("ACOVME_AMOUNT")
    private Integer ACOVME_AMOUNT;

    @JsonProperty("ACOVME_PREMIUM")
    private Integer ACOVME_PREMIUM;

    @JsonProperty("ACOVME_PREMIUM_EXCLUDE_ADJUST")
    private Integer ACOVME_PREMIUM_EXCLUDE_ADJUST;

    @JsonProperty("ACOVMP_AMOUNT")
    private Integer ACOVMP_AMOUNT;

    @JsonProperty("ACOVMP_PREMIUM")
    private Integer ACOVMP_PREMIUM;

    @JsonProperty("ACOVMP_PREMIUM_EXCLUDE_ADJUST")
    private Integer ACOVMP_PREMIUM_EXCLUDE_ADJUST;

    @JsonProperty("ACOVOD_AMOUNT")
    private Integer ACOVOD_AMOUNT;

    @JsonProperty("ACOVOT_AMOUNT")
    private Integer ACOVOT_AMOUNT;

    @JsonProperty("ACOVOT_PERCENT")
    private Integer ACOVOT_PERCENT;

    @JsonProperty("ACOVPD_AMOUNT")
    private Integer ACOVPD_AMOUNT;

    @JsonProperty("ACOVPD_PREMIUM")
    private Integer ACOVPD_PREMIUM;

    @JsonProperty("ACOVPD_PREMIUM_EXCLUDE_ADJUST")
    private Integer ACOVPD_PREMIUM_EXCLUDE_ADJUST;

    @JsonProperty("ACOVPP_AMOUNT")
    private Integer ACOVPP_AMOUNT;

    @JsonProperty("ACOVPP_PREMIUM")
    private Integer ACOVPP_PREMIUM;

    @JsonProperty("ACOVPP_PREMIUM_EXCLUDE_ADJUST")
    private Integer ACOVPP_PREMIUM_EXCLUDE_ADJUST;

    @JsonProperty("ACOVTD_AMOUNT")
    private Integer ACOVTD_AMOUNT;

    @JsonProperty("ACOVTD_PREMIUM")
    private Integer ACOVTD_PREMIUM;

    @JsonProperty("ACOVTD_PREMIUM_EXCLUDE_ADJUST")
    private Integer ACOVTD_PREMIUM_EXCLUDE_ADJUST;

    @JsonProperty("ACOVTH_AMOUNT")
    private Integer ACOVTH_AMOUNT;

    @JsonProperty("ACOVTP_AMOUNT")
    private Integer ACOVTP_AMOUNT;

    @JsonProperty("ACOVTP_PREMIUM")
    private Integer ACOVTP_PREMIUM;

    @JsonProperty("ACOVTP_PREMIUM_EXCLUDE_ADJUST")
    private Integer ACOVTP_PREMIUM_EXCLUDE_ADJUST;

    @JsonProperty("ADDITIONAL_EQUIPMENT_FLAG")
    private String ADDITIONAL_EQUIPMENT_FLAG;

    @JsonProperty("ADDRESS_LINE1")
    private String ADDRESS_LINE1;

    @JsonProperty("ADDRESS_LINE2")
    private String ADDRESS_LINE2;

    @JsonProperty("ADDRESS_PROVINCE_CODE")
    private String ADDRESS_PROVINCE_CODE;

    @JsonProperty("AGENT_CODE")
    private String AGENT_CODE;

    @JsonProperty("AGENT_NAME")
    private String AGENT_NAME;

    @JsonProperty("AGENT_TYPE_KEY")
    private String AGENT_TYPE_KEY;

    @JsonProperty("AV5_BASIC_PREMIUM")
    private Integer AV5_BASIC_PREMIUM;

    @JsonProperty("AV5_SUM_INSURED")
    private Integer AV5_SUM_INSURED;

    @JsonProperty("BASIC_PREMIUM")
    private Integer BASIC_PREMIUM;

    @JsonProperty("BENEFICIARY_CODE")
    private String BENEFICIARY_CODE;

    @JsonProperty("BENEFICIARY_NAME")
    private String BENEFICIARY_NAME;

    @JsonProperty("BRANCH_CODE")
    private String BRANCH_CODE;

    @JsonProperty("BRANCH_NAME")
    private String BRANCH_NAME;

    @JsonProperty("BROKER_CODE")
    private String BROKER_CODE;

    @JsonProperty("BROKER_NAME")
    private String BROKER_NAME;

    @JsonProperty("CAMPAIGN_CODE")
    private String CAMPAIGN_CODE;

    @JsonProperty("CAMPAIGN_NAME")
    private String CAMPAIGN_NAME;

    @JsonProperty("CLASS_DESC_ENG")
    private String CLASS_DESC_ENG;

    @JsonProperty("COLLECTION_FEE")
    private Integer COLLECTION_FEE;

    @JsonProperty("COMM")
    private Float COMM;

    @JsonProperty("COMMISSION_PERCENT")
    private Integer COMMISSION_PERCENT;

    @JsonProperty("COUNT_CLAIM")
    private Integer COUNT_CLAIM;

    @JsonProperty("COVER_NOTE_DATE")
    private String COVER_NOTE_DATE;

    @JsonProperty("DEDUCT_PREM")
    private Integer DEDUCT_PREM;

    @JsonProperty("DIRECT_AGENT_TYPE_FLAG")
    private String DIRECT_AGENT_TYPE_FLAG;

    @JsonProperty("DISTRIBUTION_TYPE")
    private String DISTRIBUTION_TYPE;

    @JsonProperty("DISTRICT")
    private String DISTRICT;

    @JsonProperty("DRIVER1_BIRTH_DATE")
    private String DRIVER1_BIRTH_DATE;

    @JsonProperty("DRIVER1_ID_CARD_NO")
    private String DRIVER1_ID_CARD_NO;

    @JsonProperty("DRIVER1_LICENSE_NO")
    private String DRIVER1_LICENSE_NO;

    @JsonProperty("DRIVER1_NAME")
    private String DRIVER1_NAME;

    @JsonProperty("DRIVER1_OCCUPATION")
    private String DRIVER1_OCCUPATION;

    @JsonProperty("DRIVER1_OCCUPATION_CODE")
    private String DRIVER1_OCCUPATION_CODE;

    @JsonProperty("DRIVER1_SEX")
    private String DRIVER1_SEX;

    @JsonProperty("DRIVER2_BIRTH_DATE")
    private String DRIVER2_BIRTH_DATE;

    @JsonProperty("DRIVER2_ID_CARD_NO")
    private String DRIVER2_ID_CARD_NO;

    @JsonProperty("DRIVER2_LICENSE_NO")
    private String DRIVER2_LICENSE_NO;

    @JsonProperty("DRIVER2_NAME")
    private String DRIVER2_NAME;

    @JsonProperty("DRIVER2_OCCUPATION")
    private String DRIVER2_OCCUPATION;

    @JsonProperty("DRIVER2_OCCUPATION_CODE")
    private String DRIVER2_OCCUPATION_CODE;

    @JsonProperty("DRIVER2_SEX")
    private String DRIVER2_SEX;

    @JsonProperty("ENGLISH_NAME_FLAG")
    private String ENGLISH_NAME_FLAG;

    @JsonProperty("ENTRY_DATE")
    private String ENTRY_DATE;

    @JsonProperty("FINANCE_AGENT_TYPE_FLAG")
    private String FINANCE_AGENT_TYPE_FLAG;

    @JsonProperty("FTAV_AMOUNT")
    private Integer FTAV_AMOUNT;

    @JsonProperty("FTAV_EXCLUDE_PREM")
    private Integer FTAV_EXCLUDE_PREM;

    @JsonProperty("FTAV_PREMIUM")
    private Integer FTAV_PREMIUM;

    @JsonProperty("GPRM")
    private Integer GPRM;

    @JsonProperty("GROUP_OF_VEHICLE")
    private String GROUP_OF_VEHICLE;

    @JsonProperty("INSURED_CODE")
    private String INSURED_CODE;

    @JsonProperty("INSURED_NAME")
    private String INSURED_NAME;

    @JsonProperty("INSURED_NAME_LINE2")
    private String INSURED_NAME_LINE2;

    @JsonProperty("INSURED_TYPE")
    private String INSURED_TYPE;

    @JsonProperty("KEY_FIELD")
    private String KEY_FIELD;

    @JsonProperty("LINK_POLICY_EFFECTIVE_DATE")
    private String LINK_POLICY_EFFECTIVE_DATE;

    @JsonProperty("LINK_POLICY_EXPIRATION_DATE")
    private String LINK_POLICY_EXPIRATION_DATE;

    @JsonProperty("LINK_POLICY_NO")
    private String LINK_POLICY_NO;

    @JsonProperty("LINK_POLICY_STATUS")
    private String LINK_POLICY_STATUS;

    @JsonProperty("MOTOR_BODY_TYPE")
    private String MOTOR_BODY_TYPE;

    @JsonProperty("MOTOR_CODE")
    private String MOTOR_CODE;

    @JsonProperty("MOTOR_CODE_INFO")
    private String MOTOR_CODE_INFO;

    @JsonProperty("MOTOR_SUB_CODE")
    private String MOTOR_SUB_CODE;

    @JsonProperty("OCCUPATION")
    private String OCCUPATION;

    @JsonProperty("OCCUPATION_CODE")
    private String OCCUPATION_CODE;

    @JsonProperty("ODAV_AMOUNT")
    private Integer ODAV_AMOUNT;

    @JsonProperty("ODAV_EXCLUDE_PREM")
    private Integer ODAV_EXCLUDE_PREM;

    @JsonProperty("ODAV_PREMIUM")
    private Integer ODAV_PREMIUM;

    @JsonProperty("OFFICE_CODE")
    private String OFFICE_CODE;

    @JsonProperty("PARENT_AGENT_CODE")
    private String PARENT_AGENT_CODE;

    @JsonProperty("PARENT_AGENT_NAME")
    private String PARENT_AGENT_NAME;

    @JsonProperty("PARENT_UPLIFT_FLAT_AMOUNT")
    private Integer PARENT_UPLIFT_FLAT_AMOUNT;

    @JsonProperty("PARENT_UPLIFT_PERCENT")
    private Integer PARENT_UPLIFT_PERCENT;

    @JsonProperty("POLICY_EFFECTIVE_DATE")
    private String POLICY_EFFECTIVE_DATE;

    @JsonProperty("POLICY_EXPIRATION_DATE")
    private String POLICY_EXPIRATION_DATE;

    @JsonProperty("POLICY_NO")
    private String POLICY_NO;

    @JsonProperty("POLICY_STATUS")
    private String POLICY_STATUS;

    @JsonProperty("POSTAL_CODE")
    private String POSTAL_CODE;

    @JsonProperty("POST_DATE")
    private String POST_DATE;

    @JsonProperty("PRICE_TEST_ID")
    private String PRICE_TEST_ID;

    @JsonProperty("PRODUCT_CODE")
    private String PRODUCT_CODE;

    @JsonProperty("PRODUCT_NAME")
    private String PRODUCT_NAME;

    @JsonProperty("PROVINCE")
    private String PROVINCE;

    @JsonProperty("REAL_POST_DATE")
    private String REAL_POST_DATE;

    @JsonProperty("REGION_CODE")
    private String REGION_CODE;

    @JsonProperty("RENEWAL_FLAG")
    private String RENEWAL_FLAG;

    @JsonProperty("RENEWAL_NUMBER")
    private String RENEWAL_NUMBER;

    @JsonProperty("SETTLED_DATE")
    private String SETTLED_DATE;

    @JsonProperty("SHORT_TERM_FLAG")
    private String SHORT_TERM_FLAG;

    @JsonProperty("SIGN_DATE")
    private String SIGN_DATE;

    @JsonProperty("SIPR_PREMIUM")
    private Integer SIPR_PREMIUM;

    @JsonProperty("SIPR_PREMIUM_EXCLUDE_ADJUST")
    private Integer SIPR_PREMIUM_EXCLUDE_ADJUST;

    @JsonProperty("SPECIFIC_UPLIFT_AMONUNT")
    private Integer SPECIFIC_UPLIFT_AMONUNT;

    @JsonProperty("SPECIFIC_UPLIFT_PERCENT")
    private Integer SPECIFIC_UPLIFT_PERCENT;

    @JsonProperty("STAMP")
    private Integer STAMP;

    @JsonProperty("SUB_AGENT_CODE")
    private String SUB_AGENT_CODE;

    @JsonProperty("SUB_AGENT_NAME")
    private String SUB_AGENT_NAME;

    @JsonProperty("SUM_INSURED")
    private Integer SUM_INSURED;

    @JsonProperty("TAX")
    private Float TAX;

    @JsonProperty("TITLE")
    private String TITLE;

    @JsonProperty("TOTAL_DAY")
    private Integer TOTAL_DAY;

    @JsonProperty("TOTAL_PREMIUM")
    private Float TOTAL_PREMIUM;

    @JsonProperty("TPBIAV_ACCIDENT_AMOUNT")
    private Integer TPBIAV_ACCIDENT_AMOUNT;

    @JsonProperty("TPBIAV_EXCLUDE_PREM")
    private Integer TPBIAV_EXCLUDE_PREM;

    @JsonProperty("TPBIAV_PERSON_AMOUNT")
    private Integer TPBIAV_PERSON_AMOUNT;

    @JsonProperty("TPBIAV_PREMIUM")
    private Integer TPBIAV_PREMIUM;

    @JsonProperty("TPPDAV_AMOUNT")
    private Integer TPPDAV_AMOUNT;

    @JsonProperty("TPPDAV_EXCLUDE_PREM")
    private Integer TPPDAV_EXCLUDE_PREM;

    @JsonProperty("TPPDAV_PREMIUM")
    private Integer TPPDAV_PREMIUM;

    @JsonProperty("TUMBON")
    private String TUMBON;

    @JsonProperty("UNIT_REMARKS")
    private String UNIT_REMARKS;

    @JsonProperty("UPLIFT_FLAT_AMOUNT")
    private Integer UPLIFT_FLAT_AMOUNT;

    @JsonProperty("UPLIFT_PERCENT")
    private Integer UPLIFT_PERCENT;

    @JsonProperty("VEHICLE_BODY_TYPE")
    private String VEHICLE_BODY_TYPE;

    @JsonProperty("VEHICLE_CAPACITY_KEY")
    private String VEHICLE_CAPACITY_KEY;

    @JsonProperty("VEHICLE_CHASSIS_NO")
    private String VEHICLE_CHASSIS_NO;

    @JsonProperty("VEHICLE_CODE_DESC")
    private String VEHICLE_CODE_DESC;

    @JsonProperty("VEHICLE_COLOR")
    private String VEHICLE_COLOR;

    @JsonProperty("VEHICLE_ENGINE_CAPACITY")
    private Integer VEHICLE_ENGINE_CAPACITY;

    @JsonProperty("VEHICLE_ENGINE_NO")
    private String VEHICLE_ENGINE_NO;

    @JsonProperty("VEHICLE_GARAGE_LOCATION")
    private String VEHICLE_GARAGE_LOCATION;

    @JsonProperty("VEHICLE_LICENSE_NO")
    private String VEHICLE_LICENSE_NO;

    @JsonProperty("VEHICLE_MAKE")
    private String VEHICLE_MAKE;

    @JsonProperty("VEHICLE_MANUFACTURE_YEAR")
    private String VEHICLE_MANUFACTURE_YEAR;

    @JsonProperty("VEHICLE_MODEL")
    private String VEHICLE_MODEL;

    @JsonProperty("VEHICLE_MODEL_TYPE")
    private String VEHICLE_MODEL_TYPE;

    @JsonProperty("VEHICLE_NO_OF_SEAT")
    private Integer VEHICLE_NO_OF_SEAT;

    @JsonProperty("VEHICLE_PREFIX_LICENSE_NO")
    private String VEHICLE_PREFIX_LICENSE_NO;

    @JsonProperty("VEHICLE_PROVINCE")
    private String VEHICLE_PROVINCE;

    @JsonProperty("VEHICLE_REG_YEAR")
    private String VEHICLE_REG_YEAR;

    @JsonProperty("VEHICLE_STICKER_NO")
    private String VEHICLE_STICKER_NO;

    @JsonProperty("VEHICLE_WEIGHT")
    private Integer VEHICLE_WEIGHT;

    @JsonProperty("WS_CALL_FROM")
    private String WS_CALL_FROM;
}
