package com.lmig.icm.thailand.endorsement.controller;

import com.lmig.icm.thailand.endorsement.model.Beneficiary;
import com.lmig.icm.thailand.endorsement.model.Endorsement;
import com.lmig.icm.thailand.endorsement.model.Province;
import com.lmig.icm.thailand.endorsement.model.Role;
import com.lmig.icm.thailand.endorsement.payload.response.ApiResponse;
import com.lmig.icm.thailand.endorsement.payload.response.RoleModules;
import com.lmig.icm.thailand.endorsement.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/resources")
public class ResourceController {

    @Autowired
    private ResourceService resourceService;

    @GetMapping("/provinces")
    public ResponseEntity<?> getProvinces() {

        List<Province> provinces = resourceService.getProvinces();

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", provinces.size(), provinces, null));
    }

    @GetMapping("/provinces/upload")
    public ResponseEntity<?> uploadProvinces()
            throws FileNotFoundException {

        List<Province> provinces = resourceService.uploadProvinces();

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", provinces.size(), provinces, null));
    }

    @GetMapping("/provinces/excel")
    public ResponseEntity<?> getProvincesFromExcel() {

        List<Province> provinces = new ArrayList<>();

        try {

            provinces = resourceService.getProvincesFromExcel();

            ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", provinces.size(), provinces, null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch(FileNotFoundException e) {

            e.printStackTrace();

            ApiResponse response = new ApiResponse(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.name(), "Failed!", provinces.size(), provinces, null);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/endorsements")
    public ResponseEntity<?> getEndorsements() {

        List<Endorsement> endorsements = resourceService.getEndorsements();

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", endorsements.size(), endorsements, null));
    }

    @GetMapping("/endorsements/upload")
    public ResponseEntity<?> uploadEndorsements()
            throws FileNotFoundException {

        List<Endorsement> endorsements = resourceService.uploadEndorsements();

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", endorsements.size(), endorsements, null));
    }

    @GetMapping("/endorsements/excel")
    public ResponseEntity<?> getEndorsementsFromExcel() {

        List<Endorsement> endorsements = new ArrayList<>();

        try {

            endorsements = resourceService.getEndorsementsFromExcel();

            ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", endorsements.size(), endorsements, null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch(FileNotFoundException e) {

            e.printStackTrace();

            ApiResponse response = new ApiResponse(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.name(), "Failed!", endorsements.size(), endorsements, null);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/beneficiaries")
    public ResponseEntity<?> getBeneficiaries() {

        List<Beneficiary> beneficiaries = resourceService.getBeneficiaries();

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", beneficiaries.size(), beneficiaries, null));
    }

    @GetMapping("/beneficiaries/upload")
    public ResponseEntity<?> uploadBeneficiaries()
            throws FileNotFoundException {

        List<Beneficiary> beneficiaries = resourceService.uploadBeneficiaries();

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", beneficiaries.size(), beneficiaries, null));
    }

    @GetMapping("/beneficiaries/excel")
    public ResponseEntity<?> getBeneficiariesFromExcel() {

        List<Beneficiary> beneficiaries = new ArrayList<>();

        try {

            beneficiaries = resourceService.getBeneficiariesFromExcel();

            ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", beneficiaries.size(), beneficiaries, null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch(FileNotFoundException e) {

            e.printStackTrace();

            ApiResponse response = new ApiResponse(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.name(), "Failed!", beneficiaries.size(), beneficiaries, null);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/roles")
    public ResponseEntity<?> getRoles() {

        List<Role> roles = resourceService.getRoles();

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", roles.size(), roles, null));
    }

    @GetMapping("/roles/upload")
    public ResponseEntity<?> uploadRoles()
            throws FileNotFoundException {

        List<Role> roles = resourceService.uploadRoles();

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", roles.size(), roles, null));
    }

    @GetMapping("/roles/excel")
    public ResponseEntity<?> getRolesFromExcel() {

        List<Role> roles = new ArrayList<>();

        try {

            roles = resourceService.getRolesFromExcel();

            ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", roles.size(), roles, null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch(FileNotFoundException e) {

            e.printStackTrace();

            ApiResponse response = new ApiResponse(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.name(), "Failed!", roles.size(), roles, null);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/roles/{roleName}")
    public ResponseEntity<?> getRoleModulesByRoleName(@Valid @PathVariable String roleName) {

        RoleModules roleModules = resourceService.getRoleModulesByRoleName(roleName);

        return new ResponseEntity<>(roleModules, HttpStatus.OK);
    }
}
