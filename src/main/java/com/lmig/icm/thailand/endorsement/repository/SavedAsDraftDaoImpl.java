package com.lmig.icm.thailand.endorsement.repository;

import com.lmig.icm.thailand.endorsement.model.SavedAsDraft;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class SavedAsDraftDaoImpl implements SavedAsDraftDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final class SavedAsDraftRowMapper implements RowMapper<SavedAsDraft> {

        @Override
        public SavedAsDraft mapRow(final ResultSet rs, final int rowNum) throws SQLException {

            return new SavedAsDraft(
                    rs.getLong("ID"),
                    rs.getString("CATEGORY"),
                    rs.getString("CONTENT"),
                    rs.getString("POLICY_NO"),
                    rs.getString("CREATOR"),
                    rs.getString("CREATED_DATE"));
        }
    }

    @Override
    public boolean insert(SavedAsDraft savedAsDraft) {

        jdbcTemplate.update("INSERT INTO SAVED_AS_DRAFT (" +
                        "CATEGORY, " +
                        "CONTENT, " +
                        "POLICY_NO, " +
                        "CREATOR, " +
                        "CREATED_DATE) " +
                        "VALUES (?, ?, ?, ?, ?)",
                savedAsDraft.getCategory(),
                savedAsDraft.getContent(),
                savedAsDraft.getPolicyNo(),
                savedAsDraft.getCreator(),
                savedAsDraft.getCreated_date());

        return true;
    }

    @Override
    public boolean update(String content, Integer savedAsDraftId) {

        jdbcTemplate.update("UPDATE SAVED_AS_DRAFT " +
                        "SET CONTENT = ? " +
                        "WHERE ID = ? ",
                content,
                savedAsDraftId);

        return true;
    }

    @Override
    public List<SavedAsDraft> getDraftsById(Integer savedAsDraftId) {

        return jdbcTemplate.query("SELECT * FROM SAVED_AS_DRAFT " +
                        "WHERE ID = ? ",
                new Object[] { savedAsDraftId },
                new SavedAsDraftDaoImpl.SavedAsDraftRowMapper());
    }

    @Override
    public List<SavedAsDraft> getDraftsByCategory(String category, String creator) {

        return jdbcTemplate.query("SELECT * FROM SAVED_AS_DRAFT " +
                        "WHERE CATEGORY = ? " +
                        "AND CREATOR = ? " +
                        "ORDER BY ID DESC ",
                new Object[] { category, creator },
                new SavedAsDraftDaoImpl.SavedAsDraftRowMapper());
    }

    @Override
    public List<SavedAsDraft> getDraftsByPolicyNo(String category, String policyNo, String creator) {

        return jdbcTemplate.query("SELECT * FROM SAVED_AS_DRAFT " +
                        "WHERE CATEGORY = ? " +
                        "AND POLICY_NO = ? " +
                        "AND CREATOR = ? " +
                        "ORDER BY ID DESC ",
                new Object[] { category, policyNo, creator },
                new SavedAsDraftDaoImpl.SavedAsDraftRowMapper());
    }

    @Override
    public List<SavedAsDraft> getLastDraftsByPolicyNo(String category, String policyNo, String creator) {

        if(policyNo != null) {

            return jdbcTemplate.query("SELECT * FROM SAVED_AS_DRAFT " +
                            "WHERE CATEGORY = ? " +
                            "AND POLICY_NO = ? " +
                            "AND CREATOR = ? " +
                            "ORDER BY ID DESC ",
                    new Object[] { category, policyNo, creator },
                    new SavedAsDraftDaoImpl.SavedAsDraftRowMapper());
        } else {

            List<SavedAsDraft> draftsOfCreator = jdbcTemplate.query("SELECT * FROM SAVED_AS_DRAFT " +
                            "WHERE CATEGORY = ? " +
                            "AND CREATOR = ? " +
                            "ORDER BY ID DESC ",
                    new Object[] { category, creator },
                    new SavedAsDraftDaoImpl.SavedAsDraftRowMapper());

            Set<String> policyNumbers = new HashSet<>();

            for(SavedAsDraft savedAsDraft : draftsOfCreator) {

                policyNumbers.add(savedAsDraft.getPolicyNo());
            }

            List<SavedAsDraft> results = new ArrayList<>();

            for (String policyNumber : policyNumbers) {

                SavedAsDraft draftOfCreator = jdbcTemplate.queryForObject("SELECT * FROM SAVED_AS_DRAFT " +
                                "WHERE CATEGORY = ? " +
                                "AND POLICY_NO = ? " +
                                "AND CREATOR = ? " +
                                "ORDER BY ID DESC " +
                                "LIMIT 1 ",
                        new Object[] { category, policyNumber, creator },
                        new SavedAsDraftDaoImpl.SavedAsDraftRowMapper());

                results.add(draftOfCreator);
            }

            return results;
        }
    }

    @Override
    public boolean delete(Integer savedAsDraftId) {

        String sql = "DELETE FROM SAVED_AS_DRAFT " +
                "WHERE ID = ? ";
        Object[] args = new Object[] { savedAsDraftId };

        return jdbcTemplate.update(sql, args) == 1;
    }

    @Override
    public boolean deleteByPolicyNo(String category, String policyNo) {

        String sql = "DELETE FROM SAVED_AS_DRAFT " +
                "WHERE CATEGORY = ? AND POLICY_NO = ? ";
        Object[] args = new Object[] { category, policyNo };

        return jdbcTemplate.update(sql, args) == 1;
    }

    @Override
    public List<SavedAsDraft> getDraftsByStatus(String category, String savedAsDraftStatus) {

        return jdbcTemplate.query("SELECT * FROM SAVED_AS_DRAFT " +
                        "WHERE CATEGORY = ? " +
                        "AND STATUS = ? " +
                        "ORDER BY ID DESC ",
                new Object[] { category, savedAsDraftStatus },
                new SavedAsDraftDaoImpl.SavedAsDraftRowMapper());
    }

    @Override
    public List<SavedAsDraft> getDraftsByHistory(String category) {

        return jdbcTemplate.query("SELECT * FROM SAVED_AS_DRAFT " +
                        "WHERE CATEGORY = ? " +
                        "AND (STATUS = 'STATUS_PROCESSED' OR STATUS = 'STATUS_REJECTED') " +
                        "ORDER BY ID DESC ",
                new Object[] { category },
                new SavedAsDraftDaoImpl.SavedAsDraftRowMapper());
    }
}
