package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsNebulaFileDownload {

    private String message;

    private String status;

    private String error;

    private String docObject;

    private String docName;

    private String mimeType;

    private String contentUrl;
}
