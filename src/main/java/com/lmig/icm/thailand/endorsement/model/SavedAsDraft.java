package com.lmig.icm.thailand.endorsement.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SavedAsDraft {

    private long id;

    private String category;

    private String content;

    private String policyNo;

    private String creator;

    private String created_date;
}
