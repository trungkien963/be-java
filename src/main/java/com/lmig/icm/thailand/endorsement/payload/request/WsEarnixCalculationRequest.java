package com.lmig.icm.thailand.endorsement.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsEarnixCalculationRequest {

    private String clientService;

    private String earnixEndpoint;

    private String callbackEndpoint;

    private List<WsEarnixPolicyRequest> policyRequest;
}
