package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsPolicyHistory {

    @JsonProperty("DRIVER1_NAME")
    private String DRIVER1_NAME;

    @JsonProperty("DRIVER2_NAME")
    private String DRIVER2_NAME;

    @JsonProperty("POLICY_NO")
    private String POLICY_NO;

    @JsonProperty("SETTLEMENT_DATE")
    private String SETTLEMENT_DATE;

    @JsonProperty("SETTLEMENT_STATUS")
    private String SETTLEMENT_STATUS;

    @JsonProperty("TAX_NO")
    private String TAX_NO;
}
