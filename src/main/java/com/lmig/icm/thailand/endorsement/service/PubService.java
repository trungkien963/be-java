package com.lmig.icm.thailand.endorsement.service;

import com.lmig.icm.thailand.endorsement.payload.request.PubLoginRequest;
import com.lmig.icm.thailand.endorsement.payload.response.*;

public interface PubService {

    PubLoginResponse login(PubLoginRequest pubLoginRequest);

    PubForms getForms();

    PubFormDetails getFormById(String formId);

    WsPubFormStatus upload(WsPubFormDetails wsPubFormDetails);

    WsPubFormStatus download(String requestId);
}