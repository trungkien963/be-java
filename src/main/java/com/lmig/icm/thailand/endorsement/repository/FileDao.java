package com.lmig.icm.thailand.endorsement.repository;

import com.lmig.icm.thailand.endorsement.model.PolicyEndorsementFile;

import java.nio.file.Path;
import java.util.List;

public interface FileDao {

    Path load(String fullName);

    boolean insert(PolicyEndorsementFile policyEndorsementFile);

    List<PolicyEndorsementFile> getFiles(String endorsementId);

    List<PolicyEndorsementFile> getFiles(String policyNo, String endorsementType);

    boolean delete(String fullName);
}
