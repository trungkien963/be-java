package com.lmig.icm.thailand.endorsement.controller;

import com.lmig.icm.thailand.endorsement.model.PolicyEndorsementFile;
import com.lmig.icm.thailand.endorsement.payload.request.FileUploadRequest;
import com.lmig.icm.thailand.endorsement.payload.response.ApiResponse;
import com.lmig.icm.thailand.endorsement.payload.response.WsNebulaFileDownload;
import com.lmig.icm.thailand.endorsement.payload.response.WsPubFormDetails;
import com.lmig.icm.thailand.endorsement.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/files")
public class FileController {

    @Autowired
    private FileService fileService;

    @GetMapping
    public ResponseEntity<?> getFiles(
            @Valid @RequestParam(value = "endorsementId", required = false) String endorsementId,
            @Valid @RequestParam(value = "policyNo", required = false) String policyNo,
            @Valid @RequestParam(value = "endorsementType", required = false) String endorsementType) {

        List<PolicyEndorsementFile> results = null;

        if(endorsementId != null) {

            results = fileService.getFiles(endorsementId);
        } else {

            results = fileService.getFiles(policyNo, endorsementType);
        }

        ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> handleFileUpload(
            @Valid @RequestParam("files") MultipartFile[] files,
            @Valid @RequestParam("fileType") String fileType,
            @Valid @RequestParam("endorsementId") String endorsementId,
            @Valid @RequestParam("policyNo") String policyNo,
            @Valid @RequestParam("endorsementType") String endorsementType) {

        List<?> results = new ArrayList<>();

        FileUploadRequest fileUploadRequest = new FileUploadRequest(fileType, endorsementId, policyNo, endorsementType);

        try {

            results = fileService.store(files, fileUploadRequest);

            ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();

            ApiResponse response = new ApiResponse(HttpStatus.GATEWAY_TIMEOUT.value(), HttpStatus.GATEWAY_TIMEOUT.name(), "Failed!", results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.GATEWAY_TIMEOUT);
        }
    }

    @GetMapping("/{fullName}")
    public ResponseEntity<Resource> serveFile(@PathVariable String fullName) {

        Resource file = fileService.loadAsResource(fullName);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @DeleteMapping("/{fullName}")
    public ResponseEntity<?> deleteFile(@PathVariable String fullName) {

        List<PolicyEndorsementFile> results = new ArrayList<>();

        fileService.delete(fullName);

        ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/policies/{policyNo}/nebula")
    public ResponseEntity<?> uploadFileToNebula(
            @Valid @PathVariable String policyNo,
            @Valid @RequestBody WsPubFormDetails wsPubFormDetails)
            throws IOException {

        WsNebulaFileDownload response = fileService.uploadFileToNebula(policyNo, wsPubFormDetails);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
