package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsPubLoginResponse {

    private String refresh_token_expires_in;

    private String api_product_list;

    private List<String> api_product_list_json;

    private String organization_name;

    @JsonProperty("developer.email")
    private String developer_email;

    private String token_type;

    private String issued_at;

    private String client_id;

    private String access_token;

    private String application_name;

    private String scope;

    private String expires_in;

    private String refresh_count;

    private String status;
}
