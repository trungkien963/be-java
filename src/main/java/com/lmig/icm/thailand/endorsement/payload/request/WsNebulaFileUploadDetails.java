package com.lmig.icm.thailand.endorsement.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsNebulaFileUploadDetails {

    @JsonProperty("country")
    private String country;

    @JsonProperty("lob")
    private String lob;

    @JsonProperty("createdBy")
    private String createdBy;

    @JsonProperty("sourceSystem")
    private String sourceSystem;

    @JsonProperty("transactionDetails")
    private WsNebulaFileUploadDetailsTransaction transactionDetails;

    @JsonProperty("documentDetails")
    private WsNebulaFileUploadDetailsDocument documentDetails;
}
