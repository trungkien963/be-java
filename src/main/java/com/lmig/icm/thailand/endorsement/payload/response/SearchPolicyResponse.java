package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SearchPolicyResponse {

    private String batchNumber;

    private String policyNo;

    private String entryDate;

    private String applicationDate;

    private String signDate;

    private String postDate;

    private String policyStatus;

    private String renewalFlag;

    private String lastEndorsementNo;

    private String agentCode;

    private String vehicleChassisNo;

    private String wsCallFrom;
}
