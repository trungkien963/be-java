package com.lmig.icm.thailand.endorsement.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Beneficiary {

    private long id;

    private Integer number;

    private String code;

    private String beneficiary;

    private String uploader;

    private String uploaded_date;
}
