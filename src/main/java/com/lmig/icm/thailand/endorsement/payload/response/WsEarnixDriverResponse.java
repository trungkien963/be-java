package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsEarnixDriverResponse {

    private String driverAge;

    private String driverAgeExpiring;

    private String driverOccupation;

    private String driverOccupationCode;

    private String driverOccupationCodeExpiring;

    private String driverOccupationExpiring;

    private String driverSex;

    private String driverSexExpiring;
}
