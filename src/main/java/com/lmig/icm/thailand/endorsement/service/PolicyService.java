package com.lmig.icm.thailand.endorsement.service;

import com.lmig.icm.thailand.endorsement.model.Policy;
import com.lmig.icm.thailand.endorsement.model.PolicyEndorsement;
import com.lmig.icm.thailand.endorsement.model.PolicyInfo;
import com.lmig.icm.thailand.endorsement.payload.request.*;
import com.lmig.icm.thailand.endorsement.payload.response.*;

import java.util.List;

public interface PolicyService {

    List<Policy> getPolicies(String policyNo);

    ReturnResult searchPolicies(SearchPolicyRequest searchPolicyRequest);

    PolicyDetailsResponse getPolicyDetails(PolicyDetailsRequest policyDetailsRequest);

    List<PolicyInfo> getPolicyInfos(GetPolicyInfoRequest getPolicyInfoRequest);

    ApiResponse addEndorsement(String policyNo, AddEndorsementRequest addEndorsementRequest);

    List<PolicyEndorsement> updateEndorsement(String policyNo, UpdateEndorsementRequest updateEndorsementRequest);

    CreateEndorsementResponse createEndorsement(String policyNo, Integer endorsable, CreateEndorsementRequest createEndorsementRequest);

    boolean delete(String policyNo, Integer endorsementId);

    List<PolicyEndorsement> getAllEndorsements();

    List<PolicyEndorsement> getAllEndorsements(Integer gettable);

    List<PolicyEndorsement> getAllEndorsementsByStatus(String status);

    List<PolicyEndorsement> getEndorsementsByPolicyNo(String policyNo, Integer gettable);

    List<PolicyEndorsement> getEndorsementsByStatus(String policyNo, String status);

    List<CheckEndorsementStatusResponse> checkEndorsementStatus(CheckEndorsementStatusRequest checkEndorsementStatusRequest);
}