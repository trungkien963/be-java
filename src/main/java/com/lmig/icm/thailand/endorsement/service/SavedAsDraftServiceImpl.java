package com.lmig.icm.thailand.endorsement.service;

import com.lmig.icm.thailand.endorsement.enumType.SavedAsDraftType;
import com.lmig.icm.thailand.endorsement.model.SavedAsDraft;
import com.lmig.icm.thailand.endorsement.payload.request.SavedAsDraftRequest;
import com.lmig.icm.thailand.endorsement.repository.SavedAsDraftDao;
import com.lmig.icm.thailand.endorsement.util.DateUtility;
import com.lmig.icm.thailand.endorsement.util.UserUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SavedAsDraftServiceImpl implements SavedAsDraftService {

    @Autowired
    private SavedAsDraftDao savedAsDraftDao;

    @Override
    public List<SavedAsDraft> saveEndorsementAsDraft(SavedAsDraftRequest savedAsDraftRequest) {

        String creator = UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null;

        SavedAsDraft savedAsDraft = new SavedAsDraft();
        savedAsDraft.setCategory(SavedAsDraftType.ENDORSEMENT.toString());
        savedAsDraft.setContent(savedAsDraftRequest.getContent());
        savedAsDraft.setPolicyNo(savedAsDraftRequest.getPolicyNo());
        savedAsDraft.setCreator(creator);
        savedAsDraft.setCreated_date(DateUtility.now());

        savedAsDraftDao.insert(savedAsDraft);

        return savedAsDraftDao.getDraftsByCategory(SavedAsDraftType.ENDORSEMENT.toString(), creator);
    }

    @Override
    public List<SavedAsDraft> updateEndorsementAsDraftById(String content, Integer savedAsDraftId) {

        savedAsDraftDao.update(content, savedAsDraftId);

        return savedAsDraftDao.getDraftsById(savedAsDraftId);
    }

    @Override
    public List<SavedAsDraft> getDraftsById(Integer savedAsDraftId) {

        return savedAsDraftDao.getDraftsById(savedAsDraftId);
    }

    @Override
    public List<SavedAsDraft> getDraftsByCategory(String category) {

        String creator = UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null;

        return savedAsDraftDao.getDraftsByCategory(category, creator);
    }

    @Override
    public List<SavedAsDraft> getDraftsByPolicyNo(String category, String policyNo) {

        String creator = UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null;

        return savedAsDraftDao.getDraftsByPolicyNo(category, policyNo, creator);
    }

    @Override
    public List<SavedAsDraft> getLastDraftsByPolicyNo(String category, String policyNo) {

        String creator = UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null;

        return savedAsDraftDao.getLastDraftsByPolicyNo(category, policyNo, creator);
    }

    @Override
    public boolean delete(Integer savedAsDraftId) {

        return savedAsDraftDao.delete(savedAsDraftId);
    }

    @Override
    public boolean deleteByPolicyNo(String policyNo) {

        return savedAsDraftDao.deleteByPolicyNo(SavedAsDraftType.ENDORSEMENT.toString(), policyNo);
    }

    @Override
    public List<SavedAsDraft> getDraftsByStatus(String category, String savedAsDraftStatus) {

        return savedAsDraftDao.getDraftsByStatus(category, savedAsDraftStatus);
    }

    @Override
    public List<SavedAsDraft> getDraftsByHistory(String category) {

        return savedAsDraftDao.getDraftsByHistory(category);
    }
}
