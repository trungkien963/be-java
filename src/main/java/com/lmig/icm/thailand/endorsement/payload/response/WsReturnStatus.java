package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsReturnStatus {

    private String batch_number;

    private String policy_no_par;

    private String vehicle_prefix_license_no_par;

    private String vehicle_license_no_par;

    private String province_license_no_par;

    private String insured_name_par;

    private String date_from_par;

    private String date_to_par;

    private String corp_id_par;

    private String policy_line_of_business_par;

    private String policy_serial_number_par;

    private String policy_serial_suffix_par;

    private String policy_effective_year_par;

    private String policy_effective_month_par;

    private String agent_code_par;

    private String vehicle_chassis_no_par;

    private String ws_return_code;

    private String ws_return_description;
}
