package com.lmig.icm.thailand.endorsement.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MemoInfo {

    private String batchNumber;

    private String batchKey;

    private String referenceNo;

    private String policyNo;

    private String memoCode;

    private String memoHeader;

    private String memoDetail;
}
