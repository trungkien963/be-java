package com.lmig.icm.thailand.endorsement.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsEarnixDriverRequest {

    private String driverAge;

    private String driverAgeExpiring;

    private String driverOccupation;

    private String driverOccupationCode;

    private String driverOccupationCodeExpiring;

    private String driverOccupationExpiring;

    private String driverSex;

    private String driverSexExpiring;
}
