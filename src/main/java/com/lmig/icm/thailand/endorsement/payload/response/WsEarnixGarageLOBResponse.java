package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsEarnixGarageLOBResponse {

    private String acCaCode;

    private Float additionalPremium;

    private Integer annualizedAdditionalPremium;

    private Integer annualizedNetPremium;

    private Integer basePremium;

    private String caCode;

    private String classificationOptimization;

    private Integer collBasePremium;

    private Integer collMainPremium;

    private Float dryLossCost;

    private Integer fleetDiscountAmount;

    private Integer fleetDiscountPercent;

    private Float freshLossCost;

    private Float grossPremium;

    private Integer grossPremiumOver1Year;

    private Integer impliedElasticity;

    private String lobGarage;

    private Integer mainPremium;

    private Integer ncbAmount;

    private Float ncbPercent;

    private Integer netPremium;

    private Integer netPremiumOver1Year;

    private Integer odDeductAmount;

    private Integer odDeductDiscountAmount;

    private Integer otherDiscountAmount;

    private Integer otherDiscountPercent;

    private Integer period1Days;

    private Integer period1PremiumPercent;

    private Integer period2Days;

    private Integer period2GrossPremium;

    private Integer period2PremiumPercent;

    private Integer period3Days;

    private Integer period3GrossPremium;

    private Integer period3PremiumPercent;

    private String productCode;

    private Integer stampAmount;

    private Integer stampAmountOver1Year;

    private Integer sumInsured;

    private Integer surchargeAmount;

    private Integer surchargePercent;

    private Integer targetNetPremium;

    private Float taxAmount;

    private Integer taxAmountOver1Year;

    private Float totalGrossPremium;

    private Integer tpDeductAmount;

    private Integer tpDeductDiscountAmount;

    @JsonProperty("Coverage")
    private List<WsEarnixCoverageResponse> coverage;
}
