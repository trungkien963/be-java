package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsAegisCreateEndorsementReturnCode {

    @JsonProperty("BATCH_NUMBER")
    private String batchNumber;

    @JsonProperty("BATCH_KEY")
    private String batchKey;

    @JsonProperty("REFERENCE_NO")
    private String referenceNo;

    @JsonProperty("WS_RETURN_CODE")
    private String wsReturnCode;

    @JsonProperty("WS_RETURN_DESCRIPTION")
    private String wsReturnDescription;

    @JsonProperty("POLICY_NO_PAR")
    private String policyNoPar;

    @JsonProperty("POLICY_ENDORSEMENT_NO_PAR")
    private String policyEndorsementNoPar;

    public String getBatchNumber() {
        return (this.batchNumber == null ? "" : this.batchNumber);
    }

    public String getBatchKey() {
        return (this.batchKey == null ? "" : this.batchKey);
    }

    public String getReferenceNo() {
        return (this.referenceNo == null ? "" : this.referenceNo);
    }

    public String getWsReturnCode() {
        return (this.wsReturnCode == null ? "" : this.wsReturnCode);
    }

    public String getWsReturnDescription() {
        return (this.wsReturnDescription == null ? "" : this.wsReturnDescription);
    }

    public String getPolicyNoPar() {
        return (this.policyNoPar == null ? "" : this.policyNoPar);
    }

    public String getPolicyEndorsementNoPar() {
        return (this.policyEndorsementNoPar == null ? "" : this.policyEndorsementNoPar);
    }
}
