package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsPubFormUpload {

    private String requestId;

    private String clientRequestId;

    private String statusURL;

    private List<WsPubFormUploadStatusList> statusList;
}
