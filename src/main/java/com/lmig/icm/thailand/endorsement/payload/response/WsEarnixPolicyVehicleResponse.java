package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsEarnixPolicyVehicleResponse {

    @JsonProperty("4YearsPriorLossRatio")
    private Integer fourYearsPriorLossRatio;

    private Float acGrossPremium;

    private String acMotorCode;

    private Integer acNetPremium;

    private Integer acPolicyDays;

    private Integer acStampAmount;

    private Float acTaxAmount;

    private Float actuarialCostParameter;

    private Integer adjustmentRate;

    private String agentTypeDesc;

    private String agentTypeGroupsNew;

    private Integer atFaultClaimCountExpiring;

    private String atFaultClaimCountExpiringIndicator;

    private Integer atFaultLossRatioExpiring;

    private Integer atFaultRecoveryAmountExpiring;

    private Integer atFaultReserveAmountExpiring;

    private Integer dryClaimCountExpiring;

    private Integer expiringYearLossRatio;

    private String firstFinanceIndicator;

    private String firstFinanceIndicatorFMT;

    private Integer fleetIndicator;

    private Integer freshClaimCountExpiring;

    private String garage;

    private String lob;

    private Integer lossCostExpiring;

    private Float ncbBeforeBackfit;

    @JsonProperty("Optimization")
    private String optimization;

    private Integer optimizedMargin;

    @JsonProperty("Optimization Segment")
    private String optimizationSegment;

    private Integer policyDays;

    private String policyNumber;

    private Float predictedLossRatioCommission;

    private Integer premiumParameter;

    private Integer priceTestIndicator;

    private String priceTestNominal;

    private Float renewalDemand;

    private String renewDecisionCode;

    private String renewDecisionDesc;

    private String renewDecisionOpt;

    private String requestId;

    private String rrChangeInNCD2;

    private String rrChangeInNCDFo;

    private String sendTo;

    private Float tariffMaxVsExpiringPremiumLoop;

    private Float tariffMinVsExpiringPremiumLoop;

    private Integer totalClaimCountExpiring;

    private Integer totalCommissionRatio;

    private Integer totalLossAmtExpiring;

    private Float totalLossCost;

    private String v2RuleNominal;

    private String vehicleCodeDesc;

    private String vehicleCodeSimple;

    private String vehicleGroup;

    @JsonProperty("Driver")
    private List<WsEarnixDriverResponse> driver;

    @JsonProperty("Claims")
    private List<WsEarnixClaimsResponse> claims;

    @JsonProperty("GarageLOB")
    private List<WsEarnixGarageLOBResponse> garageLOB;
}
