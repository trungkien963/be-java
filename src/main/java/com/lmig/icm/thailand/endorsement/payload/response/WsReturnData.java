package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsReturnData {

    private String batch_number;

    private String policy_no;

    private String entry_date;

    private String application_date;

    private String sign_date;

    private String post_date;

    private String policy_status;

    private String renewal_flag;

    private String last_endorsement_no;

    private String agent_code;

    private String vehicle_chassis_no;

    private String ws_call_from;
}
