package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsGetCustomerDetailsResponse {

    @JsonProperty("BATCH_NUMBER")
    private String batchNumber;

    @JsonProperty("Customer_Info")
    private List<WsCustomerInfo> wsCustomerInfo;

    @JsonProperty("REFERENCE_NO")
    private String referenceNo;

    @JsonProperty("WS_RETURN_CODE")
    private String wsReturnCode;

    @JsonProperty("WS_RETURN_DESCRIPTION")
    private String wsReturnDescription;

    public String getBatchNumber() {
        return (this.batchNumber == null ? "" : this.batchNumber);
    }

    public List<WsCustomerInfo> getWsCustomerInfo() {
        return (this.wsCustomerInfo == null ? new ArrayList<>() : this.wsCustomerInfo);
    }

    public String getReferenceNo() {
        return (this.referenceNo == null ? "" : this.referenceNo);
    }

    public String getWsReturnCode() {
        return (this.wsReturnCode == null ? "" : this.wsReturnCode);
    }

    public String getWsReturnDescription() {
        return (this.wsReturnDescription == null ? "" : this.wsReturnDescription);
    }
}
