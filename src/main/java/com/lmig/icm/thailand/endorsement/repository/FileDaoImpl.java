package com.lmig.icm.thailand.endorsement.repository;

import com.lmig.icm.thailand.endorsement.config.StorageConfig;
import com.lmig.icm.thailand.endorsement.model.PolicyEndorsementFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class FileDaoImpl implements FileDao {

    private final Path rootLocation;

    @Autowired
    public FileDaoImpl(StorageConfig storageConfig) {

        this.rootLocation = Paths.get(storageConfig.getLocation());
    }

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final class PolicyEndorsementFileRowMapper implements RowMapper<PolicyEndorsementFile> {

        @Override
        public PolicyEndorsementFile mapRow(final ResultSet rs, final int rowNum) throws SQLException {

            return new PolicyEndorsementFile(
                    rs.getLong("ID"),
                    rs.getString("ENDORSEMENT_ID"),
                    rs.getString("POLICY_NO"),
                    rs.getString("ENDORSEMENT_TYPE"),
                    rs.getString("FILE_NAME"),
                    rs.getString("FULL_NAME"),
                    rs.getString("EXTENSION"),
                    rs.getString("FILE_PATH"),
                    rs.getString("UPLOADER"),
                    rs.getString("UPLOADED_DATE"));
        }
    }

    @Override
    public Path load(String fullName) {

        return rootLocation.resolve(fullName);
    }

    @Override
    public boolean insert(PolicyEndorsementFile policyEndorsementFile) {

        jdbcTemplate.update("INSERT INTO POLICY_ENDORSEMENT_FILE (" +
                        "ENDORSEMENT_ID, " +
                        "POLICY_NO, " +
                        "ENDORSEMENT_TYPE, " +
                        "FILE_NAME, " +
                        "FULL_NAME, " +
                        "EXTENSION, " +
                        "FILE_PATH, " +
                        "UPLOADER, " +
                        "UPLOADED_DATE) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                policyEndorsementFile.getEndorsementId(),
                policyEndorsementFile.getPolicyNo(),
                policyEndorsementFile.getEndorsementType(),
                policyEndorsementFile.getFileName(),
                policyEndorsementFile.getFullName(),
                policyEndorsementFile.getExtension(),
                policyEndorsementFile.getFilePath(),
                policyEndorsementFile.getUploader(),
                policyEndorsementFile.getUploadedDate());

        return true;
    }

    @Override
    public List<PolicyEndorsementFile> getFiles(String endorsementId) {

        return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT_FILE " +
                        "WHERE ENDORSEMENT_ID = ? ",
                new Object[] { endorsementId },
                new FileDaoImpl.PolicyEndorsementFileRowMapper());
    }

    @Override
    public List<PolicyEndorsementFile> getFiles(String policyNo, String endorsementType) {

        return jdbcTemplate.query("SELECT * FROM POLICY_ENDORSEMENT_FILE " +
                        "WHERE POLICY_NO = ? AND ENDORSEMENT_TYPE = ? ",
                new Object[] { policyNo, endorsementType },
                new FileDaoImpl.PolicyEndorsementFileRowMapper());
    }

    @Override
    public boolean delete(String fullName) {

        String sql = "DELETE FROM POLICY_ENDORSEMENT_FILE WHERE FULL_NAME = ?";
        Object[] args = new Object[] { fullName };

        return jdbcTemplate.update(sql, args) == 1;
    }
}
