package com.lmig.icm.thailand.endorsement.repository;

import com.lmig.icm.thailand.endorsement.model.Beneficiary;
import com.lmig.icm.thailand.endorsement.model.Endorsement;
import com.lmig.icm.thailand.endorsement.model.Province;
import com.lmig.icm.thailand.endorsement.model.Role;
import com.lmig.icm.thailand.endorsement.util.DateUtility;
import com.lmig.icm.thailand.endorsement.util.UserUtility;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ResourceDaoImpl implements ResourceDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final class ProvinceRowMapper implements RowMapper<Province> {

        @Override
        public Province mapRow(final ResultSet rs, final int rowNum) throws SQLException {

            return new Province(
                    rs.getLong("ID"),
                    rs.getString("KEY_FIELD"),
                    rs.getString("PROVINCE_CODE"),
                    rs.getString("PROVINCE_NAME"),
                    rs.getDouble("MOD_DATE"),
                    rs.getString("PROVINCE_NAME_ENG"),
                    rs.getString("PROVINCE_FLAG"));
        }
    }

    private static final class EndorsementRowMapper implements RowMapper<Endorsement> {

        @Override
        public Endorsement mapRow(final ResultSet rs, final int rowNum) throws SQLException {

            return new Endorsement(
                    rs.getLong("ID"),
                    rs.getString("TYPE"),
                    rs.getString("ENDORSEMENT"),
                    rs.getInt("PRIORITY"),
                    rs.getInt("FINALIZED_PRIORITY_AFTER_20210907"),
                    rs.getInt("PHASE"),
                    rs.getInt("RORJOR_NO"),
                    rs.getString("THAI_LANGUAGE"));
        }
    }

    private static final class BeneficiaryRowMapper implements RowMapper<Beneficiary> {

        @Override
        public Beneficiary mapRow(final ResultSet rs, final int rowNum) throws SQLException {

            return new Beneficiary(
                    rs.getLong("ID"),
                    rs.getInt("NUMBER"),
                    rs.getString("CODE"),
                    rs.getString("BENEFICIARY"),
                    rs.getString("UPLOADER"),
                    rs.getString("UPLOADED_DATE"));
        }
    }

    private static final class RoleRowMapper implements RowMapper<Role> {

        @Override
        public Role mapRow(final ResultSet rs, final int rowNum) throws SQLException {

            return new Role(
                    rs.getLong("ID"),
                    rs.getString("ROLE"),
                    rs.getString("MODULE"),
                    rs.getString("UPLOADER"),
                    rs.getString("UPLOADED_DATE"));
        }
    }

    @Override
    public List<Province> getProvinces() {

        return jdbcTemplate.query("SELECT * FROM PROVINCE", new ProvinceRowMapper());
    }

    @Override
    public List<Province> uploadProvinces() throws FileNotFoundException {

        jdbcTemplate.update("TRUNCATE PROVINCE RESTART IDENTITY");

        File file = ResourceUtils.getFile("classpath:files/province.xlsx");

        try {

            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet sheet = workbook.getSheetAt(0);

            for(int i = 1; i <= sheet.getLastRowNum(); i++) {

                Row row = sheet.getRow(i);

                Province province = new Province();

                for(int j = 0; j < row.getLastCellNum(); j++) {

                    Cell cell = row.getCell(j);

                    String cellValueString = null;
                    Double cellValueDouble = null;

                    if (cell.getCellTypeEnum() == CellType.STRING) {
                        cellValueString = cell.getStringCellValue() ;
                    } else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                        cellValueDouble = cell.getNumericCellValue();
                    }

                    switch(j) {
                        case 0:
                            province.setKeyField(cellValueString);
                            break;
                        case 1:
                            province.setProvinceCode(cellValueString);
                            break;
                        case 2:
                            province.setProvinceName(cellValueString);
                            break;
                        case 3:
                            province.setModDate(cellValueDouble);
                            break;
                        case 4:
                            province.setProvinceNameEng(cellValueString);
                            break;
                        case 5:
                            province.setProvinceFlag(cellValueString);
                            break;
                        default:
                    }
                }

                jdbcTemplate.update("INSERT INTO PROVINCE (" +
                                "KEY_FIELD, " +
                                "PROVINCE_CODE, " +
                                "PROVINCE_NAME, " +
                                "MOD_DATE, " +
                                "PROVINCE_NAME_ENG, " +
                                "PROVINCE_FLAG) " +
                                "VALUES (?, ?, ?, ?, ?, ?)",
                        province.getKeyField(),
                        province.getProvinceCode(),
                        province.getProvinceName(),
                        province.getModDate(),
                        province.getProvinceNameEng(),
                        province.getProvinceFlag());
            }
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

        return jdbcTemplate.query("SELECT * FROM PROVINCE", new ProvinceRowMapper());
    }

    @Override
    public List<Province> getProvincesFromExcel() throws FileNotFoundException {

        List<Province> provinces = new ArrayList<>();

        File file = ResourceUtils.getFile("classpath:files/province.xlsx");

        try {

            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet sheet = workbook.getSheetAt(0);

            for(int i = 1; i <= sheet.getLastRowNum(); i++) {

                Row row = sheet.getRow(i);

                Province province = new Province();

                for(int j = 0; j < row.getLastCellNum(); j++) {

                    Cell cell = row.getCell(j);

                    String cellValueString = null;
                    Double cellValueDouble = null;

                    if (cell.getCellTypeEnum() == CellType.STRING) {
                        cellValueString = cell.getStringCellValue() ;
                    } else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                        cellValueDouble = cell.getNumericCellValue();
                    }

                    switch(j) {
                        case 0:
                            province.setKeyField(cellValueString);
                            break;
                        case 1:
                            province.setProvinceCode(cellValueString);
                            break;
                        case 2:
                            province.setProvinceName(cellValueString);
                            break;
                        case 3:
                            province.setModDate(cellValueDouble);
                            break;
                        case 4:
                            province.setProvinceNameEng(cellValueString);
                            break;
                        case 5:
                            province.setProvinceFlag(cellValueString);
                            break;
                        default:
                    }
                }

                provinces.add(province);
            }
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

        return provinces;
    }

    @Override
    public List<Endorsement> getEndorsements() {

        return jdbcTemplate.query("SELECT * FROM ENDORSEMENT", new EndorsementRowMapper());
    }

    @Override
    public List<Endorsement> uploadEndorsements() throws FileNotFoundException {

        jdbcTemplate.update("TRUNCATE ENDORSEMENT RESTART IDENTITY");

        File file = ResourceUtils.getFile("classpath:files/endorsement.xlsx");

        try {

            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet sheet = workbook.getSheetAt(0);

            for(int i = 1; i <= sheet.getLastRowNum(); i++) {

                Row row = sheet.getRow(i);

                Endorsement endorsement = new Endorsement();

                for(int j = 0; j < row.getLastCellNum(); j++) {

                    Cell cell = row.getCell(j);

                    String cellValueString = null;
                    Double cellValueDouble = null;

                    if (cell.getCellTypeEnum() == CellType.STRING) {
                        cellValueString = cell.getStringCellValue() ;
                    } else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                        cellValueDouble = cell.getNumericCellValue();
                    }

                    switch(j) {
                        case 0:
                            endorsement.setType(cellValueString);
                            break;
                        case 1:
                            endorsement.setEndorsement(cellValueString);
                            break;
                        case 2:
                            endorsement.setPriority(cellValueDouble.intValue());
                            break;
                        case 3:
                            endorsement.setFinalizedPriorityAfter20210907(cellValueDouble.intValue());
                            break;
                        case 4:
                            endorsement.setPhase(cellValueDouble.intValue());
                            break;
                        case 5:
                            endorsement.setRorjorNo(cellValueDouble.intValue());
                            break;
                        case 6:
                            endorsement.setThaiLanguage(cellValueString);
                            break;
                        default:
                    }
                }

                jdbcTemplate.update("INSERT INTO ENDORSEMENT (" +
                                "TYPE, " +
                                "ENDORSEMENT, " +
                                "PRIORITY, " +
                                "FINALIZED_PRIORITY_AFTER_20210907, " +
                                "PHASE, " +
                                "RORJOR_NO, " +
                                "THAI_LANGUAGE) " +
                                "VALUES (?, ?, ?, ?, ?, ?, ?)",
                        endorsement.getType(),
                        endorsement.getEndorsement(),
                        endorsement.getPriority(),
                        endorsement.getFinalizedPriorityAfter20210907(),
                        endorsement.getPhase(),
                        endorsement.getRorjorNo(),
                        endorsement.getThaiLanguage());
            }
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

        return jdbcTemplate.query("SELECT * FROM ENDORSEMENT", new EndorsementRowMapper());
    }

    @Override
    public List<Endorsement> getEndorsementsFromExcel() throws FileNotFoundException {

        List<Endorsement> endorsements = new ArrayList<>();

        File file = ResourceUtils.getFile("classpath:files/endorsement.xlsx");

        try {

            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet sheet = workbook.getSheetAt(0);

            for(int i = 1; i <= sheet.getLastRowNum(); i++) {

                Row row = sheet.getRow(i);

                Endorsement endorsement = new Endorsement();

                for(int j = 0; j < row.getLastCellNum(); j++) {

                    Cell cell = row.getCell(j);

                    String cellValueString = null;
                    Double cellValueDouble = null;

                    if (cell.getCellTypeEnum() == CellType.STRING) {
                        cellValueString = cell.getStringCellValue() ;
                    } else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                        cellValueDouble = cell.getNumericCellValue();
                    }

                    switch(j) {
                        case 0:
                            endorsement.setType(cellValueString);
                            break;
                        case 1:
                            endorsement.setEndorsement(cellValueString);
                            break;
                        case 2:
                            endorsement.setPriority(cellValueDouble.intValue());
                            break;
                        case 3:
                            endorsement.setFinalizedPriorityAfter20210907(cellValueDouble.intValue());
                            break;
                        case 4:
                            endorsement.setPhase(cellValueDouble.intValue());
                            break;
                        case 5:
                            endorsement.setRorjorNo(cellValueDouble.intValue());
                            break;
                        case 6:
                            endorsement.setThaiLanguage(cellValueString);
                            break;
                        default:
                    }
                }

                endorsements.add(endorsement);
            }
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

        return endorsements;
    }

    @Override
    public List<Beneficiary> getBeneficiaries() {

        return jdbcTemplate.query("SELECT * FROM BENEFICIARY", new BeneficiaryRowMapper());
    }

    @Override
    public List<Beneficiary> uploadBeneficiaries() throws FileNotFoundException {

        String uploader = UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null;

        jdbcTemplate.update("TRUNCATE BENEFICIARY RESTART IDENTITY");

        File file = ResourceUtils.getFile("classpath:files/beneficiary.xlsx");

        try {

            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet sheet = workbook.getSheetAt(0);

            for(int i = 1; i <= sheet.getLastRowNum(); i++) {

                Row row = sheet.getRow(i);

                Beneficiary beneficiary = new Beneficiary();

                for(int j = 0; j < row.getLastCellNum(); j++) {

                    Cell cell = row.getCell(j);

                    String cellValueString = null;
                    Double cellValueDouble = null;

                    if (cell.getCellTypeEnum() == CellType.STRING) {
                        cellValueString = cell.getStringCellValue() ;
                    } else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                        cellValueDouble = cell.getNumericCellValue();
                    }

                    switch(j) {
                        case 0:
                            beneficiary.setNumber(cellValueDouble.intValue());
                            break;
                        case 1:
                            beneficiary.setCode(cellValueString);
                            break;
                        case 2:
                            beneficiary.setBeneficiary(cellValueString);
                            break;
                        default:
                    }
                }

                beneficiary.setUploader(uploader);
                beneficiary.setUploaded_date(DateUtility.now());

                jdbcTemplate.update("INSERT INTO BENEFICIARY (" +
                                "NUMBER, " +
                                "CODE, " +
                                "BENEFICIARY, " +
                                "UPLOADER, " +
                                "UPLOADED_DATE) " +
                                "VALUES (?, ?, ?, ?, ?)",
                        beneficiary.getNumber(),
                        beneficiary.getCode(),
                        beneficiary.getBeneficiary(),
                        beneficiary.getUploader(),
                        beneficiary.getUploaded_date());
            }
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

        return jdbcTemplate.query("SELECT * FROM BENEFICIARY", new BeneficiaryRowMapper());
    }

    @Override
    public List<Beneficiary> getBeneficiariesFromExcel() throws FileNotFoundException {

        List<Beneficiary> beneficiaries = new ArrayList<>();

        File file = ResourceUtils.getFile("classpath:files/beneficiary.xlsx");

        try {

            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet sheet = workbook.getSheetAt(0);

            for(int i = 1; i <= sheet.getLastRowNum(); i++) {

                Row row = sheet.getRow(i);

                Beneficiary beneficiary = new Beneficiary();

                for(int j = 0; j < row.getLastCellNum(); j++) {

                    Cell cell = row.getCell(j);

                    String cellValueString = null;
                    Double cellValueDouble = null;

                    if (cell.getCellTypeEnum() == CellType.STRING) {
                        cellValueString = cell.getStringCellValue() ;
                    } else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                        cellValueDouble = cell.getNumericCellValue();
                    }

                    switch(j) {
                        case 0:
                            beneficiary.setNumber(cellValueDouble.intValue());
                            break;
                        case 1:
                            beneficiary.setCode(cellValueString);
                            break;
                        case 2:
                            beneficiary.setBeneficiary(cellValueString);
                            break;
                        default:
                    }
                }

                beneficiary.setUploader(UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null);
                beneficiary.setUploaded_date(DateUtility.now());

                beneficiaries.add(beneficiary);
            }
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

        return beneficiaries;
    }

    @Override
    public List<Role> getRoles() {

        return jdbcTemplate.query("SELECT * FROM ROLE", new RoleRowMapper());
    }

    @Override
    public List<Role> uploadRoles() throws FileNotFoundException {

        String uploader = UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null;

        jdbcTemplate.update("TRUNCATE ROLE RESTART IDENTITY");

        File file = ResourceUtils.getFile("classpath:files/role.xlsx");

        try {

            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet sheet = workbook.getSheetAt(0);

            for(int i = 1; i <= sheet.getLastRowNum(); i++) {

                Row row = sheet.getRow(i);

                Role role = new Role();

                for(int j = 0; j < row.getLastCellNum(); j++) {

                    Cell cell = row.getCell(j);

                    String cellValueString = null;
                    Double cellValueDouble = null;

                    if (cell.getCellTypeEnum() == CellType.STRING) {
                        cellValueString = cell.getStringCellValue() ;
                    } else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                        cellValueDouble = cell.getNumericCellValue();
                    }

                    switch(j) {
                        case 0:
                            role.setRole(cellValueString);
                            break;
                        case 1:
                            role.setModule(cellValueString);
                            break;
                        default:
                    }
                }

                role.setUploader(uploader);
                role.setUploaded_date(DateUtility.now());

                jdbcTemplate.update("INSERT INTO ROLE (" +
                                "ROLE, " +
                                "MODULE, " +
                                "UPLOADER, " +
                                "UPLOADED_DATE) " +
                                "VALUES (?, ?, ?, ?)",
                        role.getRole(),
                        role.getModule(),
                        role.getUploader(),
                        role.getUploaded_date());
            }
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

        return jdbcTemplate.query("SELECT * FROM ROLE", new RoleRowMapper());
    }

    @Override
    public List<Role> getRolesFromExcel() throws FileNotFoundException {

        List<Role> roles = new ArrayList<>();

        File file = ResourceUtils.getFile("classpath:files/role.xlsx");

        try {

            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet sheet = workbook.getSheetAt(0);

            for(int i = 1; i <= sheet.getLastRowNum(); i++) {

                Row row = sheet.getRow(i);

                Role role = new Role();

                for(int j = 0; j < row.getLastCellNum(); j++) {

                    Cell cell = row.getCell(j);

                    String cellValueString = null;
                    Double cellValueDouble = null;

                    if (cell.getCellTypeEnum() == CellType.STRING) {
                        cellValueString = cell.getStringCellValue() ;
                    } else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
                        cellValueDouble = cell.getNumericCellValue();
                    }

                    switch(j) {
                        case 0:
                            role.setRole(cellValueString);
                            break;
                        case 1:
                            role.setModule(cellValueString);
                            break;
                        default:
                    }
                }

                role.setUploader(UserUtility.getUserInfo() != null ? UserUtility.getUserInfo().getNickname() : null);
                role.setUploaded_date(DateUtility.now());

                roles.add(role);
            }
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

        return roles;
    }

    @Override
    public List<Role> getRoleModulesByRoleName(String roleName) {

        return jdbcTemplate.query("SELECT * FROM ROLE " +
                        "WHERE ROLE = ? ",
                new Object[] { roleName },
                new ResourceDaoImpl.RoleRowMapper());
    }
}
