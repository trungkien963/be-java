package com.lmig.icm.thailand.endorsement.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.lmig.icm.thailand.endorsement.payload.response.WsCustomerInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PolicyInfo {

    private long id;

    private String batchNumber;

    private String batchKey;

    private String referenceNo;

    private String policyNo;

    private String officeCode;

    private String policyEffectiveDate;

    private String policyExpirationDate;

    private String totalDay;

    private String agentCode;

    private String agentName;

    private String financeAgentTypeFlag;

    private String directAgentTypeFlag;

    private String agentTypeKey;

    private String brokerCode;

    private String brokerName;

    private String parentAgentCode;

    private String parentAgentName;

    private String subAgentCode;

    private String subAgentName;

    private String gprm;

    private String tax;

    private String stamp;

    private String totalPremium;

    private String signDate;

    private String entryDate;

    private String insuredCode;

    private String insuredType;

    private String title;

    private String insuredName;

    private String occupationCode;

    private String occupation;

    private String addressLine1;

    private String tumbon;

    private String district;

    private String province;

    private String postalCode;

    private String motorCode;

    private String motorSubCode;

    private String motorCodeInfo;

    private String vehiclePrefixLicenseNo;

    private String vehicleLicenseNo;

    private String vehicleProvince;

    private String keyField;

    private String vehicleChassisNo;

    private String vehicleEngineNo;

    private String vehicleCapacityKey;

    private String groupOfVehicle;

    private String vehicleRegYear;

    private String additionalEquipmentFlag;

    private String vehicleNoOfSeat;

    private String countClaim;

    private String linkPolicyNo;

    private String linkPolicyEffectiveDate;

    private String linkPolicyExpirationDate;

    private String linkPolicyStatus;

    private String basicPremium;

    private String sumInsured;

    private String av5BasicPremium;

    private String av5SumInsured;

    private String odavAmount;

    private String ftavAmount;

    private String tpbiavPersonAmount;

    private String tpbiavAccidentAmount;

    private String tppdavAmount;

    private String siprPremium;

    private String acovpdAmount;

    private String acovpdPremium;

    private String acovppAmount;

    private String acovppPremium;

    private String acovtdAmount;

    private String acovtdPremium;

    private String acovtpAmount;

    private String acovtpPremium;

    private String acovmeAmount;

    private String acovmePremium;

    private String acovmpAmount;

    private String acovmpPremium;

    private String acovbbAmount;

    private String acovbbPremium;

    private String acovodAmount;

    private String acovthAmount;

    private String acovfdPercent;

    private String acovfdAmount;

    private String acovedPercent;

    private String acovedAmount;

    private String acovotPercent;

    private String acovotAmount;

    private String acoveaPercent;

    private String acoveaAmount;

    private String vehicleMake;

    private String vehicleModel;

    private String vehicleBodyType;

    private String vehicleEngineCapacity;

    private String vehicleGarageLocation;

    private String vehicleModelType;

    private String englishNameFlag;

    private String wsCallFrom;

    private String motorBodyType;

    private String epolicyFlag;

    private String policyStatus;

    private List<MemoInfo> memoInfos;

    private List<EndorsementInfo> endorsementInfos;

    @JsonProperty("Customer_Info")
    private List<WsCustomerInfo> wsCustomerInfo;
}
