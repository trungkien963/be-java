package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsNebulaFileDetailsItems {

    private String docReferenceNo;

    private List<WsNebulaFileDetailsItemsIndex> docIndex;
}
