package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsPubFormPackage {

    private List<String> formIds;

    private String packageId;

    private WsPubFormFields formFields;

    private WsPubFormDistributions distributions;
}
