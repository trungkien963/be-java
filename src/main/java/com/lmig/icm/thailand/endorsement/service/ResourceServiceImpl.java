package com.lmig.icm.thailand.endorsement.service;

import com.lmig.icm.thailand.endorsement.enumType.RoleType;
import com.lmig.icm.thailand.endorsement.model.Beneficiary;
import com.lmig.icm.thailand.endorsement.model.Endorsement;
import com.lmig.icm.thailand.endorsement.model.Province;
import com.lmig.icm.thailand.endorsement.model.Role;
import com.lmig.icm.thailand.endorsement.payload.response.RoleModules;
import com.lmig.icm.thailand.endorsement.repository.ResourceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.util.List;

@Service
public class ResourceServiceImpl implements ResourceService {

    @Autowired
    private ResourceDao resourceDao;

    @Override
    public List<Province> getProvinces() {

        return resourceDao.getProvinces();
    }

    @Override
    public List<Province> uploadProvinces()
            throws FileNotFoundException {

        return resourceDao.uploadProvinces();
    }

    @Override
    public List<Province> getProvincesFromExcel()
            throws FileNotFoundException {

        return resourceDao.getProvincesFromExcel();
    }

    @Override
    public List<Endorsement> getEndorsements() {

        return resourceDao.getEndorsements();
    }

    @Override
    public List<Endorsement> uploadEndorsements() throws FileNotFoundException {

        return resourceDao.uploadEndorsements();
    }

    @Override
    public List<Endorsement> getEndorsementsFromExcel() throws FileNotFoundException {

        return resourceDao.getEndorsementsFromExcel();
    }

    @Override
    public List<Beneficiary> getBeneficiaries() {

        return resourceDao.getBeneficiaries();
    }

    @Override
    public List<Beneficiary> uploadBeneficiaries() throws FileNotFoundException {

        return resourceDao.uploadBeneficiaries();
    }

    @Override
    public List<Beneficiary> getBeneficiariesFromExcel() throws FileNotFoundException {

        return resourceDao.getBeneficiariesFromExcel();
    }

    @Override
    public List<Role> getRoles() {

        return resourceDao.getRoles();
    }

    @Override
    public List<Role> uploadRoles() throws FileNotFoundException {

        return resourceDao.uploadRoles();
    }

    @Override
    public List<Role> getRolesFromExcel() throws FileNotFoundException {

        return resourceDao.getRolesFromExcel();
    }

    @Override
    public RoleModules getRoleModulesByRoleName(String roleName) {

        String role = null;
        
        RoleModules roleModules = new RoleModules();

        if(roleName.equals(RoleType.Branch.name())) {

            role = "ROLE_BRANCH";
            roleModules.setViewMyWorkWithoutApprovingAuthority("N");
        }

        if(roleName.equals(RoleType.JuniorUnderwriter.name())) {

            role = "ROLE_JUNIOR_UNDERWRITER";
            roleModules.setViewMyWorkWithoutApprovingAuthority("N");
        }

        if(roleName.equals(RoleType.Underwriter.name())) {

            role = "ROLE_UNDERWRITER";
            roleModules.setViewMyWorkWithoutApprovingAuthority("N");
        }

        if(roleName.equals(RoleType.Supervisor.name())) {

            role = "ROLE_SUPERVISOR";
            roleModules.setViewMyWorkWithoutApprovingAuthority("N");
        }

        if(roleName.equals(RoleType.Manager.name())) {

            role = "ROLE_MANAGER";
            roleModules.setViewMyWorkWithoutApprovingAuthority("N");
        }

        if(roleName.equals(RoleType.TopManager.name())) {

            role = "ROLE_TOP_MANAGER";
            roleModules.setViewMyWorkWithoutApprovingAuthority("N");
        }
        
        List<Role> roles = resourceDao.getRoleModulesByRoleName(role);
        
        return roleModules;
    }
}
