package com.lmig.icm.thailand.endorsement.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetPolicyInfoRequest {

    @JsonProperty("CALLER")
    private String caller;

    @JsonProperty("REFERENCE_NO")
    private String referenceNo;

    @JsonProperty("POLICY_NO")
    private String policyNo;

    public String getCaller() {
        return (this.caller == null ? "" : this.caller);
    }

    public String getReferenceNo() {
        return (this.referenceNo == null ? "" : this.referenceNo);
    }

    public String getPolicyNo() {
        return (this.policyNo == null ? "" : this.policyNo);
    }
}
