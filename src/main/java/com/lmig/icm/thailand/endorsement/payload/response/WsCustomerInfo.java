package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsCustomerInfo {

    @JsonProperty("BIRTH_DATE")
    private String birthDate;

    @JsonProperty("CARD_TYPE")
    private String cardType;

    @JsonProperty("EMAIL_SEND_POLICY")
    private String emailSendPolicy;

    @JsonProperty("ENGLISH_FULL_NAME")
    private String englishFullName;

    @JsonProperty("ENGLISH_SALUTATION")
    private String englishSalutation;

    @JsonProperty("FIRST_NAME")
    private String firstName;

    @JsonProperty("FULL_NAME")
    private String fullName;

    @JsonProperty("HOME_PHONE_NO")
    private String homePhoneNo;

    @JsonProperty("ID_CARD_NO")
    private String idCardNo;

    @JsonProperty("INSURED_CODE")
    private String insuredCode;

    @JsonProperty("INSURED_TYPE")
    private String insuredType;

    @JsonProperty("LAST_NAME")
    private String lastName;

    @JsonProperty("MOBILE_PHONE_NO_1")
    private String mobilePhoneNo1;

    @JsonProperty("OCCUPATION")
    private String occupation;

    @JsonProperty("OCCUPATION_CODE")
    private String occupationCode;

    @JsonProperty("SALUTATION")
    private String salutation;

    @JsonProperty("SEX")
    private String sex;

    @JsonProperty("TAX_BRANCH_CODE")
    private String taxBranchCode;

    @JsonProperty("TAX_EXEMPTION_FLAG")
    private String taxExemptionFlag;

    @JsonProperty("TAX_ID_NO")
    private String taxIdNo;

    public String getBirthDate() {
        return (this.birthDate == null ? "" : this.birthDate);
    }

    public String getCardType() {
        return (this.cardType == null ? "" : this.cardType);
    }

    public String getEmailSendPolicy() {
        return (this.emailSendPolicy == null ? "" : this.emailSendPolicy);
    }

    public String getEnglishFullName() {
        return (this.englishFullName == null ? "" : this.englishFullName);
    }

    public String getEnglishSalutation() {
        return (this.englishSalutation == null ? "" : this.englishSalutation);
    }

    public String getFirstName() {
        return (this.firstName == null ? "" : this.firstName);
    }

    public String getFullName() {
        return (this.fullName == null ? "" : this.fullName);
    }

    public String getHomePhoneNo() {
        return (this.homePhoneNo == null ? "" : this.homePhoneNo);
    }

    public String getIdCardNo() {
        return (this.idCardNo == null ? "" : this.idCardNo);
    }

    public String getInsuredCode() {
        return (this.insuredCode == null ? "" : this.insuredCode);
    }

    public String getInsuredType() {
        return (this.insuredType == null ? "" : this.insuredType);
    }

    public String getLastName() {
        return (this.lastName == null ? "" : this.lastName);
    }

    public String getMobilePhoneNo1() {
        return (this.mobilePhoneNo1 == null ? "" : this.mobilePhoneNo1);
    }

    public String getOccupation() {
        return (this.occupation == null ? "" : this.occupation);
    }

    public String getOccupationCode() {
        return (this.occupationCode == null ? "" : this.occupationCode);
    }

    public String getSalutation() {
        return (this.salutation == null ? "" : this.salutation);
    }

    public String getSex() {
        return (this.sex == null ? "" : this.sex);
    }

    public String getTaxBranchCode() {
        return (this.taxBranchCode == null ? "" : this.taxBranchCode);
    }

    public String getTaxExemptionFlag() {
        return (this.taxExemptionFlag == null ? "" : this.taxExemptionFlag);
    }

    public String getTaxIdNo() {
        return (this.taxIdNo == null ? "" : this.taxIdNo);
    }
}
