package com.lmig.icm.thailand.endorsement.service;

import com.lmig.icm.thailand.endorsement.payload.request.WsEarnixCalculationRequest;
import com.lmig.icm.thailand.endorsement.payload.response.WsEarnixCalculationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class CalculationServiceImpl implements CalculationService {

    private final static String xApiKey = "Fopi60SlZj3SZ97lMNtifFNpdYrq7t95tgzNqIh7";

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public WsEarnixCalculationResponse calculation(WsEarnixCalculationRequest wsEarnixCalculationRequest) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-api-key", xApiKey);

        HttpEntity<WsEarnixCalculationRequest> entity = new HttpEntity<>(wsEarnixCalculationRequest, headers);

        WsEarnixCalculationResponse wsEarnixCalculationResponse = restTemplate.postForObject("https://xvm0cvhkzf-vpce-0f88b0f9eae23c614.execute-api.ap-southeast-1.amazonaws.com/dev/callEarnix", entity, WsEarnixCalculationResponse.class);

        return wsEarnixCalculationResponse;
    }
}
