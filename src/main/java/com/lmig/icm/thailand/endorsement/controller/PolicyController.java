package com.lmig.icm.thailand.endorsement.controller;

import com.lmig.icm.thailand.endorsement.model.Policy;
import com.lmig.icm.thailand.endorsement.model.PolicyEndorsement;
import com.lmig.icm.thailand.endorsement.model.PolicyInfo;
import com.lmig.icm.thailand.endorsement.payload.request.*;
import com.lmig.icm.thailand.endorsement.payload.response.*;
import com.lmig.icm.thailand.endorsement.service.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/policies")
public class PolicyController {

    @Autowired
    private PolicyService policyService;

    @GetMapping
    public ResponseEntity<?> searchPolicies(@Valid SearchPolicyRequest searchPolicyRequest) {

        ReturnResult results = null;

        try {

            results = policyService.searchPolicies(searchPolicyRequest);

            ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), results.getWsStatus().getWsReturnDescription(), results.getResults().size(), results.getResults(), results.getWsStatus());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();

            Integer httpStatus = Integer.parseInt(e.getMessage().substring(0, 3));

            if(httpStatus == HttpStatus.INTERNAL_SERVER_ERROR.value()) {

                ApiResponse response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.name(), e.getMessage().substring(6), 0, null, null);
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            List<Policy> policies = new ArrayList<>();

            ApiResponse response = new ApiResponse(HttpStatus.GATEWAY_TIMEOUT.value(), HttpStatus.GATEWAY_TIMEOUT.name(), HttpStatus.GATEWAY_TIMEOUT.name(), policies.size(), policies, null);
            return new ResponseEntity<>(response, HttpStatus.GATEWAY_TIMEOUT);
        }
    }

    @GetMapping("/details")
    public ResponseEntity<?> getPolicyInfos(@Valid GetPolicyInfoRequest getPolicyInfoRequest) {

        List<PolicyInfo> results = new ArrayList<>();

        try {

            results = policyService.getPolicyInfos(getPolicyInfoRequest);

            ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();

            Integer httpStatus = Integer.parseInt(e.getMessage().substring(0, 3));

            if(httpStatus == HttpStatus.INTERNAL_SERVER_ERROR.value()) {

                ApiResponse response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.name(), e.getMessage().substring(6), 0, null, null);
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            ApiResponse response = new ApiResponse(HttpStatus.GATEWAY_TIMEOUT.value(), HttpStatus.GATEWAY_TIMEOUT.name(), HttpStatus.GATEWAY_TIMEOUT.name(), results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.GATEWAY_TIMEOUT);
        }
    }

    @GetMapping("/{policyNo}")
    public ResponseEntity<?> getPolicyDetails(@Valid @PathVariable String policyNo) {

        PolicyDetailsRequest policyDetailsRequest = new PolicyDetailsRequest();
        policyDetailsRequest.setPOLICY_NO(policyNo);
        policyDetailsRequest.setCALLER("NEO");
        policyDetailsRequest.setREFERENCE_NO(policyNo);
        policyDetailsRequest.setMK_INFO_FLAG("Y");

        List<PolicyDetailsResponse> results = new ArrayList<>();

        try {

            PolicyDetailsResponse policyDetailsResponse = policyService.getPolicyDetails(policyDetailsRequest);
            results.add(policyDetailsResponse);

            ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();

            Integer httpStatus = Integer.parseInt(e.getMessage().substring(0, 3));

            if(httpStatus == HttpStatus.INTERNAL_SERVER_ERROR.value()) {

                ApiResponse response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.name(), e.getMessage().substring(6), 0, null, null);
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            ApiResponse response = new ApiResponse(HttpStatus.GATEWAY_TIMEOUT.value(), HttpStatus.GATEWAY_TIMEOUT.name(), HttpStatus.GATEWAY_TIMEOUT.name(), results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.GATEWAY_TIMEOUT);
        }
    }

    @GetMapping("/{policyNo}/endorsements")
    public ResponseEntity<?> getEndorsementsByPolicyNo(
            @Valid @PathVariable(required = true) String policyNo,
            @Valid @RequestParam(value = "status", required = false) String status,
            @Valid @RequestParam(value = "gettable", required = false) Integer gettable) {

        List<PolicyEndorsement> results = new ArrayList<>();

        try {

            if(status != null) {

                results = policyService.getEndorsementsByStatus(policyNo, status);
            } else {

                results = policyService.getEndorsementsByPolicyNo(policyNo, gettable);
            }

            ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();

            Integer httpStatus = Integer.parseInt(e.getMessage().substring(0, 3));

            if(httpStatus == HttpStatus.INTERNAL_SERVER_ERROR.value()) {

                ApiResponse response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.name(), e.getMessage().substring(6), 0, null, null);
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            ApiResponse response = new ApiResponse(HttpStatus.GATEWAY_TIMEOUT.value(), HttpStatus.GATEWAY_TIMEOUT.name(), HttpStatus.GATEWAY_TIMEOUT.name(), results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.GATEWAY_TIMEOUT);
        }
    }

    @PostMapping("/{policyNo}/endorsements")
    public ResponseEntity<?> addPolicyEndorsement(
            @Valid @PathVariable String policyNo,
            @Valid @RequestBody AddEndorsementRequest addEndorsementRequest) {

        ApiResponse response = null;

        try {

            response = policyService.addEndorsement(policyNo, addEndorsementRequest);

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();

            Integer httpStatus = Integer.parseInt(e.getMessage().substring(0, 3));

            if(httpStatus == HttpStatus.INTERNAL_SERVER_ERROR.value()) {

                response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.name(), e.getMessage().substring(6), 0, null, null);
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            response = new ApiResponse(HttpStatus.GATEWAY_TIMEOUT.value(), HttpStatus.GATEWAY_TIMEOUT.name(), HttpStatus.GATEWAY_TIMEOUT.name(), 0, null, null);
            return new ResponseEntity<>(response, HttpStatus.GATEWAY_TIMEOUT);
        }
    }

    @PutMapping("/{policyNo}/endorsements")
    public ResponseEntity<?> updatePolicyEndorsement(
            @Valid @PathVariable String policyNo,
            @Valid @RequestBody UpdateEndorsementRequest updateEndorsementRequest) {

        List<PolicyEndorsement> results = new ArrayList<>();

        try {

            results = policyService.updateEndorsement(policyNo, updateEndorsementRequest);

            ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();

            Integer httpStatus = Integer.parseInt(e.getMessage().substring(0, 3));

            if(httpStatus == HttpStatus.INTERNAL_SERVER_ERROR.value()) {

                ApiResponse response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.name(), e.getMessage().substring(6), 0, null, null);
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            ApiResponse response = new ApiResponse(HttpStatus.GATEWAY_TIMEOUT.value(), HttpStatus.GATEWAY_TIMEOUT.name(), HttpStatus.GATEWAY_TIMEOUT.name(), results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.GATEWAY_TIMEOUT);
        }
    }

    @PostMapping("/{policyNo}/endorsements/endorse")
    public ResponseEntity<?> createEndorsement(
            @Valid @PathVariable String policyNo,
            @Valid @RequestParam(value = "endorsable", required = false) Integer endorsable,
            @Valid @RequestBody CreateEndorsementRequest createEndorsementRequest) {

        CreateEndorsementResponse createEndorsementResponse = null;

        try {

            createEndorsementResponse = policyService.createEndorsement(policyNo, endorsable, createEndorsementRequest);

            if(createEndorsementResponse.getResults().isEmpty() && createEndorsementResponse.getWsStatus() == null) {

                ApiResponse response = new ApiResponse(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.name(), "No endorsement is found!", createEndorsementResponse.getResults().size(), createEndorsementResponse.getResults(), null);
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else if(createEndorsementResponse.getWsStatus().getWsReturnCode().equals("1")) {

                ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), createEndorsementResponse.getWsStatus().getWsReturnDescription(), createEndorsementResponse.getResults().size(), createEndorsementResponse.getResults(), createEndorsementResponse.getWsStatus());
                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {

                ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), createEndorsementResponse.getWsStatus().getWsReturnDescription(), createEndorsementResponse.getResults().size(), createEndorsementResponse.getResults(), createEndorsementResponse.getWsStatus());
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {

            e.printStackTrace();

            Integer httpStatus = Integer.parseInt(e.getMessage().substring(0, 3));

            if(httpStatus == HttpStatus.INTERNAL_SERVER_ERROR.value()) {

                ApiResponse response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.name(), e.getMessage().substring(6), 0, null, null);
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            List<Policy> policies = new ArrayList<>();

            ApiResponse response = new ApiResponse(HttpStatus.GATEWAY_TIMEOUT.value(), HttpStatus.GATEWAY_TIMEOUT.name(), HttpStatus.GATEWAY_TIMEOUT.name(), policies.size(), policies, null);
            return new ResponseEntity<>(response, HttpStatus.GATEWAY_TIMEOUT);
        }
    }

    @DeleteMapping("/{policyNo}/endorsements/{endorsementId}")
    public ResponseEntity<?> delete(
            @Valid @PathVariable String policyNo,
            @Valid @PathVariable Integer endorsementId) {

        List<PolicyEndorsement> results = new ArrayList<>();

        policyService.delete(policyNo, endorsementId);

        return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null));
    }

    @GetMapping("/endorsements")
    public ResponseEntity<?> getAllEndorsements(
            @Valid @RequestParam(value = "status", required = false) String status,
            @Valid @RequestParam(value = "gettable", required = false) Integer gettable) {

        List<PolicyEndorsement> results = new ArrayList<>();

        try {

            if(status != null) {

                results = policyService.getAllEndorsementsByStatus(status);
            } else if(gettable != null) {

                results = policyService.getAllEndorsements(gettable);
            } else {

                results = policyService.getAllEndorsements();
            }

            ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();

            Integer httpStatus = Integer.parseInt(e.getMessage().substring(0, 3));

            if(httpStatus == HttpStatus.INTERNAL_SERVER_ERROR.value()) {

                ApiResponse response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.name(), e.getMessage().substring(6), 0, null, null);
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            ApiResponse response = new ApiResponse(HttpStatus.GATEWAY_TIMEOUT.value(), HttpStatus.GATEWAY_TIMEOUT.name(), HttpStatus.GATEWAY_TIMEOUT.name(), results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.GATEWAY_TIMEOUT);
        }
    }

    @PostMapping("/endorsements/status")
    public ResponseEntity<?> checkEndorsementStatus(@Valid @RequestBody CheckEndorsementStatusRequest checkEndorsementStatusRequest) {

        List<CheckEndorsementStatusResponse> results = new ArrayList<>();

        try {

            results = policyService.checkEndorsementStatus(checkEndorsementStatusRequest);

            ApiResponse response = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK.name(), "Successful!", results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();

            Integer httpStatus = Integer.parseInt(e.getMessage().substring(0, 3));

            if(httpStatus == HttpStatus.INTERNAL_SERVER_ERROR.value()) {

                ApiResponse response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.name(), e.getMessage().substring(6), 0, null, null);
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            ApiResponse response = new ApiResponse(HttpStatus.GATEWAY_TIMEOUT.value(), HttpStatus.GATEWAY_TIMEOUT.name(), HttpStatus.GATEWAY_TIMEOUT.name(), results.size(), results, null);
            return new ResponseEntity<>(response, HttpStatus.GATEWAY_TIMEOUT);
        }
    }
}
