package com.lmig.icm.thailand.endorsement.enumType;

public enum PolicyEndorsementStatus {
    UNAPPROVED,
    WAITING_FOR_APPROVAL,
    APPROVED,
    REJECTED,
    PROCESSED;
}
