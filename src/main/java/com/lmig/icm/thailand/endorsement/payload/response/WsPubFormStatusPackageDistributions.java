package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsPubFormStatusPackageDistributions {

    private String timestamp;

    private String type;

    private String status;

    private String message;

    private String metadata;
}
