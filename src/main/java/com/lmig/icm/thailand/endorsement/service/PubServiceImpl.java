package com.lmig.icm.thailand.endorsement.service;

import com.lmig.icm.thailand.endorsement.payload.request.PubLoginRequest;
import com.lmig.icm.thailand.endorsement.payload.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class PubServiceImpl implements PubService {

    private static final String clientId = "HJ4FwzG2cCmUmqLjF9AnaO8EFV4REfoL";
    private static final String clientSecret = "k2zn4aarQDKZ520D";

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public PubLoginResponse login(PubLoginRequest pubLoginRequest) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", pubLoginRequest.getClient_id());
        map.add("client_secret", pubLoginRequest.getClient_secret());

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

        WsPubLoginResponse wsPubLoginResponse = restTemplate.postForObject("https://test-apis.ap-southeast-1.lmig.com/oauth/client_credential/accesstoken?grant_type=client_credentials", entity, WsPubLoginResponse.class);

        PubLoginResponse pubLoginResponse = new PubLoginResponse();
        pubLoginResponse.setRefreshTokenExpiresIn(wsPubLoginResponse.getRefresh_token_expires_in());
        pubLoginResponse.setApiProductList(wsPubLoginResponse.getApi_product_list());
        pubLoginResponse.setApiProductListJson(wsPubLoginResponse.getApi_product_list_json());
        pubLoginResponse.setOrganizationName(wsPubLoginResponse.getOrganization_name());
        pubLoginResponse.setDeveloperEmail(wsPubLoginResponse.getDeveloper_email());
        pubLoginResponse.setTokenType(wsPubLoginResponse.getToken_type());
        pubLoginResponse.setIssuedAt(wsPubLoginResponse.getIssued_at());
        pubLoginResponse.setClientId(wsPubLoginResponse.getClient_id());
        pubLoginResponse.setAccessToken(wsPubLoginResponse.getAccess_token());
        pubLoginResponse.setApplicationName(wsPubLoginResponse.getApplication_name());
        pubLoginResponse.setScope(wsPubLoginResponse.getScope());
        pubLoginResponse.setExpiresIn(wsPubLoginResponse.getExpires_in());
        pubLoginResponse.setRefreshCount(wsPubLoginResponse.getRefresh_count());
        pubLoginResponse.setStatus(wsPubLoginResponse.getStatus());

        return pubLoginResponse;
    }

    @Override
    public PubForms getForms() {

        PubLoginRequest pubLoginRequest = new PubLoginRequest();
        pubLoginRequest.setClient_id(clientId);
        pubLoginRequest.setClient_secret(clientSecret);

        PubLoginResponse pubLoginResponse = login(pubLoginRequest);

        String token = "Bearer " + pubLoginResponse.getAccessToken();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", token);

        HttpEntity entity = new HttpEntity(headers);

        ResponseEntity<WsPubFormList> response = restTemplate.exchange("https://test-apis.ap-southeast-1.lmig.com/v2/pubapi/forms?clientId=amcmregional", HttpMethod.GET, entity, WsPubFormList.class);

        WsPubFormList wsPubFormList = response.getBody();
        List<WsPubForm> wsPubForms = wsPubFormList.getFormList();
        PubForms pubForms = new PubForms();
        List<PubForm> forms = new ArrayList<>();
        for(WsPubForm wsPubForm : wsPubForms) {

            PubForm form = new PubForm();
            form.setId(wsPubForm.getFormId());
            form.setMetaURL(wsPubForm.getFormMetaURL());

            forms.add(form);
        }
        pubForms.setForms(forms);

        return pubForms;
    }

    @Override
    public PubFormDetails getFormById(String formId) {

        PubLoginRequest pubLoginRequest = new PubLoginRequest();
        pubLoginRequest.setClient_id(clientId);
        pubLoginRequest.setClient_secret(clientSecret);

        PubLoginResponse pubLoginResponse = login(pubLoginRequest);

        String token = "Bearer " + pubLoginResponse.getAccessToken();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", token);

        HttpEntity entity = new HttpEntity(headers);

        ResponseEntity<WsPubFormDetails> response = restTemplate.exchange("https://test-apis.ap-southeast-1.lmig.com/v2/pubapi/sampleFormsRequest?clientId=amcmregional&formId=" + formId, HttpMethod.GET, entity, WsPubFormDetails.class);
        WsPubFormDetails wsPubFormDetails = response.getBody();

        PubFormDetails pubFormDetails = new PubFormDetails();
        pubFormDetails.setCallBackUrl(wsPubFormDetails.getCallBackUrl());
        pubFormDetails.setValidateOnly(wsPubFormDetails.getValidateOnly());

        PubFormClient pubFormClient = new PubFormClient();
        pubFormClient.setId(wsPubFormDetails.getClient().getId());
        pubFormClient.setRequestId(wsPubFormDetails.getClient().getRequestId());
        pubFormClient.setClientRequestId(wsPubFormDetails.getClient().getClientRequestId());
        pubFormClient.setApiKey(wsPubFormDetails.getClient().getApiKey());
        pubFormDetails.setClient(pubFormClient);

        List<PubFormPackage> pubFormPackages = new ArrayList<>();
        List<WsPubFormPackage> wsPubFormPackages = wsPubFormDetails.getFormPackages();
        for(WsPubFormPackage wsPubFormPackage : wsPubFormPackages) {

            PubFormPackage pubFormPackage = new PubFormPackage();
            pubFormPackage.setFormIds(wsPubFormPackage.getFormIds());
            pubFormPackage.setPackageId(wsPubFormPackage.getPackageId());

            PubFormFields pubFormFields = new PubFormFields();
            pubFormFields.setCompanyAddress(wsPubFormPackage.getFormFields().getThailand_Endorsement_Sample_CompanyAddress());
            pubFormFields.setRecipientName(wsPubFormPackage.getFormFields().getThailand_Endorsement_Sample_RecipientName());
            pubFormFields.setSurname(wsPubFormPackage.getFormFields().getThailand_Endorsement_Sample_Surname());
            pubFormFields.setCompanyAddress(wsPubFormPackage.getFormFields().getThailand_Endorsement_Sample_CompanyAddress());
            pubFormFields.setRecipientAddress(wsPubFormPackage.getFormFields().getThailand_Endorsement_Sample_RecipientAddress());
            pubFormFields.setRecipientCity(wsPubFormPackage.getFormFields().getThailand_Endorsement_Sample_RecipientCity());
            pubFormPackage.setFormFields(pubFormFields);

            PubFormDistributions pubFormDistributions = new PubFormDistributions();
            pubFormDistributions.setEmail(wsPubFormPackage.getDistributions().getEmail());
            pubFormDistributions.setArchive(wsPubFormPackage.getDistributions().getArchive());
            pubFormDistributions.setEsign(wsPubFormPackage.getDistributions().getEsign());
            pubFormDistributions.setPrint(wsPubFormPackage.getDistributions().getPrint());
            pubFormDistributions.setNebulaArchive(wsPubFormPackage.getDistributions().getNebulaArchive());
            pubFormPackage.setDistributions(pubFormDistributions);

            pubFormPackages.add(pubFormPackage);
        }
        pubFormDetails.setFormPackages(pubFormPackages);

        return pubFormDetails;
    }

    @Override
    public WsPubFormStatus upload(WsPubFormDetails wsPubFormDetails) {

        PubLoginRequest pubLoginRequest = new PubLoginRequest();
        pubLoginRequest.setClient_id(clientId);
        pubLoginRequest.setClient_secret(clientSecret);

        PubLoginResponse pubLoginResponse = login(pubLoginRequest);

        String token = "Bearer " + pubLoginResponse.getAccessToken();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", token);

        HttpEntity<WsPubFormDetails> entity = new HttpEntity<>(wsPubFormDetails, headers);

        WsPubFormUpload wsPubFormUpload = restTemplate.postForObject("https://test-apis.ap-southeast-1.lmig.com/v2/pubapi/formsRequest?clientId=amcmregional", entity, WsPubFormUpload.class);

        HttpHeaders headers2 = new HttpHeaders();
        headers2.setContentType(MediaType.APPLICATION_JSON);
        headers2.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers2.set("Authorization", token);

        HttpEntity entity2 = new HttpEntity(headers2);

        ResponseEntity<WsPubFormStatus> response = restTemplate.exchange("https://test-apis.ap-southeast-1.lmig.com/v2/pubapi/formsRequestStatus?requestId=" + wsPubFormUpload.getRequestId() + "&clientId=amcmregional", HttpMethod.GET, entity2, WsPubFormStatus.class);
        WsPubFormStatus wsPubFormStatus = response.getBody();

        return wsPubFormStatus;
    }

    @Override
    public WsPubFormStatus download(String requestId) {

        PubLoginRequest pubLoginRequest = new PubLoginRequest();
        pubLoginRequest.setClient_id(clientId);
        pubLoginRequest.setClient_secret(clientSecret);

        PubLoginResponse pubLoginResponse = login(pubLoginRequest);

        String token = "Bearer " + pubLoginResponse.getAccessToken();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", token);

        HttpEntity entity = new HttpEntity(headers);

        ResponseEntity<WsPubFormStatus> response = restTemplate.exchange("https://test-apis.ap-southeast-1.lmig.com/v2/pubapi/formsRequestStatus?requestId=" + requestId + "&clientId=amcmregional", HttpMethod.GET, entity, WsPubFormStatus.class);
        WsPubFormStatus wsPubFormStatus = response.getBody();

        return wsPubFormStatus;
    }
}
