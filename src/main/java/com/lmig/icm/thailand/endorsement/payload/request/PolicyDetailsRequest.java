package com.lmig.icm.thailand.endorsement.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PolicyDetailsRequest {

    @JsonProperty("CALLER")
    private String CALLER;

    @JsonProperty("REFERENCE_NO")
    private String REFERENCE_NO;

    @JsonProperty("POLICY_NO")
    private String POLICY_NO;

    @JsonProperty("MK_INFO_FLAG")
    private String MK_INFO_FLAG;

    public String getCALLER() {
        return (this.CALLER == null ? "" : this.CALLER);
    }

    public String getREFERENCE_NO() {
        return (this.REFERENCE_NO == null ? "" : this.REFERENCE_NO);
    }

    public String getPOLICY_NO() {
        return (this.POLICY_NO == null ? "" : this.POLICY_NO);
    }

    public String getMK_INFO_FLAG() {
        return (this.MK_INFO_FLAG == null ? "" : this.MK_INFO_FLAG);
    }
}
