package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsMemoInfo {

    private String batch_number;

    private String batch_key;

    private String reference_no;

    private String policy_no;

    private String memo_code;

    private String memo_header;

    private String memo_detail;
}
