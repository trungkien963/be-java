package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsCheckOKToEndorseAutoResponse {

    public String BATCH_NUMBER;

    public String BATCH_KEY;

    public String REFERENCE_NO;

    public String WS_RETURN_CODE;

    public String WS_RETURN_DESCRIPTION;

    public String RESULT_CODE;

    public String POLICY_NO_PAR;

    public String RORJOR_NO_PAR;

    public String RESULT_DESCRIPTION;
}
