package com.lmig.icm.thailand.endorsement.controller;

import com.lmig.icm.thailand.endorsement.payload.request.WsEarnixCalculationRequest;
import com.lmig.icm.thailand.endorsement.payload.response.WsEarnixCalculationResponse;
import com.lmig.icm.thailand.endorsement.service.CalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/calculation")
public class CalculationController {

    @Autowired
    private CalculationService calculationService;

    @PostMapping
    public ResponseEntity<?> calculate(@Valid @RequestBody WsEarnixCalculationRequest wsEarnixCalculationRequest) {

        WsEarnixCalculationResponse response = calculationService.calculation(wsEarnixCalculationRequest);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
