package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PubFormDetails {

    private String callBackUrl;

    private String validateOnly;

    private PubFormClient client;

    private List<PubFormPackage> formPackages;
}
