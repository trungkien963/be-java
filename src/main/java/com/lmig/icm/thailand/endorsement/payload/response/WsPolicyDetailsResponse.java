package com.lmig.icm.thailand.endorsement.payload.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsPolicyDetailsResponse {

    @JsonProperty("BATCH_NUMBER")
    private String BATCH_NUMBER;

    @JsonProperty("Endorsement_Info")
    private List<WsEndorsementInfoNew> Endorsement_Info;

    @JsonProperty("Memo_Info")
    private List<WsMemoInfoNew> Memo_Info;

    @JsonProperty("Policy_Detail")
    private List<WsPolicyDetails> Policy_Detail;

    @JsonProperty("Policy_History")
    private List<WsPolicyHistory> Policy_History;

    @JsonProperty("Policy_Renewal_History")
    private List<WsPolicyRenewalHistory> Policy_Renewal_History;

    @JsonProperty("REFERENCE_NO")
    private String REFERENCE_NO;

    @JsonProperty("WS_RETURN_CODE")
    private String WS_RETURN_CODE;

    @JsonProperty("WS_RETURN_DESCRIPTION")
    private String WS_RETURN_DESCRIPTION;
}
