package com.lmig.icm.thailand.endorsement.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Role {

    private long id;

    private String role;

    private String module;

    private String uploader;

    private String uploaded_date;
}
