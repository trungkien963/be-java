package com.lmig.icm.thailand.endorsement.payload.request;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddEndorsementRequest {

    private String endorsementType;

    private String content;

    public String getEndorsementType() {
        return (this.endorsementType == null ? "" : this.endorsementType);
    }

    public String getContent() {
        return (this.content == null ? "" : this.content);
    }
}
