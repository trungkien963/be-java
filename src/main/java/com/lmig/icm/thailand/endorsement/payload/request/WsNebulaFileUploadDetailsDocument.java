package com.lmig.icm.thailand.endorsement.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsNebulaFileUploadDetailsDocument {

    @JsonProperty("docIndex")
    private List<WsNebulaFileUploadDetailsDocumentIndex> docIndex;

    @JsonProperty("docObject")
    private String docObject;

    @JsonProperty("docName")
    private String docName;

    @JsonProperty("docType")
    private String docType;

    @JsonProperty("mimeType")
    private String mimeType;
}
