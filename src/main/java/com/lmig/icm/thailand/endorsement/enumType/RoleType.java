package com.lmig.icm.thailand.endorsement.enumType;

public enum RoleType {
    Branch,
    JuniorUnderwriter,
    Underwriter,
    Supervisor,
    Manager,
    TopManager
}
