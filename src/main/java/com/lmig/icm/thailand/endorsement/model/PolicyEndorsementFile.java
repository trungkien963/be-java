package com.lmig.icm.thailand.endorsement.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PolicyEndorsementFile {

    private long id;

    private String endorsementId;

    private String policyNo;

    private String endorsementType;

    private String fileName;

    private String fullName;

    private String extension;

    private String filePath;

    private String uploader;

    private String uploadedDate;
}
