package com.lmig.icm.thailand.endorsement.payload.request;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CheckEndorsementStatusRequest {

    private String referenceNo;

    private String caller;

    private String policyNo;

    private String rorjorNo;

    public String getReferenceNo() {
        return (this.referenceNo == null ? "" : this.referenceNo);
    }

    public String getCaller() {
        return (this.caller == null ? "" : this.caller);
    }

    public String getPolicyNo() {
        return (this.policyNo == null ? "" : this.policyNo);
    }

    public String getRorjorNo() {
        return (this.rorjorNo == null ? "" : this.rorjorNo);
    }
}
