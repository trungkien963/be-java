package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleModules {

    private String searchOnLandingPage = "Y";

    private String createSingleEndorsement = "Y";

    private String createBulkEndorsement = "Y";

    private String viewMyWorkWithApprovingAuthority = "Y";

    private String viewMyWorkWithoutApprovingAuthority = "Y";

    private String approveEndorsements = "Y";

    private String rejectEndorsements = "Y";

    private String reprintEndorsement = "Y";
}
