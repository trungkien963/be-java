package com.lmig.icm.thailand.endorsement.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsGetCustomerDetailsRequest {

    @JsonProperty("REFERENCE_NO")
    private String referenceNo;

    @JsonProperty("CALLER")
    private String caller;

    @JsonProperty("INSURED_NAME")
    private String insuredName;

    @JsonProperty("INSURED_CODE")
    private String insuredCode;

    @JsonProperty("ID_CARD_NO")
    private String idCardNo;

    public String getReferenceNo() {
        return (this.referenceNo == null ? "" : this.referenceNo);
    }

    public String getCaller() {
        return (this.caller == null ? "" : this.caller);
    }

    public String getInsuredName() {
        return (this.insuredName == null ? "" : this.insuredName);
    }

    public String getInsuredCode() {
        return (this.insuredCode == null ? "" : this.insuredCode);
    }

    public String getIdCardNo() {
        return (this.idCardNo == null ? "" : this.idCardNo);
    }
}
