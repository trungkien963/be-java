package com.lmig.icm.thailand.endorsement.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EndorsementInfo {

    private long id;

    private String batchNumber;

    private String batchKey;

    private String referenceNo;

    private String policyNo;

    private String policyEndorsementNo;

    private String endorsementType;
}
