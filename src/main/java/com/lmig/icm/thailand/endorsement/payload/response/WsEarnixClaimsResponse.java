package com.lmig.icm.thailand.endorsement.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WsEarnixClaimsResponse {

    private String claimNumber;

    private String claimStatus;

    private String faultInd;

    private String isDry;

    private Float lossAmount;

    private String lossCause;
}
