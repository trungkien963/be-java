package com.lmig.icm.thailand.endorsement.payload.request;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FileUploadRequest {

    private String fileType;

    private String endorsementId;

    private String policyNo;

    private String endorsementType;

    public String getFileType() {
        return (this.fileType == null ? "" : this.fileType);
    }

    public String getEndorsementId() {
        return (this.endorsementId == null ? "" : this.endorsementId);
    }

    public String getPolicyNo() {
        return (this.policyNo == null ? "" : this.policyNo);
    }

    public String getEndorsementType() {
        return (this.endorsementType == null ? "" : this.endorsementType);
    }
}
