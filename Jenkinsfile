#!groovy
@Library(['gic-utils', 'utils@master']) _

def customWorkerImages = ['maven=artifactory-apac.aws.lmig.com/pod-templates-apac/maven:3.6.2-jdk-11-slim']
pod(workers: customWorkerImages) {

  try {

    def jobParams = parseJobParamsThailand(env.JOB_NAME)

    // Override jobParams
    echo "workspace = ${env.WORKSPACE}"
    def workspaces = env.WORKSPACE.split('/')
    def appName = workspaces[workspaces.length-1]
    def appEnv = workspaces[workspaces.length-2]

    jobParams.projectName = appName
    jobParams.jobName = appName
    jobParams.appName = appName
    jobParams.environment = appEnv
    jobParams.awsEnvironment = appEnv
    echo "jobParams = ${jobParams}"

    env.appName = jobParams.appName
    env.environment = jobParams.environment

    if(env.environment == 'non_prod') {
      properties([
        parameters([
          string(
            name: 'BRANCH_OVERRIDE',
            defaultValue: 'staging',
            description: 'Branch to build. (staging or master)'
          ),
          choice(
            name: 'ENVIRONMENT_OVERRIDE',
            choices: [
              'non_prod',
              'prod'
            ],
            description: 'Environment to deploy'
          )
        ])
      ])
    }

    if (env.ENVIRONMENT_OVERRIDE) {
      env.environment = env.ENVIRONMENT_OVERRIDE
    }

    stage('Checkout') {
      checkout scm

      def pom = readMavenPom file: 'pom.xml'
      env.version = [
        jobParams.countryShortName,
        env.environment,
        pom.version
          .replace(
            '-SNAPSHOT',
            ''
          ),
        env.BUILD_NUMBER
      ].join('-')

      currentBuild.displayName = "[${env.environment}] - [${env.version}] - #${env.BUILD_NUMBER}"
    }

    container("maven") {
      stage('Maven build') {
        sh """
          mvn -s ./.mvn/settings.xml versions:set -DnewVersion=${env.version}
          mvn -s ./.mvn/settings.xml clean install
        """
      }
//      stage('Code Analysis') {
//        // skiping SonarQube analysis for feature branch deployments given license issue
//        withSonarQubeEnv('sonarqube'){
//          sh "mvn sonar:sonar"
//        }
//        timeout(time: 2, unit: 'MINUTES') {
//          waitForQualityGate abortPipeline: true
//        }
//      }
    }

    stage('Extract Jar file') {
      sh "cp target/${env.appName}-${env.version}.jar target/app.jar"
      sh "mkdir target/zip"
      sh "cd target/ && java -Djarmode=layertools -jar ./app.jar extract && cd .."
      sh "ls target/";
    }

    stage('Build Docker image') {
      sh "docker build -t ${env.appName}:${env.version} . --network=host"
      sh "docker save -o target/zip/${env.appName}.tar ${env.appName}:${env.version}"
    }

    stage('Build Zip file') {
      sh "cp -r deploy/cloudformation target/zip"
      sh "cd target/zip && zip -r ../${env.appName}.zip *"
    }

    stage('Push to Artifactory') {
      def artifacts = ["target/${env.appName}.zip"]
      artifactoryUploadFiles files: artifacts, version: env.version
    }

    if(env.ENVIRONMENT_OVERRIDE != 'prod') {
      stage("Trigger deploy") {
        def jobName = "${env.JOB_NAME}-deploy"
        build job: jobName, parameters: [[$class: 'StringParameterValue', name: 'APP_VERSION', value: env.version]], wait: false
      }
    }

    currentBuild.result = 'SUCCESS'

  } catch (e) {

    echo "Build Failed"
    currentBuild.result = 'FAILURE'
    throw e

  }
}
