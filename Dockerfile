FROM container-images.lmig.com/docker/base-image/base_springboot

WORKDIR /app

ARG DEPENDENCY_DIR=./target
ADD ${DEPENDENCY_DIR}/dependencies ./
ADD ${DEPENDENCY_DIR}/spring-boot-loader ./
ADD ${DEPENDENCY_DIR}/snapshot-dependencies ./
ADD ${DEPENDENCY_DIR}/application ./

EXPOSE 8084

ENTRYPOINT java -cp /app \
  -Dspring.profiles.active=${env} \
  -Dspring.datasource.password=${db_password} \
  org.springframework.boot.loader.JarLauncher \
